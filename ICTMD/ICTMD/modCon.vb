﻿Imports System.Data.Odbc
Module modCon
    Public con As New OdbcConnection

    'Database connection
    Public Sub connect()
        Try
            'localhost/server type
            'con = New OdbcConnection("UID=root;Driver={MYSQL ODBC 3.51 Driver};Database=dbictmd;")
            'client type
            'test'
            'con = New OdbcConnection("Server=192.168.31.18;UID=root;Password=12345;Driver={MySQL ODBC 3.51 Driver};Database=dbictmd;")
            con = New OdbcConnection("Server=localhost;UID=root;Password=1234;Driver={MySQL ODBC 3.51 Driver};Database=dbictmd;")
            'bahay 
            'con = New OdbcConnection("Server=192.168.254.123;UID=toor;Driver={MYSQL ODBC 3.51 Driver};Database=dbictmd;")

            con.Open()
        Catch ex As Exception
            MsgBox(ex.Message.ToString, vbCritical, "Error")
            End
        Finally
            GC.Collect()
        End Try
    End Sub

    'To show the data to datagridview
    Public Sub loadData(ByVal sql As String, ByVal dgv As DataGridView)
        Dim da As New OdbcDataAdapter
        Dim dt As New DataTable
        da = New OdbcDataAdapter(sql, con)
        da.Fill(dt)
        dgv.DataSource = dt
        da.Dispose()

    End Sub

    Function rep(ByVal str As String) As String
        rep = Replace(str, "\", "")
    End Function

    'To save the data from accomplish
    Public Sub save(ByVal sql As String)
        Dim cmd As New OdbcCommand
        cmd = New OdbcCommand(sql, con)

        cmd.Parameters.AddWithValue("?", frmAccomplish.cbDivision.SelectedItem)
        cmd.Parameters.AddWithValue("?", frmAccomplish.cbSection.SelectedItem)
        cmd.Parameters.AddWithValue("?", frmAccomplish.cbNature.SelectedItem)
        cmd.Parameters.AddWithValue("?", frmAccomplish.cmbNature.SelectedItem)
        cmd.Parameters.AddWithValue("?", frmAccomplish.cbLocation.SelectedItem)
        cmd.Parameters.AddWithValue("?", frmAccomplish.txtFindings.Text)
        cmd.Parameters.AddWithValue("?", frmAccomplish.txtRemarks.Text)
        cmd.Parameters.AddWithValue("?", frmAccomplish.txtRequest.Text)
        cmd.Parameters.AddWithValue("?", frmAccomplish.cmbStatus.SelectedItem)
        cmd.Parameters.AddWithValue("?", MDIMain.tslUser.Text)
        cmd.Parameters.AddWithValue("?", Date.Now.ToString("dd-MM-yyyy") + "  |  " + Date.Now.ToLongTimeString())
        'cmd.Parameters.AddWithValue("?", MDIMain.tslUser.Text)
        cmd.Parameters.AddWithValue("?", MDIMain.tslnum.Text)


        cmd.ExecuteNonQuery()

    End Sub

    'To save the data from request
    Public Sub save2(ByVal sql As String)
        Dim claim As String
        frmRequest.lblClaimby.Text = claim
        claim = ""

        Dim cmd As New OdbcCommand
        cmd = New OdbcCommand(sql, con)
        cmd.Parameters.AddWithValue("?", frmRequest.cbDivision.SelectedItem)
        cmd.Parameters.AddWithValue("?", frmRequest.cbSection.SelectedItem)
        cmd.Parameters.AddWithValue("?", frmRequest.cbNature.SelectedItem)
        cmd.Parameters.AddWithValue("?", frmRequest.cmbNature.SelectedItem)
        cmd.Parameters.AddWithValue("?", frmRequest.cbLocation.SelectedItem)
        cmd.Parameters.AddWithValue("?", frmRequest.txtFindings.Text)
        cmd.Parameters.AddWithValue("?", frmRequest.txtRemarks.Text)
        cmd.Parameters.AddWithValue("?", frmRequest.lblRequested.Text)
        cmd.Parameters.AddWithValue("?", frmRequest.cmbStatus.SelectedItem)
        cmd.Parameters.AddWithValue("?", If(String.IsNullOrEmpty(claim), DBNull.Value, claim))
        cmd.Parameters.AddWithValue("?", Date.Now.ToString("dd-MM-yyyy") + "  |  " + Date.Now.ToLongTimeString())
        'cmd.Parameters.AddWithValue("?", Date.Now.ToString("dd-MM-yyyy hh:mm:ss"))
        cmd.Parameters.AddWithValue("?", MDIMain.tslnum.Text)
        cmd.Parameters.AddWithValue("?", Date.Now.ToString("dd-MM-yyyy") + "  |  " + Date.Now.ToLongTimeString())
        cmd.Parameters.AddWithValue("?", frmRequest.txtRoom.Text)

        cmd.ExecuteNonQuery()

    End Sub


End Module
