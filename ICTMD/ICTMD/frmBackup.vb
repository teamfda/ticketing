﻿Imports System.Data.Odbc
Imports System.IO
Public Class frmBackup
    Dim da As New OdbcDataAdapter
    Dim dt As New DataTable
    Dim cmd As String
    Dim dtseCt As Integer

    'Connection to database
    Private Sub frmBackup_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Call modCon.connect()

    End Sub

    'To open the connection
    Public Sub koneksi()
        Try
            con = New OdbcConnection("Server=" & txtHostname.Text & "; " _
                                                + "user id=" & txtUsername.Text & ";" _
                                                + "password=" & txtPassword.Text & ";Driver={MYSQL ODBC 3.51 Driver};")
            If con.State = ConnectionState.Closed Then

                con.Open() ' open our connections
            End If
        Catch ex As Exception
            MsgBox("Connection Failed !")
        End Try
    End Sub

    'To select database
    Private Sub btnDatabase_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDatabase.Click
        Try
            'Call modCon.connect()
            koneksi()
            cmd = "SELECT DISTINCT TABLE_SCHEMA FROM information_schema.TABLES"
            da = New OdbcDataAdapter(cmd, modCon.con)
            da.Fill(dt)
            dtseCt = 0
            cmbDatabase.Enabled = True
            cmbDatabase.Items.Clear()
            cmbDatabase.Items.Add("== Select Database ==")
            While dtseCt < dt.Rows.Count
                cmbDatabase.Items.Add(dt.Rows(dtseCt)(0).ToString())
                dtseCt = dtseCt + 1
            End While
            cmbDatabase.SelectedIndex = 0
            btnDatabase.Enabled = False
            btnBackup.Enabled = True
            btnRestore.Enabled = True
            con.Close()
            dt.Dispose()
            da.Dispose()

        Catch ex As Exception
            MsgBox("Connection Failed!")
        End Try
    End Sub

    'To export the database
    Private Sub btnBackup_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBackup.Click
        Dim DbFile As String
        Try
            'Call modCon.connect()
            koneksi()
            SaveFileDialog1.Filter = "SQL Dump File (*.sql)|*.sql|All files (*.*)|*.*"
            SaveFileDialog1.FileName = "Database Backup " + DateTime.Now.ToString("yyyy-MM-dd HH-mm-ss") + ".sql"
            If SaveFileDialog1.ShowDialog = DialogResult.OK Then
                DbFile = SaveFileDialog1.FileName
                Dim BackupProccess As New Process
                BackupProccess.StartInfo.FileName = "cmd.exe"
                BackupProccess.StartInfo.UseShellExecute = False
                BackupProccess.StartInfo.WorkingDirectory = "C:\xampp\mysql\bin\"
                BackupProccess.StartInfo.RedirectStandardInput = True
                BackupProccess.StartInfo.RedirectStandardOutput = True
                BackupProccess.Start()

                Dim BackupStream As StreamWriter = BackupProccess.StandardInput
                Dim myStreamReader As StreamReader = BackupProccess.StandardOutput


                BackupStream.WriteLine("mysqldump --user=" & txtUsername.Text & "  --password=" & txtPassword.Text & " -h " & txtHostname.Text & " " & cmbDatabase.Text & " > """ + DbFile + """")


                BackupStream.Close()
                BackupProccess.WaitForExit()
                BackupProccess.Close()
                con.Close()
                MsgBox("Backup your MySQL database Created Successfully!", MsgBoxStyle.Information, "Backup MySql Database")
            End If


        Catch ex As Exception
            MsgBox("Nothing to do!")
        End Try
    End Sub

    'To import the database
    Private Sub btnRestore_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRestore.Click
        Dim DbFile As String
        Try
            'Call modCon.connect()
            koneksi()
            ' create svaFileDialog and OpenFileDialog Component to our project
            OpenFileDialog1.Filter = "SQL Dump File (*.sql)|*.sql|All files (*.*)|*.*"
            If OpenFileDialog1.ShowDialog = DialogResult.OK Then


                DbFile = OpenFileDialog1.FileName
                Dim BackupProccess As New Process
                BackupProccess.StartInfo.FileName = "cmd.exe"
                BackupProccess.StartInfo.UseShellExecute = False
                BackupProccess.StartInfo.WorkingDirectory = "C:\xampp\mysql\bin\"
                BackupProccess.StartInfo.RedirectStandardInput = True
                BackupProccess.StartInfo.RedirectStandardOutput = True
                BackupProccess.Start()

                Dim BackupStream As StreamWriter = BackupProccess.StandardInput
                Dim myStreamReader As StreamReader = BackupProccess.StandardOutput
                BackupStream.WriteLine("mysqldump --user=" & txtUsername.Text & "  --password=" & txtPassword.Text & "  -h " & txtHostname.Text & " " & cmbDatabase.Text & " > """ + DbFile + """")

                BackupStream.Close()
                BackupProccess.WaitForExit()
                BackupProccess.Close()
                con.Close()
                MsgBox("Restore your MySQL database Successfully!", MsgBoxStyle.Information, "Restore MySql Database")
            End If

        Catch ex As Exception
            MsgBox("Nothing to do!")
        End Try
    End Sub

    'To exit the backup form
    Private Sub btnExit_Click(sender As Object, e As EventArgs) Handles btnExit.Click
        If MsgBox("Are you sure you want to close?", vbQuestion + vbYesNo, "User Account") = vbYes Then
            cmbDatabase.Items.Clear()
            txtHostname.Clear()
            txtPassword.Clear()
            txtUsername.Clear()
            Me.Hide()
        End If
    End Sub

    Private Sub Label4_Click(sender As Object, e As EventArgs) Handles Label4.Click

    End Sub

    Private Sub Panel1_Paint(sender As Object, e As PaintEventArgs) Handles Panel1.Paint

    End Sub
End Class