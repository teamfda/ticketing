﻿Imports System.Data.Odbc
Imports System.IO
Imports System
Imports System.Threading
Imports Tulpep.NotificationWindow

Public Class frmRequest
    Sub populate()
        Call loadData("SELECT ID,Division,Section,Nature,Categories,Location,Findings,Remarks,Requested,Date,Status,Claimby,Dateclaim,Datefinish,Doneby,Room FROM tblaccomplish WHERE Fk2 = '" & MDIMain.tslnum.Text & "'", dvgstudent)
    End Sub

    'To load the data from datagridview
    Private Sub frmRequest_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Call populate()
        cbDivision.SelectedIndex = -1
        cmbStatus.SelectedIndex = 1
        txtRoom.Text = ""
        txtRemarks.Text = ""
        dvgstudent.Text = ""
        cbSection.SelectedIndex = -1
        cbLocation.Text = ""
        cbNature.Text = ""
        cbLocation.SelectedIndex = -1
        cbNature.SelectedIndex = -1
        cmbNature.SelectedIndex = -1
        lblRequested.Text = ""
        lblClaimby.Text = ""
        dvgstudent.Tag = ""
        lblRequested.Text = MDIMain.tslUser.Text
        lblDoneby.Text = MDIMain.tslUser.Text
        'Call loadData("SELECT ID,Division,Section,Nature,Categories,Location,Findings,Remarks,Requested,Date,Status,Claimby,Dateclaim,Datefinish,Priority,Room FROM tblaccomplish WHERE Fk2 LIKE '%" & MDIMain.tslnum.Text & "%'", dvgstudent)
        Call loadComboLoc()
        'Call loadComboNat()
        'Call loadComboRec()
        'Call loadComboLev()
        Call loadComboSec()
        'Timer1.Start()

        If MDIMain.tslPos.Text = "Super Administrator" Or MDIMain.tslPos.Text = "Administrator" Then
            btnCaseClosed.Enabled = False
        Else
            btnCaseClosed.Enabled = False
        End If

    End Sub

    'To submit data for request
    Private Sub btnSubmit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        If cbDivision.Text = "" Or cbLocation.Text = "" Or cbNature.Text = "" Or cbSection.Text = "" Or cmbNature.Text = "" Then
            MsgBox("Please fill up all field!", vbExclamation, "Accomplish Information")
            cmbStatus.SelectedIndex = 1
            lblRequested.Text = MDIMain.tslUser.Text
        Else
            If Len(dvgstudent.Tag) = 0 Then
                'My.Computer.Audio.PlaySystemSound(
                'System.Media.SystemSounds.Exclamation)

                'Dim objPopup As New PopupNotifier

                'objPopup.AnimationDuration = 2000

                'objPopup.Image = Image.FromFile("")
                'objPopup.TitleText = "Notification Alert"
                'objPopup.ContentText = "New Request!"
                'objPopup.Popup()


                Call modCon.save2("INSERT INTO tblaccomplish(Division,Section,Nature,Categories,Location,Findings,Remarks,Requested,Status,Claimby,Date,Fk2,DateRequested,Room)VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?)")
                MsgBox("ADDED SUCCESSFULLY", vbInformation, "Accomplish Information")

                cbDivision.SelectedIndex = -1
                txtRoom.Text = ""
                txtRemarks.Text = ""
                lblRequested.Text = ""
                cbSection.SelectedIndex = -1
                cbDivision.SelectedIndex = -1
                cmbStatus.SelectedIndex = 1
                cbLocation.SelectedIndex = -1
                cbNature.SelectedIndex = -1
                cmbNature.SelectedIndex = -1
                cbNature.Text = ""
                lblRequested.Text = MDIMain.tslUser.Text
                'Call loadData("SELECT ID,Division,Section,Nature,Categories,Location,Findings,Remarks,Requested,Date,Status,Claimby,Dateclaim,Datefinish,Priority,EstimatedDays,Room FROM tblaccomplish WHERE Fk2 LIKE '%" & MDIMain.tslnum.Text & "%'", dvgstudent)
                Call populate()

                Dim da As OdbcDataAdapter
                Dim dt As New DataTable
                Dim sql As String = "SELECT * FROM tblaccomplish WHERE ID IN (SELECT MAX(ID) FROM tblaccomplish)"
                da = New OdbcDataAdapter(sql, modCon.con)
                da.Fill(dt)

                Dim strTime0 As String = dt.Rows(0)(15)
                Dim strTime1 = strTime0.Replace("-", "/")
                Dim strTime = strTime1.Remove(12, 2)

                ' Convert to datetime
                Dim dtTime As DateTime = Convert.ToDateTime(strTime)
                ' Display in 24hr format
                Dim requestedDate1 = dtTime.ToString("HH:mm tt")


                If requestedDate1 >= "17:00 PM" And requestedDate1 <= "7:00 AM" Then
                    MsgBox("Your request has been sent, but it will be accomodate NEXT WORKING DAY! Thank you!", vbExclamation, "Information")
                End If

            Else
                Dim cmd As Odbc.OdbcCommand
                Dim sql As String

                sql = "UPDATE tblaccomplish SET Division = ?, Section = ?, Nature = ?, Categories = ?, Location = ?, Findings = ?, Remarks = ?, Doneby = ?, Date = ?, Room = ?, Fk2 = ? WHERE ID = " & Val(dvgstudent.Tag) & ""
                MsgBox("UPDATED SUCCESSFULLY", vbInformation, "Accomplish Information")
                cmd = New Odbc.OdbcCommand(sql, modCon.con)
                cmd.Parameters.AddWithValue("?", cbDivision.Text)
                cmd.Parameters.AddWithValue("?", cbSection.Text)
                cmd.Parameters.AddWithValue("?", cbNature.Text)
                cmd.Parameters.AddWithValue("?", cmbNature.Text)
                cmd.Parameters.AddWithValue("?", cbLocation.Text)
                cmd.Parameters.AddWithValue("?", txtFindings.Text)
                cmd.Parameters.AddWithValue("?", txtRemarks.Text)
                cmd.Parameters.AddWithValue("?", lblDoneby.Text)
                cmd.Parameters.AddWithValue("?", Date.Now.ToString("dd-MM-yyyy") + "  |  " + Date.Now.ToLongTimeString())
                cmd.Parameters.AddWithValue("?", txtRoom.Text
                                            )
                cmd.Parameters.AddWithValue("?", MDIMain.tslnum.Text)
                cmd.ExecuteNonQuery()
                dvgstudent.Tag = ""
                lblRequested.Text = MDIMain.tslUser.Text
                cbDivision.SelectedIndex = -1
                txtRoom.Text = ""
                txtRemarks.Text = ""
                lblRequested.Text = ""
                cbSection.SelectedIndex = -1
                cbLocation.SelectedIndex = -1
                cbNature.SelectedIndex = -1
                cmbNature.SelectedIndex = -1
                cmbStatus.SelectedIndex = 1
                cbNature.Text = ""
                'Call loadData("SELECT ID,Division,Section,Nature,Categories,Location,Findings,Remarks,Requested,Date,Status,Claimby,Dateclaim,Datefinish,Priority,EstimatedDays,Room FROM tblaccomplish WHERE Fk2 LIKE '%" & MDIMain.tslnum.Text & "%'", dvgstudent)
                Call populate()


            End If
        End If
    End Sub

    'To select the data from datagridview
    Private Sub dgvStudent_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dvgstudent.CellClick

        If e.RowIndex >= 0 Then
            dvgstudent.Tag = dvgstudent.Item(0, e.RowIndex).Value.ToString()
            cbDivision.Text = dvgstudent.Item(1, e.RowIndex).Value.ToString()
            cbSection.Text = dvgstudent.Item(2, e.RowIndex).Value.ToString()
            cbNature.Text = dvgstudent.Item(3, e.RowIndex).Value.ToString()
            cmbNature.Text = dvgstudent.Item(4, e.RowIndex).Value.ToString()
            cbLocation.Text = dvgstudent.Item(5, e.RowIndex).Value.ToString()
            txtRoom.Text = dvgstudent.Item(15, e.RowIndex).Value.ToString()
            txtFindings.Text = dvgstudent.Item(6, e.RowIndex).Value.ToString()
            txtRemarks.Text = dvgstudent.Item(7, e.RowIndex).Value.ToString()
            lblRequested.Text = dvgstudent.Item(8, e.RowIndex).Value.ToString()
            cmbStatus.Text = dvgstudent.Item(10, e.RowIndex).Value.ToString()
            lblClaimby.Text = dvgstudent.Item(11, e.RowIndex).Value.ToString()

        End If

        If cmbStatus.Text = "Done" Or cmbStatus.Text = "Ongoing" Then
            Call enabler()
        Else
            Call enabler1()
        End If

    End Sub

    Private Sub enabler1()
        cbDivision.Enabled = True
        cbSection.Enabled = True
        cbNature.Enabled = True
        cmbNature.Enabled = True
        cbLocation.Enabled = True
        txtRoom.Enabled = True
        txtFindings.Enabled = False
        txtRemarks.Enabled = True
        lblRequested.Enabled = True
        lblClaimby.Enabled = True
        btnSubmit.Enabled = True
        btnCaseClosed.Enabled = False
    End Sub

    Private Sub enabler()
        cbDivision.Enabled = False
        cbSection.Enabled = False
        cbNature.Enabled = False
        cmbNature.Enabled = False
        cbLocation.Enabled = False
        txtRoom.Enabled = False
        txtFindings.Enabled = False
        txtRemarks.Enabled = False
        lblRequested.Enabled = False
        cmbStatus.Enabled = False
        lblClaimby.Enabled = False
        btnSubmit.Enabled = False
        btnCaseClosed.Enabled = True
    End Sub

    'Clean textbox and combobox
    Private Sub btnClear_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClear.Click
        cbDivision.SelectedIndex = -1
        cmbStatus.SelectedIndex = 1
        txtRoom.Text = ""
        txtRemarks.Text = ""
        dvgstudent.Text = ""
        cbSection.SelectedIndex = -1
        cbLocation.Text = ""
        cbNature.Text = ""
        cbLocation.SelectedIndex = -1
        cbNature.SelectedIndex = -1
        cmbNature.SelectedIndex = -1
        lblRequested.Text = ""
        lblClaimby.Text = ""
        txtRoom.Text = ""
        txtFindings.Text = ""
        dvgstudent.Tag = ""
        lblRequested.Text = MDIMain.tslUser.Text
        'Call loadData("SELECT ID, Division, Section, Nature, Categories, Location, Findings, Remarks, Requested, Date, Status, Claimby, Dateclaim, Datefinish, Priority, EstimatedDays, Room FROM tblaccomplish WHERE Fk2 Like '%" & MDIMain.tslnum.Text & "%'", dvgstudent)
        Call populate()
        Call enabler1()
    End Sub

    'Load the data from combobox Location
    Private Sub loadComboLoc()
        Dim da As OdbcDataAdapter
        Dim dt As New DataTable
        Dim sql As String = "SELECT Location FROM tbllocation"
        da = New OdbcDataAdapter(sql, modCon.con)
        da.Fill(dt)
        cbLocation.Items.Clear()
        cbLocation.Items.Add("")
        For x = 0 To dt.Rows.Count - 1
            cbLocation.Items.Add(dt.Rows(x)(0))
        Next

    End Sub

    'Load the data from combobox Nature
    Private Sub loadComboNat()
        Dim da As OdbcDataAdapter
        Dim dt As New DataTable
        Dim sql As String = "SELECT Nature FROM tblnature"
        da = New OdbcDataAdapter(sql, modCon.con)
        da.Fill(dt)
        cbNature.Items.Clear()
        cbNature.Items.Add("")
        For x = 0 To dt.Rows.Count - 1
            cbNature.Items.Add(dt.Rows(x)(0))
        Next

    End Sub

    'Load the priority level data from combobox Level
    Private Sub loadComboLev()
        Dim da As OdbcDataAdapter
        Dim dt As New DataTable
        'Dim sql As String = "SELECT Categories FROM tblnature WHERE Nature LIKE 'Hardware'"
        Dim sql As String = "SELECT Priority FROM tblnature WHERE Categories LIKE '%" & cmbNature.Text & "%'"
        da = New OdbcDataAdapter(sql, modCon.con)
        da.Fill(dt)
        'cblevel.Text = dt.Rows(0)(0)

    End Sub

    'Load the estimated days data
    'Private Sub loadDays()
    'Dim da As OdbcDataAdapter
    'Dim dt As New DataTable
    'Dim sql As String = "SELECT Categories FROM tblnature WHERE Nature LIKE 'Hardware'"
    'Dim sql As String = "SELECT EstimatedDays FROM tblnature WHERE Priority LIKE '%" & cblevel.Text & "%'"
    'da = New OdbcDataAdapter(sql, modCon.con)
    'da.Fill(dt)
    'txtDays.Text = dt.Rows(0)(0)

    'End Sub

    'Load the categories data from tblnature where nature is like hardware
    Private Sub loadComboCat1()

        Dim da As OdbcDataAdapter
        Dim dt As New DataTable
        'Dim sql As String = "SELECT Categories,Priority FROM tblnature WHERE Nature LIKE 'Hardware'"
        Dim sql As String = "SELECT Categories FROM tblnature WHERE Nature LIKE 'Hardware'"
        da = New OdbcDataAdapter(sql, modCon.con)
        da.Fill(dt)
        cmbNature.Items.Clear()
        cmbNature.Items.Add("")

        For x = 0 To dt.Rows.Count - 1
            cmbNature.Items.Add(dt.Rows(x)(0))
            'cblevel.Items.Add(dt.Rows(x)(1))
        Next

    End Sub

    'Load the categories data from tblnature where nature is like software
    Private Sub loadComboCat2()

        Dim da As OdbcDataAdapter
        Dim dt As New DataTable
        Dim sql As String = "SELECT Categories FROM tblnature WHERE Nature LIKE 'Software'"
        da = New OdbcDataAdapter(sql, modCon.con)
        da.Fill(dt)
        cmbNature.Items.Clear()
        cmbNature.Items.Add("")
        For x = 0 To dt.Rows.Count - 1
            cmbNature.Items.Add(dt.Rows(x)(0))
        Next

    End Sub

    'Load the categories data from tblnature where nature is like network
    Private Sub loadComboCat3()

        Dim da As OdbcDataAdapter
        Dim dt As New DataTable
        Dim sql As String = "SELECT Categories FROM tblnature WHERE Nature LIKE 'Network'"
        da = New OdbcDataAdapter(sql, modCon.con)
        da.Fill(dt)
        cmbNature.Items.Clear()
        cmbNature.Items.Add("")
        For x = 0 To dt.Rows.Count - 1
            cmbNature.Items.Add(dt.Rows(x)(0))
        Next

    End Sub

    'Load the categories data from tblnature where nature is like printer
    Private Sub loadComboCat4()

        Dim da As OdbcDataAdapter
        Dim dt As New DataTable
        Dim sql As String = "SELECT Categories FROM tblnature WHERE Nature LIKE 'Printer'"
        da = New OdbcDataAdapter(sql, modCon.con)
        da.Fill(dt)
        cmbNature.Items.Clear()
        cmbNature.Items.Add("")
        For x = 0 To dt.Rows.Count - 1
            cmbNature.Items.Add(dt.Rows(x)(0))
        Next

    End Sub

    'Load the categories data from tbldivision
    Private Sub loadComboSec()
        Dim da As OdbcDataAdapter
        Dim dt As New DataTable
        Dim sql As String = "SELECT Categories FROM tbldivision"
        da = New OdbcDataAdapter(sql, modCon.con)
        da.Fill(dt)
        cbSection.Items.Clear()
        cbSection.Items.Add("")
        For x = 0 To dt.Rows.Count - 1
            cbSection.Items.Add(dt.Rows(x)(0))
        Next

    End Sub

    'To exit the request form
    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click


        If MsgBox("Are you sure you want to close?", vbQuestion + vbYesNo, "REQUEST") = vbYes Then
            Call cleaner()
            Call enabler1()
            Me.Hide()
        End If
    End Sub

    'Load the data from combobox Nature
    Private Sub cbNature_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbNature.SelectedIndexChanged
        cmbNature.SelectedIndex = -1
        If cbNature.Text = "Hardware" Then
            Call loadComboCat1()
        Else
            If cbNature.Text = "Software" Then
                Call loadComboCat2()
            Else
                If cbNature.Text = "Network" Then
                    Call loadComboCat3()
                Else
                    If cbNature.Text = "Printer" Then
                        Call loadComboCat4()
                    End If
                End If
            End If
        End If
    End Sub

    'Load the data from combobox division
    Private Sub cbDivision_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbDivision.SelectedIndexChanged
        cbSection.SelectedIndex = -1
        If cbDivision.Text = "AFS" Then
            Call loadComboCata()
        Else
            If cbDivision.Text = "CDRR" Then
                Call loadComboCatb()
            Else
                If cbDivision.Text = "CFRR" Then
                    Call loadComboCatc()

                Else
                    If cbDivision.Text = "CCRR" Then
                        Call loadComboCatd()
                    Else
                        If cbDivision.Text = "CDRRHR" Then
                            Call loadComboCatf()
                        Else
                            If cbDivision.Text = "CSL" Then
                                Call loadComboCatg()
                            Else
                                If cbDivision.Text = "ODG" Then
                                    Call loadComboCath()
                                Else
                                    If cbDivision.Text = "FROO" Then
                                        Call loadComboCati()
                                    Else
                                        If cbDivision.Text = "PPS" Then
                                            Call loadComboCatj()
                                        End If
                                    End If
                                End If
                            End If
                        End If
                    End If
                End If
            End If
        End If

    End Sub

    'Load the categories data from tbldivision where division is like afs
    Private Sub loadComboCata()

        Dim da As OdbcDataAdapter
        Dim dt As New DataTable
        Dim sql As String = "SELECT Categories FROM tbldivision WHERE Division LIKE 'AFS'"
        da = New OdbcDataAdapter(sql, modCon.con)
        da.Fill(dt)
        cbSection.Items.Clear()
        cbSection.Items.Add("")
        For x = 0 To dt.Rows.Count - 1
            cbSection.Items.Add(dt.Rows(x)(0))
        Next

    End Sub

    'Load the categories data from tbldivision where division is like cdrr
    Private Sub loadComboCatb()

        Dim da As OdbcDataAdapter
        Dim dt As New DataTable
        Dim sql As String = "SELECT Categories FROM tbldivision WHERE Division LIKE 'CDRR'"
        da = New OdbcDataAdapter(sql, modCon.con)
        da.Fill(dt)
        cbSection.Items.Clear()
        cbSection.Items.Add("")
        For x = 0 To dt.Rows.Count - 1
            cbSection.Items.Add(dt.Rows(x)(0))
        Next

    End Sub

    'Load the categories data from tbldivision where division is like cfrr
    Private Sub loadComboCatc()

        Dim da As OdbcDataAdapter
        Dim dt As New DataTable
        Dim sql As String = "SELECT Categories FROM tbldivision WHERE Division LIKE 'CFRR'"
        da = New OdbcDataAdapter(sql, modCon.con)
        da.Fill(dt)
        cbSection.Items.Clear()
        cbSection.Items.Add("")
        For x = 0 To dt.Rows.Count - 1
            cbSection.Items.Add(dt.Rows(x)(0))
        Next

    End Sub

    'Load the categories data from tbldivision where division is like cdrrhr
    Private Sub loadComboCatd()

        Dim da As OdbcDataAdapter
        Dim dt As New DataTable
        Dim sql As String = "SELECT Categories FROM tbldivision WHERE Division LIKE 'CCRR'"
        da = New OdbcDataAdapter(sql, modCon.con)
        da.Fill(dt)
        cbSection.Items.Clear()
        cbSection.Items.Add("")
        For x = 0 To dt.Rows.Count - 1
            cbSection.Items.Add(dt.Rows(x)(0))
        Next

    End Sub

    'Load the categories data from tbldivision where division is like csl
    Private Sub loadComboCatf()

        Dim da As OdbcDataAdapter
        Dim dt As New DataTable
        Dim sql As String = "SELECT Categories FROM tbldivision WHERE Division LIKE 'CDRRHR'"
        da = New OdbcDataAdapter(sql, modCon.con)
        da.Fill(dt)
        cbSection.Items.Clear()
        cbSection.Items.Add("")
        For x = 0 To dt.Rows.Count - 1
            cbSection.Items.Add(dt.Rows(x)(0))
        Next

    End Sub

    'Load the categories data from tbldivision where division is like odg
    Private Sub loadComboCatg()

        Dim da As OdbcDataAdapter
        Dim dt As New DataTable
        Dim sql As String = "SELECT Categories FROM tbldivision WHERE Division LIKE 'CSL'"
        da = New OdbcDataAdapter(sql, modCon.con)
        da.Fill(dt)
        cbSection.Items.Clear()
        cbSection.Items.Add("")
        For x = 0 To dt.Rows.Count - 1
            cbSection.Items.Add(dt.Rows(x)(0))
        Next

    End Sub

    'Load the categories data from tbldivision where division is like reu
    Private Sub loadComboCath()

        Dim da As OdbcDataAdapter
        Dim dt As New DataTable
        Dim sql As String = "SELECT Categories FROM tbldivision WHERE Division LIKE 'ODG'"
        da = New OdbcDataAdapter(sql, modCon.con)
        da.Fill(dt)
        cbSection.Items.Clear()
        cbSection.Items.Add("")
        For x = 0 To dt.Rows.Count - 1
            cbSection.Items.Add(dt.Rows(x)(0))
        Next

    End Sub

    'Load the categories data from tbldivision where division is like froo
    Private Sub loadComboCati()

        Dim da As OdbcDataAdapter
        Dim dt As New DataTable
        Dim sql As String = "SELECT Categories FROM tbldivision WHERE Division LIKE 'FROO'"
        da = New OdbcDataAdapter(sql, modCon.con)
        da.Fill(dt)
        cbSection.Items.Clear()
        cbSection.Items.Add("")
        For x = 0 To dt.Rows.Count - 1
            cbSection.Items.Add(dt.Rows(x)(0))
        Next

    End Sub

    'Load the categories data from tbldivision where division is like pps
    Private Sub loadComboCatj()

        Dim da As OdbcDataAdapter
        Dim dt As New DataTable
        Dim sql As String = "SELECT Categories FROM tbldivision WHERE Division LIKE 'PPS'"
        da = New OdbcDataAdapter(sql, modCon.con)
        da.Fill(dt)
        cbSection.Items.Clear()
        cbSection.Items.Add("")
        For x = 0 To dt.Rows.Count - 1
            cbSection.Items.Add(dt.Rows(x)(0))
        Next

    End Sub

    'Clean textbox
    Private Sub cleaner()
        Dim obj As Object
        For Each obj In grpEntry.Controls
            If TypeOf obj Is TextBox Then
                obj.clear()
            End If
        Next
    End Sub

    'Refresh the data in datagrid
    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        lblRequested.Text = MDIMain.tslUser.Text
        'Call loadComboLev()
        Call loadData("SELECT ID,Division,Section,Nature,Categories,Location,Findings,Remarks,Requested,Date,Status,Claimby,Dateclaim,Datefinish,Priority,Room FROM tblaccomplish WHERE Fk2 LIKE '%" & MDIMain.tslnum.Text & "%'", dvgstudent)
    End Sub

    'To update tha status to case closed
    Private Sub btnCaseClosed_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCaseClosed.Click
        Dim status = "Done"
        Dim cmd As Odbc.OdbcCommand
        Dim sql As String
        If Len(dvgstudent.Tag) = 0 Then
            MsgBox("Please Select Query!", vbInformation, "Request Information")
            dvgstudent.Tag = ""
            'Call loadData("SELECT ID,Division,Section,Nature,Categories,Location,Findings,Remarks,Requested,Date,Status,Claimby,Dateclaim,Datefinish,Priority,EstimatedDays,Room FROM tblaccomplish WHERE Fk2 LIKE '%" & MDIMain.tslnum.Text & "%'", dvgstudent)
            Call populate()
            lblRequested.Text = MDIMain.tslUser.Text
        Else
            If MsgBox("Are you sure you the request is done?!", vbQuestion + vbYesNo, "Information") = vbYes Then
                sql = "UPDATE tblaccomplish SET Status = ?, Remarks = ?, Datefinish = ?, Doneby = ? WHERE ID = " & Val(dvgstudent.Tag) & ""
                cmd = New Odbc.OdbcCommand(sql, modCon.con)
                cmd.Parameters.AddWithValue("?", status)
                cmd.Parameters.AddWithValue("?", txtRemarks.Text)
                cmd.Parameters.AddWithValue("?", Date.Now.ToString("dd-MM-yyyy") + " | " + Date.Now.ToLongTimeString())
                cmd.Parameters.AddWithValue("?", lblDoneby.Text)
                cmd.ExecuteNonQuery()
                'Call loadData("SELECT ID,Division,Section,Nature,Categories,Location,Findings,Remarks,Requested,Date,Status,Claimby,Dateclaim,Datefinish,Priority,EstimatedDays,Room FROM tblaccomplish WHERE Fk2 LIKE '%" & MDIMain.tslnum.Text & "%'", dvgstudent)
                Call populate()
                dvgstudent.Tag = ""
                lblRequested.Text = MDIMain.tslUser.Text
            Else
                Me.Show()
            End If
        End If


        Call cleaner()
        cbDivision.SelectedIndex = -1
        cmbStatus.SelectedIndex = 1
        txtRoom.Text = ""
        txtRemarks.Text = ""
        dvgstudent.Text = ""
        cbSection.SelectedIndex = -1
        cbLocation.Text = ""
        cbNature.Text = ""
        cbLocation.SelectedIndex = -1
        cbNature.SelectedIndex = -1
        cmbNature.SelectedIndex = -1
        lblClaimby.Text = ""
        txtRoom.Text = ""
    End Sub

    'To load the data in Priority level and estimated days from combobox Nature
    Private Sub cmbNature_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbNature.SelectedIndexChanged
        Call loadComboLev()
        'Call loadDays()
    End Sub

    Private Sub btnPrint_Click(sender As Object, e As EventArgs) Handles btnPrint.Click

        Hide()
        frmUserPrint.ShowDialog()
    End Sub

    Private Sub btnRefresh_Click_1(sender As Object, e As EventArgs) Handles btnRefresh.Click
        Call populate()
    End Sub
End Class