﻿Imports System.Data.Odbc
Imports System.IO
Imports System
Imports System.Threading

Public Class frmJoborder
    Sub populate()
        Dim forrequest = "For Request"
        lblClaimby.Text = MDIMain.tslUser.Text
        Call loadData("SELECT ID,Division,Section,Nature,Categories,Location,Findings,Remarks,Requested,Status,Claimby,Date,Doneby,Room FROM tblaccomplish WHERE Status = '" & forrequest & "' ORDER BY Division IN ('ODG', 'CDRR', 'FROO') DESC, Section IN ('ODG-IM') DESC", dvgstudent)
    End Sub

    'Load the data in datagridview
    Private Sub frmJoborder_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim forrequest = "For Request"
        Call populate()
        'Call loadData("SELECT ID,Division,Section,Nature,Categories,Location,Findings,Remarks,Requested,Status,Claimby,Date,Priority,Room FROM tblaccomplish WHERE Status LIKE '%" & forrequest & "%'", dvgstudent)
        Call loadComboLoc()
        'Call loadComboSec()
        Timer1.Start()
        cmbStatus.SelectedIndex = 2
        lblClaimby.Text = MDIMain.tslUser.Text
    End Sub

    Private Sub enabler()
        cbDivision.Enabled = False
        cbSection.Enabled = False
        cbNature.Enabled = False
        cmbNature.Enabled = False
        cbLocation.Enabled = False
        txtFindings.Enabled = True
        txtRemarks.Enabled = True
        txtRoom.Enabled = False
        cmbStatus.Enabled = False
    End Sub

    'For the color coded data based on their status
    Private Sub dvgstudent_CellFormatting(ByVal sender As Object, ByVal e As DataGridViewCellFormattingEventArgs) Handles dvgstudent.CellFormatting
        For i As Integer = 0 To Me.dvgstudent.Rows.Count - 1
            If Me.dvgstudent.Rows(i).Cells("Column2").Value = "CDRR" Then
                Me.dvgstudent.Rows(i).Cells("Column1").Style.ForeColor = Color.Red
                Me.dvgstudent.Rows(i).Cells("Column2").Style.ForeColor = Color.Red
                Me.dvgstudent.Rows(i).Cells("Column3").Style.ForeColor = Color.Red
                Me.dvgstudent.Rows(i).Cells("Column4").Style.ForeColor = Color.Red
                Me.dvgstudent.Rows(i).Cells("Column5").Style.ForeColor = Color.Red
                Me.dvgstudent.Rows(i).Cells("Column6").Style.ForeColor = Color.Red
                Me.dvgstudent.Rows(i).Cells("Column7").Style.ForeColor = Color.Red
                Me.dvgstudent.Rows(i).Cells("Column8").Style.ForeColor = Color.Red
                Me.dvgstudent.Rows(i).Cells("Column9").Style.ForeColor = Color.Red
                Me.dvgstudent.Rows(i).Cells("Column10").Style.ForeColor = Color.Red
                Me.dvgstudent.Rows(i).Cells("Column11").Style.ForeColor = Color.Red
                Me.dvgstudent.Rows(i).Cells("Column12").Style.ForeColor = Color.Red
                Me.dvgstudent.Rows(i).Cells("Column14").Style.ForeColor = Color.Red

            Else
                If Me.dvgstudent.Rows(i).Cells("Column2").Value = "ODG" Then
                    Me.dvgstudent.Rows(i).Cells("Column1").Style.ForeColor = Color.Red
                    Me.dvgstudent.Rows(i).Cells("Column2").Style.ForeColor = Color.Red
                    Me.dvgstudent.Rows(i).Cells("Column3").Style.ForeColor = Color.Red
                    Me.dvgstudent.Rows(i).Cells("Column4").Style.ForeColor = Color.Red
                    Me.dvgstudent.Rows(i).Cells("Column5").Style.ForeColor = Color.Red
                    Me.dvgstudent.Rows(i).Cells("Column6").Style.ForeColor = Color.Red
                    Me.dvgstudent.Rows(i).Cells("Column7").Style.ForeColor = Color.Red
                    Me.dvgstudent.Rows(i).Cells("Column8").Style.ForeColor = Color.Red
                    Me.dvgstudent.Rows(i).Cells("Column9").Style.ForeColor = Color.Red
                    Me.dvgstudent.Rows(i).Cells("Column10").Style.ForeColor = Color.Red
                    Me.dvgstudent.Rows(i).Cells("Column11").Style.ForeColor = Color.Red
                    Me.dvgstudent.Rows(i).Cells("Column12").Style.ForeColor = Color.Red
                    Me.dvgstudent.Rows(i).Cells("Column14").Style.ForeColor = Color.Red
                Else
                    If Me.dvgstudent.Rows(i).Cells("Column3").Value = "ODG-IM" Then
                        Me.dvgstudent.Rows(i).Cells("Column1").Style.ForeColor = Color.Red
                        Me.dvgstudent.Rows(i).Cells("Column2").Style.ForeColor = Color.Red
                        Me.dvgstudent.Rows(i).Cells("Column3").Style.ForeColor = Color.Red
                        Me.dvgstudent.Rows(i).Cells("Column4").Style.ForeColor = Color.Red
                        Me.dvgstudent.Rows(i).Cells("Column5").Style.ForeColor = Color.Red
                        Me.dvgstudent.Rows(i).Cells("Column6").Style.ForeColor = Color.Red
                        Me.dvgstudent.Rows(i).Cells("Column7").Style.ForeColor = Color.Red
                        Me.dvgstudent.Rows(i).Cells("Column8").Style.ForeColor = Color.Red
                        Me.dvgstudent.Rows(i).Cells("Column9").Style.ForeColor = Color.Red
                        Me.dvgstudent.Rows(i).Cells("Column10").Style.ForeColor = Color.Red
                        Me.dvgstudent.Rows(i).Cells("Column11").Style.ForeColor = Color.Red
                        Me.dvgstudent.Rows(i).Cells("Column12").Style.ForeColor = Color.Red
                        Me.dvgstudent.Rows(i).Cells("Column14").Style.ForeColor = Color.Red
                    Else
                        If Me.dvgstudent.Rows(i).Cells("Column2").Value = "FROO" Then
                            Me.dvgstudent.Rows(i).Cells("Column1").Style.ForeColor = Color.Red
                            Me.dvgstudent.Rows(i).Cells("Column2").Style.ForeColor = Color.Red
                            Me.dvgstudent.Rows(i).Cells("Column3").Style.ForeColor = Color.Red
                            Me.dvgstudent.Rows(i).Cells("Column4").Style.ForeColor = Color.Red
                            Me.dvgstudent.Rows(i).Cells("Column5").Style.ForeColor = Color.Red
                            Me.dvgstudent.Rows(i).Cells("Column6").Style.ForeColor = Color.Red
                            Me.dvgstudent.Rows(i).Cells("Column7").Style.ForeColor = Color.Red
                            Me.dvgstudent.Rows(i).Cells("Column8").Style.ForeColor = Color.Red
                            Me.dvgstudent.Rows(i).Cells("Column9").Style.ForeColor = Color.Red
                            Me.dvgstudent.Rows(i).Cells("Column10").Style.ForeColor = Color.Red
                            Me.dvgstudent.Rows(i).Cells("Column11").Style.ForeColor = Color.Red
                            Me.dvgstudent.Rows(i).Cells("Column12").Style.ForeColor = Color.Red
                            Me.dvgstudent.Rows(i).Cells("Column14").Style.ForeColor = Color.Red
                        End If
                    End If
                End If
            End If
        Next
    End Sub

    'Clean textbox and combobox
    Private Sub btnClear_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClear.Click
        cbDivision.SelectedIndex = -1
        cmbStatus.SelectedIndex = 2
        txtFindings.Text = ""
        txtRemarks.Text = ""
        dvgstudent.Text = ""
        cbSection.SelectedIndex = -1
        cbLocation.Text = ""
        cbNature.Text = ""
        cbLocation.SelectedIndex = -1
        cbNature.SelectedIndex = -1
        cmbNature.SelectedIndex = -1
        lblRequested.Text = ""
        lblClaimby.Text = ""
        dvgstudent.Tag = ""
        lblClaimby.Text = MDIMain.tslUser.Text
        txtRoom.Text = ""
        'Call loadData("SELECT ID,Division,Section,Nature,Categories,Location,Findings,Remarks,Requested,Status,Claimby,Date,Priority,Room FROM tblaccomplish WHERE Fk2 LIKE '%" & MDIMain.tslnum.Text & "%'", dvgstudent)
        Call populate()
    End Sub

    'Load the data from combobox Location
    Private Sub loadComboLoc()
        Dim da As OdbcDataAdapter
        Dim dt As New DataTable
        Dim sql As String = "SELECT Location FROM tbllocation"
        da = New OdbcDataAdapter(sql, modCon.con)
        da.Fill(dt)
        cbLocation.Items.Clear()
        cbLocation.Items.Add("")
        For x = 0 To dt.Rows.Count - 1
            cbLocation.Items.Add(dt.Rows(x)(0))
        Next
    End Sub

    'Load the data from combobox Nature
    Private Sub loadComboNat()
        Dim da As OdbcDataAdapter
        Dim dt As New DataTable
        Dim sql As String = "SELECT Nature FROM tblnature"
        da = New OdbcDataAdapter(sql, modCon.con)
        da.Fill(dt)
        cbNature.Items.Clear()
        cbNature.Items.Add("")
        For x = 0 To dt.Rows.Count - 1
            cbNature.Items.Add(dt.Rows(x)(0))
        Next
    End Sub

    'Load the data from combobox where the nature is like hardware
    Private Sub loadComboCat1()
        Dim da As OdbcDataAdapter
        Dim dt As New DataTable
        Dim sql As String = "SELECT Categories FROM tblnature WHERE Nature LIKE 'Hardware'"
        da = New OdbcDataAdapter(sql, modCon.con)
        da.Fill(dt)
        cmbNature.Items.Clear()
        cmbNature.Items.Add("")
        For x = 0 To dt.Rows.Count - 1
            cmbNature.Items.Add(dt.Rows(x)(0))
        Next
    End Sub

    'Load the data from combobox where the nature is like software
    Private Sub loadComboCat2()
        Dim da As OdbcDataAdapter
        Dim dt As New DataTable
        Dim sql As String = "SELECT Categories FROM tblnature WHERE Nature LIKE 'Software'"
        da = New OdbcDataAdapter(sql, modCon.con)
        da.Fill(dt)
        cmbNature.Items.Clear()
        cmbNature.Items.Add("")
        For x = 0 To dt.Rows.Count - 1
            cmbNature.Items.Add(dt.Rows(x)(0))
        Next
    End Sub

    'Load the data from combobox where the nature is like network
    Private Sub loadComboCat3()
        Dim da As OdbcDataAdapter
        Dim dt As New DataTable
        Dim sql As String = "SELECT Categories FROM tblnature WHERE Nature LIKE 'Network'"
        da = New OdbcDataAdapter(sql, modCon.con)
        da.Fill(dt)
        cmbNature.Items.Clear()
        cmbNature.Items.Add("")
        For x = 0 To dt.Rows.Count - 1
            cmbNature.Items.Add(dt.Rows(x)(0))
        Next
    End Sub

    'Load the data from combobox where the nature is like printer
    Private Sub loadComboCat4()
        Dim da As OdbcDataAdapter
        Dim dt As New DataTable
        Dim sql As String = "SELECT Categories FROM tblnature WHERE Nature LIKE 'Printer'"
        da = New OdbcDataAdapter(sql, modCon.con)
        da.Fill(dt)
        cmbNature.Items.Clear()
        cmbNature.Items.Add("")
        For x = 0 To dt.Rows.Count - 1
            cmbNature.Items.Add(dt.Rows(x)(0))
        Next
    End Sub

    'Load tha categories data from tbldivision
    Private Sub loadComboSec()
        Dim da As OdbcDataAdapter
        Dim dt As New DataTable
        Dim sql As String = "SELECT Categories FROM tbldivision"
        da = New OdbcDataAdapter(sql, modCon.con)
        da.Fill(dt)
        cbSection.Items.Clear()
        cbSection.Items.Add("")
        For x = 0 To dt.Rows.Count - 1
            cbSection.Items.Add(dt.Rows(x)(0))
        Next
    End Sub

    'To exit the joborder form 
    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        If MsgBox("Are you sure you want to close?", vbQuestion + vbYesNo, "JobOrder") = vbYes Then
            Me.Hide()
        End If
    End Sub

    'To select from loaded data in combobox Nature
    Private Sub cbNature_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbNature.SelectedIndexChanged
        cmbNature.SelectedIndex = -1
        If cbNature.Text = "Hardware" Then
            Call loadComboCat1()

        Else
            If cbNature.Text = "Software" Then
                Call loadComboCat2()

            Else
                If cbNature.Text = "Network" Then
                    Call loadComboCat3()

                Else
                    If cbNature.Text = "Printer" Then
                        Call loadComboCat4()

                    End If
                End If
            End If
        End If
    End Sub

    'To select from loaded data in combobox division
    Private Sub cbDivision_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbDivision.SelectedIndexChanged
        cbSection.SelectedIndex = -1
        If cbDivision.Text = "AFS" Then
            Call loadComboCata()
        Else
            If cbDivision.Text = "CDRR" Then
                Call loadComboCatb()
            Else
                If cbDivision.Text = "CFRR" Then
                    Call loadComboCatc()

                Else
                    If cbDivision.Text = "CDRRHR" Then
                        Call loadComboCatd()
                    Else
                        If cbDivision.Text = "CSL" Then
                            Call loadComboCatf()
                        Else
                            If cbDivision.Text = "ODG" Then
                                Call loadComboCatg()
                            Else
                                If cbDivision.Text = "REU" Then
                                    Call loadComboCath()
                                Else
                                    If cbDivision.Text = "FROO" Then
                                        Call loadComboCati()
                                    Else
                                        If cbDivision.Text = "PPS" Then
                                            Call loadComboCatj()
                                        End If
                                    End If
                                End If
                            End If
                        End If
                    End If
                End If
            End If
        End If

    End Sub

    'Load the data from combobox where the division is like afs
    Private Sub loadComboCata()
        Dim da As OdbcDataAdapter
        Dim dt As New DataTable
        Dim sql As String = "SELECT Categories FROM tbldivision WHERE Division LIKE 'AFS'"
        da = New OdbcDataAdapter(sql, modCon.con)
        da.Fill(dt)
        cbSection.Items.Clear()
        cbSection.Items.Add("")
        For x = 0 To dt.Rows.Count - 1
            cbSection.Items.Add(dt.Rows(x)(0))
        Next
    End Sub

    'Load the data from combobox where the division is like cdrr
    Private Sub loadComboCatb()
        Dim da As OdbcDataAdapter
        Dim dt As New DataTable
        Dim sql As String = "SELECT Categories FROM tbldivision WHERE Division LIKE 'CDRR'"
        da = New OdbcDataAdapter(sql, modCon.con)
        da.Fill(dt)
        cbSection.Items.Clear()
        cbSection.Items.Add("")
        For x = 0 To dt.Rows.Count - 1
            cbSection.Items.Add(dt.Rows(x)(0))
        Next
    End Sub

    'Load the data from combobox where the division is like cfrr
    Private Sub loadComboCatc()
        Dim da As OdbcDataAdapter
        Dim dt As New DataTable
        Dim sql As String = "SELECT Categories FROM tbldivision WHERE Division LIKE 'CFRR'"
        da = New OdbcDataAdapter(sql, modCon.con)
        da.Fill(dt)
        cbSection.Items.Clear()
        cbSection.Items.Add("")
        For x = 0 To dt.Rows.Count - 1
            cbSection.Items.Add(dt.Rows(x)(0))
        Next

    End Sub

    'Load the data from combobox where the division is like cdrrhr
    Private Sub loadComboCatd()
        Dim da As OdbcDataAdapter
        Dim dt As New DataTable
        Dim sql As String = "SELECT Categories FROM tbldivision WHERE Division LIKE 'CDRRHR'"
        da = New OdbcDataAdapter(sql, modCon.con)
        da.Fill(dt)
        cbSection.Items.Clear()
        cbSection.Items.Add("")
        For x = 0 To dt.Rows.Count - 1
            cbSection.Items.Add(dt.Rows(x)(0))
        Next

    End Sub

    'Load the data from combobox where the division is like csl
    Private Sub loadComboCatf()
        Dim da As OdbcDataAdapter
        Dim dt As New DataTable
        Dim sql As String = "SELECT Categories FROM tbldivision WHERE Division LIKE 'CSL'"
        da = New OdbcDataAdapter(sql, modCon.con)
        da.Fill(dt)
        cbSection.Items.Clear()
        cbSection.Items.Add("")
        For x = 0 To dt.Rows.Count - 1
            cbSection.Items.Add(dt.Rows(x)(0))
        Next
    End Sub

    'Load the data from combobox where the division is like odg
    Private Sub loadComboCatg()
        Dim da As OdbcDataAdapter
        Dim dt As New DataTable
        Dim sql As String = "SELECT Categories FROM tbldivision WHERE Division LIKE 'ODG'"
        da = New OdbcDataAdapter(sql, modCon.con)
        da.Fill(dt)
        cbSection.Items.Clear()
        cbSection.Items.Add("")
        For x = 0 To dt.Rows.Count - 1
            cbSection.Items.Add(dt.Rows(x)(0))
        Next

    End Sub

    'Load the data from combobox where the division is like reu
    Private Sub loadComboCath()
        Dim da As OdbcDataAdapter
        Dim dt As New DataTable
        Dim sql As String = "SELECT Categories FROM tbldivision WHERE Division LIKE 'REU'"
        da = New OdbcDataAdapter(sql, modCon.con)
        da.Fill(dt)
        cbSection.Items.Clear()
        cbSection.Items.Add("")
        For x = 0 To dt.Rows.Count - 1
            cbSection.Items.Add(dt.Rows(x)(0))
        Next

    End Sub

    'Load the data from combobox where the division is like froo
    Private Sub loadComboCati()

        Dim da As OdbcDataAdapter
        Dim dt As New DataTable
        Dim sql As String = "SELECT Categories FROM tbldivision WHERE Division LIKE 'FROO'"
        da = New OdbcDataAdapter(sql, modCon.con)
        da.Fill(dt)
        cbSection.Items.Clear()
        cbSection.Items.Add("")
        For x = 0 To dt.Rows.Count - 1
            cbSection.Items.Add(dt.Rows(x)(0))
        Next

    End Sub

    'Load the data from combobox where the division is like pps
    Private Sub loadComboCatj()

        Dim da As OdbcDataAdapter
        Dim dt As New DataTable
        Dim sql As String = "SELECT Categories FROM tbldivision WHERE Division LIKE 'PPS'"
        da = New OdbcDataAdapter(sql, modCon.con)
        da.Fill(dt)
        cbSection.Items.Clear()
        cbSection.Items.Add("")
        For x = 0 To dt.Rows.Count - 1
            cbSection.Items.Add(dt.Rows(x)(0))
        Next

    End Sub

    'Clean textbox
    Private Sub cleaner()
        Dim obj As Object
        For Each obj In grpEntry.Controls
            If TypeOf obj Is TextBox Then
                obj.clear()
            End If
        Next
    End Sub

    'Refresh the data from the datagridview
    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        Dim forrequest = "For Request"
        lblClaimby.Text = MDIMain.tslUser.Text
        'Call loadData("SELECT ID,Division,Section,Nature,Categories,Location,Findings,Remarks,Requested,Status,Claimby,Date,Priority,Room FROM tblaccomplish WHERE Status LIKE '%" & forrequest & "%'", dvgstudent)
    End Sub

    'To update the status "request" to "ongoing"
    Private Sub btnOngoing_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOngoing.Click
        Dim forrequest = "For Request"
        Dim status = "Ongoing"
        Dim cmd As Odbc.OdbcCommand
        Dim sql As String
        If Len(dvgstudent.Tag) = 0 Then
            MsgBox("Please Select Query!", vbInformation, "Accomplish Information")
            dvgstudent.Tag = ""
            'Call loadData("SELECT ID,Division,Section,Nature,Categories,Location,Findings,Remarks,Requested,Status,Claimby,Date,Priority,Room FROM tblaccomplish WHERE Status LIKE '%" & forrequest & "%'", dvgstudent)
            Call populate()
            lblClaimby.Text = MDIMain.tslUser.Text

        Else
            sql = "UPDATE tblaccomplish SET Findings = ?, Status = ?, Fk = ?, Claimby = ?, Dateclaim = ? WHERE ID = " & Val(dvgstudent.Tag) & ""
            MsgBox("Successfully Claimed!", vbInformation, "Accomplish Information")
            cmd = New Odbc.OdbcCommand(sql, modCon.con)
            cmd.Parameters.AddWithValue("?", txtFindings.Text)
            cmd.Parameters.AddWithValue("?", status)
            cmd.Parameters.AddWithValue("?", MDIMain.tslnum.Text)
            cmd.Parameters.AddWithValue("?", lblClaimby.Text)
            cmd.Parameters.AddWithValue("?", Date.Now.ToString("dd-MM-yyyy") + "  |  " + Date.Now.ToLongTimeString())
            cmd.ExecuteNonQuery()
            'Call loadData("SELECT ID,Division,Section,Nature,Categories,Location,Findings,Remarks,Requested,Status,Claimby,Date,Priority,Room FROM tblaccomplish WHERE Status LIKE '%" & forrequest & "%'", dvgstudent)
            Call populate()
            dvgstudent.Tag = ""
            lblClaimby.Text = MDIMain.tslUser.Text
        End If
        Call cleaner()
        cbDivision.SelectedIndex = -1
        cmbStatus.SelectedIndex = 2
        txtFindings.Text = ""
        txtRemarks.Text = ""
        dvgstudent.Text = ""
        cbSection.SelectedIndex = -1
        cbLocation.Text = ""
        cbNature.Text = ""
        cbLocation.SelectedIndex = -1
        cbNature.SelectedIndex = -1
        cmbNature.SelectedIndex = -1
        lblClaimby.Text = ""
        txtRoom.Text = ""
    End Sub

    'To select the data from datagrid
    Private Sub dgvStudent_CellClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dvgstudent.CellClick
        If e.RowIndex >= 0 Then
            dvgstudent.Tag = dvgstudent.Item(0, e.RowIndex).Value.ToString()
            cbDivision.Text = dvgstudent.Item(1, e.RowIndex).Value.ToString()
            cbSection.Text = dvgstudent.Item(2, e.RowIndex).Value.ToString()
            cbNature.Text = dvgstudent.Item(3, e.RowIndex).Value.ToString()
            cmbNature.Text = dvgstudent.Item(4, e.RowIndex).Value.ToString()
            cbLocation.Text = dvgstudent.Item(5, e.RowIndex).Value.ToString()
            txtFindings.Text = dvgstudent.Item(6, e.RowIndex).Value.ToString()
            txtRemarks.Text = dvgstudent.Item(7, e.RowIndex).Value.ToString()
            lblRequested.Text = dvgstudent.Item(8, e.RowIndex).Value.ToString()
            cmbStatus.Text = dvgstudent.Item(9, e.RowIndex).Value.ToString()
            lblClaimby.Text = dvgstudent.Item(10, e.RowIndex).Value.ToString()
            txtRoom.Text = dvgstudent.Item(13, e.RowIndex).Value.ToString()

        End If


        Call enabler()
    End Sub


    'PRINT
    'Private Sub Button1_Click(sender As Object, e As EventArgs)
    'frmPrint.ShowDialog()
    'End Sub

End Class