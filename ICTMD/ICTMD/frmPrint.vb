﻿Imports System.Drawing.Printing
Public Class frmPrint
    Private Sub frmPrint_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        'Call loadData("SELECT ID,Division,Section,Nature,Categories,Location,Findings,Remarks,Requested,Date,Status,Claimby,Dateclaim,Datefinish,Priority FROM tblaccomplish WHERE Status = 'Case Closed'", dvgstudent)
        'Fk2 LIKE '%" & MDIMain.tslnum.Text & "%' AND 
    End Sub

    'The printing of the document
    Private bitmap As Bitmap
    Private Sub PrintDocument1_PrintPage(ByVal sender As System.Object, ByVal e As System.Drawing.Printing.PrintPageEventArgs) Handles PrintDocument1.PrintPage
        PrintDialog1.PrinterSettings = PrintDocument1.PrinterSettings
        If PrintDialog1.ShowDialog() = Windows.Forms.DialogResult.OK Then
            PrintDocument1.PrinterSettings = PrintDialog1.PrinterSettings
            Dim pagesetup As New PageSettings
            With pagesetup
                .Margins.Left = 50
                .Margins.Right = 50
                .Margins.Top = 50
                .Margins.Bottom = 50
                .Landscape = True

            End With
            PrintDocument1.DefaultPageSettings = pagesetup
        End If
        PrintPreviewDialog1.Document = PrintDocument1

        Dim bm As New Bitmap(Me.GroupBox1.Width, Me.GroupBox1.Height)

        GroupBox1.DrawToBitmap(bm, New Rectangle(0, 0, Me.GroupBox1.Width, Me.GroupBox1.Height))

        e.Graphics.DrawImage(bm, 0, 0)
    End Sub

    'To print the document
    Private Sub btnPrint_Click(sender As Object, e As EventArgs) Handles btnPrint.Click
        PrintDocument1.Print()
    End Sub

    'Print preview
    Private Sub PrintPreviewDialog1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PrintPreviewDialog1.Load
        PrintDialog1.PrinterSettings = PrintDocument1.PrinterSettings
        If PrintDialog1.ShowDialog() = Windows.Forms.DialogResult.OK Then
            PrintDocument1.PrinterSettings = PrintDialog1.PrinterSettings
            Dim pagesetup As New PageSettings
            With pagesetup
                .Margins.Left = 150
                .Margins.Right = 150
                .Margins.Top = 150
                .Margins.Bottom = 150
                .Landscape = True
            End With
            PrintDocument1.DefaultPageSettings = pagesetup
        End If
        PrintDocument1.DefaultPageSettings.Landscape = True
        PrintPreviewDialog1.Document = PrintDocument1
    End Sub

    'DATAGRIDVIEW CLICK
    'Private Sub dgvStudent_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dvgstudent.CellClick

    'If e.RowIndex >= 0 Then
    'dvgstudent.Tag = dvgstudent.Item(0, e.RowIndex).Value.ToString()
    'Label4.Text = dvgstudent.Item(0, e.RowIndex).Value.ToString()
    'Label5.Text = dvgstudent.Item(9, e.RowIndex).Value.ToString()
    'Label6.Text = dvgstudent.Item(13, e.RowIndex).Value.ToString()
    'Label17.Text = dvgstudent.Item(5, e.RowIndex).Value.ToString()
    'Label21.Text = dvgstudent.Item(1, e.RowIndex).Value.ToString()
    'Label22.Text = dvgstudent.Item(2, e.RowIndex).Value.ToString()
    'Label27.Text = dvgstudent.Item(3, e.RowIndex).Value.ToString()
    'Label28.Text = dvgstudent.Item(4, e.RowIndex).Value.ToString()
    'TextBox1.Text = dvgstudent.Item(6, e.RowIndex).Value.ToString()
    'TextBox2.Text = dvgstudent.Item(6, e.RowIndex).Value.ToString()
    'TextBox3.Text = dvgstudent.Item(7, e.RowIndex).Value.ToString()
    'Label39.Text = dvgstudent.Item(11, e.RowIndex).Value.ToString()
    'Label40.Text = dvgstudent.Item(8, e.RowIndex).Value.ToString()
    'Label41.Text = dvgstudent.Item(8, e.RowIndex).Value.ToString()
    'End If
    'End Sub

    'To show the print preview dialog
    Private Sub btnPrintPreview_Click(sender As Object, e As EventArgs)
        PrintPreviewDialog1.ShowDialog()
    End Sub


End Class