﻿Imports System.Windows.Forms
Imports System.Data.Odbc
Imports Tulpep.NotificationWindow
Public Class MDIMain
    Sub populate()

    End Sub

    Private m_ChildFormNumber As Integer
    'Connection of database
    Private Sub MDIMain_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Call modCon.connect()
        'ACTUAL CLOCK
        Timer1.Start()
    End Sub

    'To get the actual date and time
    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick

        tslData.Text = Date.Now.ToLongDateString + " | " + Date.Now.ToLongTimeString

        Call connect()
        Dim da As New OdbcDataAdapter
        Dim ds As New DataSet

        Dim sql As String = "SELECT count(*) FROM tblaccomplish WHERE Status = 'For Request'"
        da = New OdbcDataAdapter(sql, con)
        da.Fill(ds)

        lblrCount.Text = ds.Tables.Item(0).Rows(0).ItemArray(0).ToString

        If Val(lblrCount.Text) > 0 Then
            lblrCount.BackColor = Color.Red

        Else
            lblrCount.BackColor = Color.LawnGreen
        End If


    End Sub


    'Toolstrip logout button
    Private Sub ToolStripButton2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton2.Click
        If MsgBox("Are you sure you want to Log out?", vbQuestion + vbYesNo, "Exit Program") = vbYes Then
            Me.Close()
            frmJoborder.Close()
            frmLogin.Show()
        End If
    End Sub

    'To exit the mdimain form
    Private Sub btnQuit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Me.Close()
    End Sub

    'Toolstrip for accomplish form
    Private Sub accomplishtoolstripitem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles accomplishtoolstripitem.Click
        For Each ChildForm As Form In Me.MdiChildren
            ChildForm.Close()
        Next
        frmAccomplish.MdiParent = Me
        frmAccomplish.Show()

    End Sub

    'The search toolstrip
    Private Sub SearchStudentToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SearchStudentToolStripMenuItem.Click
        'For Each ChildForm As Form In Me.MdiChildren
        'ChildForm.Close()
        'Next

        'frmSearch.MdiParent = Me
        frmSearch.ShowDialog()

    End Sub

    'Show the user form
    Private Sub UserAccountToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles UserAccountToolStripMenuItem.Click
        frmUser.ShowDialog()
    End Sub

    'Show the nature job form
    Private Sub NatureOfJobToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles NatureOfJobToolStripMenuItem.Click
        frmNature.ShowDialog()
    End Sub

    'Show the location form
    Private Sub ListOfToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ListOfToolStripMenuItem.Click
        frmLocation.ShowDialog()
    End Sub

    'Show the division form
    Private Sub DivisionToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles DivisionToolStripMenuItem.Click
        frmDivision.ShowDialog()
    End Sub

    'Show the request form
    Private Sub RequestToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles RequestToolStripMenuItem.Click
        frmRequest.ShowDialog()
    End Sub

    'Show the joborder form
    Private Sub JobOrderToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles JobOrderToolStripMenuItem.Click
        frmJoborder.Show()
    End Sub

    'Show the print form
    Private Sub PrintToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles PrintToolStripMenuItem.Click
        frmCaseClosed.ShowDialog()
    End Sub

    'Show the backup form
    Private Sub BackupDatabaseToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BackupDatabaseToolStripMenuItem.Click
        frmBackup.ShowDialog()
    End Sub

    'Show the report form
    'Private Sub ReportsToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ReportsToolStripMenuItem.Click
    'frmReport.ShowDialog()
    'End Sub

    Private Sub AccomplishmentReportToolStripMenuItem_Click(sender As Object, e As EventArgs)
        frmAccompReport.ShowDialog()
    End Sub

    Private Sub AccomplishmentToolStripMenuItem_Click(sender As Object, e As EventArgs)
        frmAccompReport1.ShowDialog()
    End Sub

    Private Sub Accomplishment_Click(sender As Object, e As EventArgs) Handles Accomplishment.Click
        frmAccompReport.ShowDialog()
    End Sub

    Private Sub AccompPerDiv_Click(sender As Object, e As EventArgs) Handles AccompPerDiv.Click
        frmAccompReport1.ShowDialog()
    End Sub

    Private Sub AdminReport_Click(sender As Object, e As EventArgs) Handles AdminReport.Click
        frmAdminReport.ShowDialog()
    End Sub
End Class