﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class MDIMain
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(MDIMain))
        Me.StatusStrip = New System.Windows.Forms.StatusStrip()
        Me.ToolStripStatusLabel = New System.Windows.Forms.ToolStripStatusLabel()
        Me.tslUser = New System.Windows.Forms.ToolStripStatusLabel()
        Me.ToolStripStatusLabel1 = New System.Windows.Forms.ToolStripStatusLabel()
        Me.tslPos = New System.Windows.Forms.ToolStripStatusLabel()
        Me.ToolStripStatusLabel2 = New System.Windows.Forms.ToolStripStatusLabel()
        Me.tslData = New System.Windows.Forms.ToolStripStatusLabel()
        Me.tslnum = New System.Windows.Forms.ToolStripStatusLabel()
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.ToolStrip1 = New System.Windows.Forms.ToolStrip()
        Me.ToolStripButton2 = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripDropDownButton1 = New System.Windows.Forms.ToolStripDropDownButton()
        Me.RequestToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.JobOrderToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.tsMaintenance = New System.Windows.Forms.ToolStripDropDownButton()
        Me.UserAccountToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.NatureOfJobToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ListOfToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DivisionToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.BackupDatabaseToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.Reports = New System.Windows.Forms.ToolStripDropDownButton()
        Me.Accomplishment = New System.Windows.Forms.ToolStripMenuItem()
        Me.AccompPerDiv = New System.Windows.Forms.ToolStripMenuItem()
        Me.AdminReport = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripButton1 = New System.Windows.Forms.ToolStripDropDownButton()
        Me.accomplishtoolstripitem = New System.Windows.Forms.ToolStripMenuItem()
        Me.SearchStudentToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PrintToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ReportsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.ToolTip = New System.Windows.Forms.ToolTip(Me.components)
        Me.lblrCount = New System.Windows.Forms.Label()
        Me.Timer2 = New System.Windows.Forms.Timer(Me.components)
        Me.Timer3 = New System.Windows.Forms.Timer(Me.components)
        Me.StatusStrip.SuspendLayout()
        Me.ToolStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'StatusStrip
        '
        Me.StatusStrip.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.StatusStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripStatusLabel, Me.tslUser, Me.ToolStripStatusLabel1, Me.tslPos, Me.ToolStripStatusLabel2, Me.tslData, Me.tslnum})
        Me.StatusStrip.Location = New System.Drawing.Point(0, 436)
        Me.StatusStrip.Name = "StatusStrip"
        Me.StatusStrip.Padding = New System.Windows.Forms.Padding(1, 0, 19, 0)
        Me.StatusStrip.Size = New System.Drawing.Size(1024, 26)
        Me.StatusStrip.TabIndex = 10
        Me.StatusStrip.Text = "StatusStrip"
        '
        'ToolStripStatusLabel
        '
        Me.ToolStripStatusLabel.Name = "ToolStripStatusLabel"
        Me.ToolStripStatusLabel.Size = New System.Drawing.Size(107, 21)
        Me.ToolStripStatusLabel.Text = "Active User :"
        '
        'tslUser
        '
        Me.tslUser.AutoSize = False
        Me.tslUser.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tslUser.Name = "tslUser"
        Me.tslUser.Size = New System.Drawing.Size(330, 21)
        Me.tslUser.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'ToolStripStatusLabel1
        '
        Me.ToolStripStatusLabel1.Name = "ToolStripStatusLabel1"
        Me.ToolStripStatusLabel1.Size = New System.Drawing.Size(76, 21)
        Me.ToolStripStatusLabel1.Text = "Position :"
        '
        'tslPos
        '
        Me.tslPos.AutoSize = False
        Me.tslPos.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tslPos.Name = "tslPos"
        Me.tslPos.Size = New System.Drawing.Size(180, 21)
        Me.tslPos.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'ToolStripStatusLabel2
        '
        Me.ToolStripStatusLabel2.Name = "ToolStripStatusLabel2"
        Me.ToolStripStatusLabel2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.ToolStripStatusLabel2.Size = New System.Drawing.Size(165, 21)
        Me.ToolStripStatusLabel2.Text = "Current Date/Time :"
        '
        'tslData
        '
        Me.tslData.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tslData.Name = "tslData"
        Me.tslData.Size = New System.Drawing.Size(0, 21)
        '
        'tslnum
        '
        Me.tslnum.Name = "tslnum"
        Me.tslnum.Size = New System.Drawing.Size(0, 21)
        Me.tslnum.Visible = False
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        Me.ToolStripSeparator1.Size = New System.Drawing.Size(6, 60)
        '
        'ToolStrip1
        '
        Me.ToolStrip1.AutoSize = False
        Me.ToolStrip1.BackgroundImage = Global.ICTMD.My.Resources.Resources.navbar
        Me.ToolStrip1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.ToolStrip1.Font = New System.Drawing.Font("Century Gothic", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ToolStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me.ToolStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripButton2, Me.ToolStripSeparator1, Me.ToolStripDropDownButton1, Me.tsMaintenance, Me.Reports, Me.ToolStripButton1})
        Me.ToolStrip1.Location = New System.Drawing.Point(0, 0)
        Me.ToolStrip1.Name = "ToolStrip1"
        Me.ToolStrip1.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.ToolStrip1.Size = New System.Drawing.Size(1024, 60)
        Me.ToolStrip1.TabIndex = 11
        Me.ToolStrip1.Text = "ToolStrip1"
        '
        'ToolStripButton2
        '
        Me.ToolStripButton2.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ToolStripButton2.ForeColor = System.Drawing.Color.PeachPuff
        Me.ToolStripButton2.Image = CType(resources.GetObject("ToolStripButton2.Image"), System.Drawing.Image)
        Me.ToolStripButton2.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.ToolStripButton2.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.ToolStripButton2.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButton2.Name = "ToolStripButton2"
        Me.ToolStripButton2.Size = New System.Drawing.Size(65, 57)
        Me.ToolStripButton2.Text = "Logout"
        Me.ToolStripButton2.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.ToolStripButton2.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'ToolStripDropDownButton1
        '
        Me.ToolStripDropDownButton1.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.RequestToolStripMenuItem, Me.JobOrderToolStripMenuItem})
        Me.ToolStripDropDownButton1.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ToolStripDropDownButton1.ForeColor = System.Drawing.Color.White
        Me.ToolStripDropDownButton1.Image = CType(resources.GetObject("ToolStripDropDownButton1.Image"), System.Drawing.Image)
        Me.ToolStripDropDownButton1.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.ToolStripDropDownButton1.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.ToolStripDropDownButton1.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripDropDownButton1.Margin = New System.Windows.Forms.Padding(0, 1, 10, 2)
        Me.ToolStripDropDownButton1.Name = "ToolStripDropDownButton1"
        Me.ToolStripDropDownButton1.Size = New System.Drawing.Size(75, 57)
        Me.ToolStripDropDownButton1.Text = "Queue"
        Me.ToolStripDropDownButton1.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.ToolStripDropDownButton1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'RequestToolStripMenuItem
        '
        Me.RequestToolStripMenuItem.Font = New System.Drawing.Font("Century Gothic", 12.75!)
        Me.RequestToolStripMenuItem.Name = "RequestToolStripMenuItem"
        Me.RequestToolStripMenuItem.Size = New System.Drawing.Size(176, 26)
        Me.RequestToolStripMenuItem.Text = "For Request"
        '
        'JobOrderToolStripMenuItem
        '
        Me.JobOrderToolStripMenuItem.Font = New System.Drawing.Font("Century Gothic", 12.75!)
        Me.JobOrderToolStripMenuItem.Name = "JobOrderToolStripMenuItem"
        Me.JobOrderToolStripMenuItem.Size = New System.Drawing.Size(176, 26)
        Me.JobOrderToolStripMenuItem.Text = "Job Order"
        '
        'tsMaintenance
        '
        Me.tsMaintenance.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.UserAccountToolStripMenuItem, Me.NatureOfJobToolStripMenuItem, Me.ListOfToolStripMenuItem, Me.DivisionToolStripMenuItem, Me.BackupDatabaseToolStripMenuItem})
        Me.tsMaintenance.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tsMaintenance.ForeColor = System.Drawing.Color.White
        Me.tsMaintenance.Image = CType(resources.GetObject("tsMaintenance.Image"), System.Drawing.Image)
        Me.tsMaintenance.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.tsMaintenance.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.tsMaintenance.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.tsMaintenance.Margin = New System.Windows.Forms.Padding(0, 1, 5, 2)
        Me.tsMaintenance.Name = "tsMaintenance"
        Me.tsMaintenance.Size = New System.Drawing.Size(126, 57)
        Me.tsMaintenance.Text = "Maintenance"
        Me.tsMaintenance.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.tsMaintenance.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'UserAccountToolStripMenuItem
        '
        Me.UserAccountToolStripMenuItem.Font = New System.Drawing.Font("Century Gothic", 12.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.UserAccountToolStripMenuItem.Name = "UserAccountToolStripMenuItem"
        Me.UserAccountToolStripMenuItem.Size = New System.Drawing.Size(221, 26)
        Me.UserAccountToolStripMenuItem.Text = "User Account"
        '
        'NatureOfJobToolStripMenuItem
        '
        Me.NatureOfJobToolStripMenuItem.Font = New System.Drawing.Font("Century Gothic", 12.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NatureOfJobToolStripMenuItem.Name = "NatureOfJobToolStripMenuItem"
        Me.NatureOfJobToolStripMenuItem.Size = New System.Drawing.Size(221, 26)
        Me.NatureOfJobToolStripMenuItem.Text = "Nature of Job"
        '
        'ListOfToolStripMenuItem
        '
        Me.ListOfToolStripMenuItem.Font = New System.Drawing.Font("Century Gothic", 12.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ListOfToolStripMenuItem.Name = "ListOfToolStripMenuItem"
        Me.ListOfToolStripMenuItem.Size = New System.Drawing.Size(221, 26)
        Me.ListOfToolStripMenuItem.Text = "Location of Job"
        '
        'DivisionToolStripMenuItem
        '
        Me.DivisionToolStripMenuItem.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DivisionToolStripMenuItem.Name = "DivisionToolStripMenuItem"
        Me.DivisionToolStripMenuItem.Size = New System.Drawing.Size(221, 26)
        Me.DivisionToolStripMenuItem.Text = "Center / Office"
        '
        'BackupDatabaseToolStripMenuItem
        '
        Me.BackupDatabaseToolStripMenuItem.Font = New System.Drawing.Font("Century Gothic", 12.0!)
        Me.BackupDatabaseToolStripMenuItem.Name = "BackupDatabaseToolStripMenuItem"
        Me.BackupDatabaseToolStripMenuItem.Size = New System.Drawing.Size(221, 26)
        Me.BackupDatabaseToolStripMenuItem.Text = "Backup Database"
        '
        'Reports
        '
        Me.Reports.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.Accomplishment, Me.AccompPerDiv, Me.AdminReport})
        Me.Reports.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Reports.ForeColor = System.Drawing.Color.White
        Me.Reports.Image = CType(resources.GetObject("Reports.Image"), System.Drawing.Image)
        Me.Reports.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.Reports.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.Reports.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.Reports.Margin = New System.Windows.Forms.Padding(0, 1, 5, 2)
        Me.Reports.Name = "Reports"
        Me.Reports.Size = New System.Drawing.Size(77, 57)
        Me.Reports.Text = "Reports"
        Me.Reports.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.Reports.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'Accomplishment
        '
        Me.Accomplishment.Font = New System.Drawing.Font("Century Gothic", 12.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Accomplishment.Name = "Accomplishment"
        Me.Accomplishment.Size = New System.Drawing.Size(317, 26)
        Me.Accomplishment.Text = "Accomplishment"
        '
        'AccompPerDiv
        '
        Me.AccompPerDiv.Font = New System.Drawing.Font("Century Gothic", 12.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.AccompPerDiv.Name = "AccompPerDiv"
        Me.AccompPerDiv.Size = New System.Drawing.Size(317, 26)
        Me.AccompPerDiv.Text = "Accomplishment per Division"
        '
        'AdminReport
        '
        Me.AdminReport.Font = New System.Drawing.Font("Century Gothic", 12.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.AdminReport.Name = "AdminReport"
        Me.AdminReport.Size = New System.Drawing.Size(317, 26)
        Me.AdminReport.Text = "Admin Report"
        '
        'ToolStripButton1
        '
        Me.ToolStripButton1.BackColor = System.Drawing.SystemColors.Control
        Me.ToolStripButton1.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.accomplishtoolstripitem, Me.SearchStudentToolStripMenuItem, Me.PrintToolStripMenuItem, Me.ReportsToolStripMenuItem})
        Me.ToolStripButton1.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ToolStripButton1.ForeColor = System.Drawing.Color.White
        Me.ToolStripButton1.Image = CType(resources.GetObject("ToolStripButton1.Image"), System.Drawing.Image)
        Me.ToolStripButton1.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.ToolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButton1.Margin = New System.Windows.Forms.Padding(0, 1, 5, 2)
        Me.ToolStripButton1.Name = "ToolStripButton1"
        Me.ToolStripButton1.Size = New System.Drawing.Size(165, 57)
        Me.ToolStripButton1.Text = "Administrator Tools"
        Me.ToolStripButton1.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.ToolStripButton1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'accomplishtoolstripitem
        '
        Me.accomplishtoolstripitem.Font = New System.Drawing.Font("Century Gothic", 12.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.accomplishtoolstripitem.Name = "accomplishtoolstripitem"
        Me.accomplishtoolstripitem.Size = New System.Drawing.Size(230, 26)
        Me.accomplishtoolstripitem.Text = "Ongoing Requests"
        '
        'SearchStudentToolStripMenuItem
        '
        Me.SearchStudentToolStripMenuItem.Font = New System.Drawing.Font("Century Gothic", 12.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SearchStudentToolStripMenuItem.Name = "SearchStudentToolStripMenuItem"
        Me.SearchStudentToolStripMenuItem.Size = New System.Drawing.Size(230, 26)
        Me.SearchStudentToolStripMenuItem.Text = "Request Data"
        '
        'PrintToolStripMenuItem
        '
        Me.PrintToolStripMenuItem.Font = New System.Drawing.Font("Century Gothic", 12.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PrintToolStripMenuItem.Name = "PrintToolStripMenuItem"
        Me.PrintToolStripMenuItem.Size = New System.Drawing.Size(230, 26)
        Me.PrintToolStripMenuItem.Text = "Job Orders Form"
        '
        'ReportsToolStripMenuItem
        '
        Me.ReportsToolStripMenuItem.Font = New System.Drawing.Font("Century Gothic", 12.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ReportsToolStripMenuItem.Name = "ReportsToolStripMenuItem"
        Me.ReportsToolStripMenuItem.Size = New System.Drawing.Size(230, 26)
        Me.ReportsToolStripMenuItem.Text = "Reports"
        '
        'Timer1
        '
        Me.Timer1.Interval = 1000
        '
        'lblrCount
        '
        Me.lblrCount.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblrCount.AutoSize = True
        Me.lblrCount.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.lblrCount.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblrCount.Location = New System.Drawing.Point(932, 9)
        Me.lblrCount.Name = "lblrCount"
        Me.lblrCount.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblrCount.Size = New System.Drawing.Size(15, 16)
        Me.lblrCount.TabIndex = 13
        Me.lblrCount.Text = "1"
        '
        'MDIMain
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackgroundImage = Global.ICTMD.My.Resources.Resources.bgbg
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.ClientSize = New System.Drawing.Size(1024, 462)
        Me.ControlBox = False
        Me.Controls.Add(Me.lblrCount)
        Me.Controls.Add(Me.StatusStrip)
        Me.Controls.Add(Me.ToolStrip1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.IsMdiContainer = True
        Me.Name = "MDIMain"
        Me.ShowIcon = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "FOOD AND DRUG ADMINISTRATION"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.StatusStrip.ResumeLayout(False)
        Me.StatusStrip.PerformLayout()
        Me.ToolStrip1.ResumeLayout(False)
        Me.ToolStrip1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents StatusStrip As System.Windows.Forms.StatusStrip
    Friend WithEvents ToolStripStatusLabel As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents tslUser As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents ToolStripStatusLabel1 As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents tslPos As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents ToolStripStatusLabel2 As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents tslData As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents ToolStripSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents SearchStudentToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsMaintenance As System.Windows.Forms.ToolStripDropDownButton
    Friend WithEvents UserAccountToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents accomplishtoolstripitem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripButton1 As System.Windows.Forms.ToolStripDropDownButton
    Friend WithEvents ToolStrip1 As System.Windows.Forms.ToolStrip
    Friend WithEvents ToolStripButton2 As System.Windows.Forms.ToolStripButton
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents ToolTip As System.Windows.Forms.ToolTip
    Friend WithEvents NatureOfJobToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ListOfToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tslnum As ToolStripStatusLabel
    Friend WithEvents DivisionToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ToolStripDropDownButton1 As ToolStripDropDownButton
    Friend WithEvents RequestToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents JobOrderToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PrintToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents BackupDatabaseToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents lblrCount As Label
    Friend WithEvents Timer2 As Timer
    Friend WithEvents Timer3 As Timer
    Friend WithEvents Reports As ToolStripDropDownButton
    Friend WithEvents Accomplishment As ToolStripMenuItem
    Friend WithEvents AccompPerDiv As ToolStripMenuItem
    Friend WithEvents AdminReport As ToolStripMenuItem
    Friend WithEvents ReportsToolStripMenuItem As ToolStripMenuItem
End Class
