﻿Imports System.Data.Odbc
Imports System.Globalization

Class frmCaseClosed

    Sub populate()
        Call loadData("SELECT ID,Division,Section,Nature,Categories,Location,Findings,Remarks,Requested,Date,Status,Claimby,Dateclaim,Datefinish,Doneby FROM tblaccomplish WHERE Fk = '" & MDIMain.tslnum.Text & "' AND Status = 'Done' AND Deleted = 'N' ORDER BY ID ASC", dvgstudent)
    End Sub
    Private Sub frmCaseClosed_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Call populate()
    End Sub

    'To select data from datagridview
    Private Sub dgvStudent_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dvgstudent.CellClick
        Dim form As New frmPrint

        If e.RowIndex >= 0 Then
            'form.dvgstudent.Tag = dvgstudent.Item(0, e.RowIndex).Value.ToString()
            form.lblControlNum.Text = dvgstudent.Item(0, e.RowIndex).Value.ToString()
            form.lblDateRequested.Text = dvgstudent.Item(9, e.RowIndex).Value.ToString()
            form.lblDateFinished.Text = dvgstudent.Item(13, e.RowIndex).Value.ToString()
            form.lblLocation.Text = dvgstudent.Item(5, e.RowIndex).Value.ToString()
            form.lblCenter.Text = dvgstudent.Item(1, e.RowIndex).Value.ToString()
            form.lblDivision.Text = dvgstudent.Item(2, e.RowIndex).Value.ToString()
            form.lblCategories.Text = dvgstudent.Item(3, e.RowIndex).Value.ToString()
            form.lblNature.Text = dvgstudent.Item(4, e.RowIndex).Value.ToString()
            form.txtFindings.Text = dvgstudent.Item(6, e.RowIndex).Value.ToString()
            form.txtRecommendation.Text = dvgstudent.Item(6, e.RowIndex).Value.ToString()
            form.txtRemarks.Text = dvgstudent.Item(7, e.RowIndex).Value.ToString()
            form.lblWorkmen.Text = dvgstudent.Item(11, e.RowIndex).Value.ToString()
            form.lblRequestedby.Text = dvgstudent.Item(8, e.RowIndex).Value.ToString()
            form.lblWorkCompletion.Text = dvgstudent.Item(8, e.RowIndex).Value.ToString()

            form.copylblControlNum.Text = dvgstudent.Item(0, e.RowIndex).Value.ToString()
            form.copylblDateRequested.Text = dvgstudent.Item(9, e.RowIndex).Value.ToString()
            form.copylblDateFinished.Text = dvgstudent.Item(13, e.RowIndex).Value.ToString()
            form.copyLblLocation.Text = dvgstudent.Item(5, e.RowIndex).Value.ToString()
            form.copylblCenter.Text = dvgstudent.Item(1, e.RowIndex).Value.ToString()
            form.copylblDivision.Text = dvgstudent.Item(2, e.RowIndex).Value.ToString()
            form.copylblCategories.Text = dvgstudent.Item(3, e.RowIndex).Value.ToString()
            form.copylblNature.Text = dvgstudent.Item(4, e.RowIndex).Value.ToString()
            form.copytxtFindings.Text = dvgstudent.Item(6, e.RowIndex).Value.ToString()
            form.copytxtRecommendation.Text = dvgstudent.Item(6, e.RowIndex).Value.ToString()
            form.copytxtRemarks.Text = dvgstudent.Item(7, e.RowIndex).Value.ToString()
            form.copylblWorkmen.Text = dvgstudent.Item(11, e.RowIndex).Value.ToString()
            form.copylblRequestedby.Text = dvgstudent.Item(8, e.RowIndex).Value.ToString()
            form.copylblWorkCompletion.Text = dvgstudent.Item(8, e.RowIndex).Value.ToString()

            form.ShowDialog()
        End If
    End Sub

    'Color coded data based in their status
    Private Sub dvgstudent_CellFormatting(ByVal sender As Object, ByVal e As DataGridViewCellFormattingEventArgs) Handles dvgstudent.CellFormatting
        For i As Integer = 0 To Me.dvgstudent.Rows.Count - 1
            If Me.dvgstudent.Rows(i).Cells("Column11").Value = "Done" Then
                Me.dvgstudent.Rows(i).Cells("Column1").Style.ForeColor = Color.Green
                Me.dvgstudent.Rows(i).Cells("Column2").Style.ForeColor = Color.Green
                Me.dvgstudent.Rows(i).Cells("Column3").Style.ForeColor = Color.Green
                Me.dvgstudent.Rows(i).Cells("Column4").Style.ForeColor = Color.Green
                Me.dvgstudent.Rows(i).Cells("Column5").Style.ForeColor = Color.Green
                Me.dvgstudent.Rows(i).Cells("Column6").Style.ForeColor = Color.Green
                Me.dvgstudent.Rows(i).Cells("Column7").Style.ForeColor = Color.Green
                Me.dvgstudent.Rows(i).Cells("Column8").Style.ForeColor = Color.Green
                Me.dvgstudent.Rows(i).Cells("Column9").Style.ForeColor = Color.Green
                Me.dvgstudent.Rows(i).Cells("Column10").Style.ForeColor = Color.Green
                Me.dvgstudent.Rows(i).Cells("Column11").Style.ForeColor = Color.Green
                Me.dvgstudent.Rows(i).Cells("Column12").Style.ForeColor = Color.Green
                Me.dvgstudent.Rows(i).Cells("Column13").Style.ForeColor = Color.Green
                Me.dvgstudent.Rows(i).Cells("Column14").Style.ForeColor = Color.Green
                Me.dvgstudent.Rows(i).Cells("Column15").Style.ForeColor = Color.Green
            End If
        Next
    End Sub

    'To exit the case closed form
    Private Sub btnClose_Click(sender As Object, e As EventArgs) Handles btnClose.Click
        If MsgBox("Are you sure you want to close?", vbQuestion + vbYesNo, "PRINT") = vbYes Then
            txtSearch.Clear()
            Me.Hide()
        End If
    End Sub

    Private Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch.Click
        Call connect()
        Dim da As OdbcDataAdapter
        Dim dt As New DataTable
        Dim search As String
        Dim Sql As String = "SELECT Requested FROM tblaccomplish WHERE Requested = '" & txtSearch.Text & "' "
        da = New OdbcDataAdapter(Sql, modCon.con)
        da.Fill(dt)

        search = dt.Rows.Count = 1
        Dim search1 = txtSearch.Text.Count = 1

        Try
            If txtSearch.Text = "" Then
                MsgBox("Enter your Query", vbInformation, "Information")
                Call populate()
            ElseIf dt.Rows.Count = 0 Then
                MsgBox("No Data Found", vbInformation, "Information")
                Call populate()
            ElseIf search = search1 Then
                Call loadData("SELECT ID,Division,Section,Nature,Categories,Location,Findings,Remarks,Requested,Date,Status,Claimby,Dateclaim,Datefinish,Doneby FROM tblaccomplish WHERE Fk = '" & MDIMain.tslnum.Text & "' AND Status = 'Done' AND Requested = '" & txtSearch.Text & "' AND Deleted = 'N' ORDER BY ID ASC", dvgstudent)
            End If

        Catch ex As Exception
        End Try
    End Sub

    Private Sub btnRefresh_Click(sender As Object, e As EventArgs) Handles btnRefresh.Click
        txtSearch.Clear()
        Call populate()
    End Sub
End Class