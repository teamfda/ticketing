﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmPrint
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmPrint))
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.copylblWorkCompletion = New System.Windows.Forms.Label()
        Me.copylblRequestedby = New System.Windows.Forms.Label()
        Me.copylblWorkmen = New System.Windows.Forms.Label()
        Me.Label46 = New System.Windows.Forms.Label()
        Me.Label47 = New System.Windows.Forms.Label()
        Me.Label48 = New System.Windows.Forms.Label()
        Me.Label49 = New System.Windows.Forms.Label()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.Label50 = New System.Windows.Forms.Label()
        Me.Label51 = New System.Windows.Forms.Label()
        Me.copytxtRemarks = New System.Windows.Forms.TextBox()
        Me.Label52 = New System.Windows.Forms.Label()
        Me.copytxtRecommendation = New System.Windows.Forms.TextBox()
        Me.Label53 = New System.Windows.Forms.Label()
        Me.copytxtFindings = New System.Windows.Forms.TextBox()
        Me.Label54 = New System.Windows.Forms.Label()
        Me.copylblNature = New System.Windows.Forms.Label()
        Me.copylblCategories = New System.Windows.Forms.Label()
        Me.Label57 = New System.Windows.Forms.Label()
        Me.Label58 = New System.Windows.Forms.Label()
        Me.Label59 = New System.Windows.Forms.Label()
        Me.copylblDivision = New System.Windows.Forms.Label()
        Me.copylblCenter = New System.Windows.Forms.Label()
        Me.Label62 = New System.Windows.Forms.Label()
        Me.Label63 = New System.Windows.Forms.Label()
        Me.copyLblLocation = New System.Windows.Forms.Label()
        Me.Label65 = New System.Windows.Forms.Label()
        Me.Label66 = New System.Windows.Forms.Label()
        Me.Label67 = New System.Windows.Forms.Label()
        Me.Label68 = New System.Windows.Forms.Label()
        Me.Label69 = New System.Windows.Forms.Label()
        Me.copylblControlNum = New System.Windows.Forms.Label()
        Me.Label71 = New System.Windows.Forms.Label()
        Me.Label72 = New System.Windows.Forms.Label()
        Me.Label73 = New System.Windows.Forms.Label()
        Me.Label74 = New System.Windows.Forms.Label()
        Me.Button4 = New System.Windows.Forms.Button()
        Me.Label75 = New System.Windows.Forms.Label()
        Me.PictureBox2 = New System.Windows.Forms.PictureBox()
        Me.Label76 = New System.Windows.Forms.Label()
        Me.Label77 = New System.Windows.Forms.Label()
        Me.copylblDateFinished = New System.Windows.Forms.Label()
        Me.copylblDateRequested = New System.Windows.Forms.Label()
        Me.Label80 = New System.Windows.Forms.Label()
        Me.Label42 = New System.Windows.Forms.Label()
        Me.lblWorkCompletion = New System.Windows.Forms.Label()
        Me.lblRequestedby = New System.Windows.Forms.Label()
        Me.lblWorkmen = New System.Windows.Forms.Label()
        Me.Label38 = New System.Windows.Forms.Label()
        Me.Label36 = New System.Windows.Forms.Label()
        Me.Label37 = New System.Windows.Forms.Label()
        Me.Label34 = New System.Windows.Forms.Label()
        Me.Label35 = New System.Windows.Forms.Label()
        Me.Label33 = New System.Windows.Forms.Label()
        Me.Label32 = New System.Windows.Forms.Label()
        Me.txtRemarks = New System.Windows.Forms.TextBox()
        Me.Label31 = New System.Windows.Forms.Label()
        Me.txtRecommendation = New System.Windows.Forms.TextBox()
        Me.Label30 = New System.Windows.Forms.Label()
        Me.txtFindings = New System.Windows.Forms.TextBox()
        Me.Label29 = New System.Windows.Forms.Label()
        Me.lblNature = New System.Windows.Forms.Label()
        Me.lblCategories = New System.Windows.Forms.Label()
        Me.Label26 = New System.Windows.Forms.Label()
        Me.Label25 = New System.Windows.Forms.Label()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.lblDivision = New System.Windows.Forms.Label()
        Me.lblCenter = New System.Windows.Forms.Label()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.lblLocation = New System.Windows.Forms.Label()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.lblDateFinished = New System.Windows.Forms.Label()
        Me.lblDateRequested = New System.Windows.Forms.Label()
        Me.lblControlNum = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.ShapeContainer1 = New Microsoft.VisualBasic.PowerPacks.ShapeContainer()
        Me.LineShape12 = New Microsoft.VisualBasic.PowerPacks.LineShape()
        Me.LineShape11 = New Microsoft.VisualBasic.PowerPacks.LineShape()
        Me.LineShape10 = New Microsoft.VisualBasic.PowerPacks.LineShape()
        Me.RectangleShape2 = New Microsoft.VisualBasic.PowerPacks.RectangleShape()
        Me.LineShape9 = New Microsoft.VisualBasic.PowerPacks.LineShape()
        Me.LineShape8 = New Microsoft.VisualBasic.PowerPacks.LineShape()
        Me.LineShape7 = New Microsoft.VisualBasic.PowerPacks.LineShape()
        Me.LineShape6 = New Microsoft.VisualBasic.PowerPacks.LineShape()
        Me.LineShape5 = New Microsoft.VisualBasic.PowerPacks.LineShape()
        Me.LineShape4 = New Microsoft.VisualBasic.PowerPacks.LineShape()
        Me.LineShape3 = New Microsoft.VisualBasic.PowerPacks.LineShape()
        Me.LineShape2 = New Microsoft.VisualBasic.PowerPacks.LineShape()
        Me.LineShape1 = New Microsoft.VisualBasic.PowerPacks.LineShape()
        Me.RectangleShape1 = New Microsoft.VisualBasic.PowerPacks.RectangleShape()
        Me.PrintDocument1 = New System.Drawing.Printing.PrintDocument()
        Me.PrintPreviewDialog1 = New System.Windows.Forms.PrintPreviewDialog()
        Me.PrintDialog1 = New System.Windows.Forms.PrintDialog()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.PrintDocument2 = New System.Drawing.Printing.PrintDocument()
        Me.Timer2 = New System.Windows.Forms.Timer(Me.components)
        Me.PrintPreviewDialog2 = New System.Windows.Forms.PrintPreviewDialog()
        Me.PrintDialog2 = New System.Windows.Forms.PrintDialog()
        Me.btnPrint = New System.Windows.Forms.Button()
        Me.GroupBox1.SuspendLayout()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.BackColor = System.Drawing.Color.White
        Me.GroupBox1.Controls.Add(Me.Label20)
        Me.GroupBox1.Controls.Add(Me.Label23)
        Me.GroupBox1.Controls.Add(Me.copylblWorkCompletion)
        Me.GroupBox1.Controls.Add(Me.copylblRequestedby)
        Me.GroupBox1.Controls.Add(Me.copylblWorkmen)
        Me.GroupBox1.Controls.Add(Me.Label46)
        Me.GroupBox1.Controls.Add(Me.Label47)
        Me.GroupBox1.Controls.Add(Me.Label48)
        Me.GroupBox1.Controls.Add(Me.Label49)
        Me.GroupBox1.Controls.Add(Me.Button3)
        Me.GroupBox1.Controls.Add(Me.Label50)
        Me.GroupBox1.Controls.Add(Me.Label51)
        Me.GroupBox1.Controls.Add(Me.copytxtRemarks)
        Me.GroupBox1.Controls.Add(Me.Label52)
        Me.GroupBox1.Controls.Add(Me.copytxtRecommendation)
        Me.GroupBox1.Controls.Add(Me.Label53)
        Me.GroupBox1.Controls.Add(Me.copytxtFindings)
        Me.GroupBox1.Controls.Add(Me.Label54)
        Me.GroupBox1.Controls.Add(Me.copylblNature)
        Me.GroupBox1.Controls.Add(Me.copylblCategories)
        Me.GroupBox1.Controls.Add(Me.Label57)
        Me.GroupBox1.Controls.Add(Me.Label58)
        Me.GroupBox1.Controls.Add(Me.Label59)
        Me.GroupBox1.Controls.Add(Me.copylblDivision)
        Me.GroupBox1.Controls.Add(Me.copylblCenter)
        Me.GroupBox1.Controls.Add(Me.Label62)
        Me.GroupBox1.Controls.Add(Me.Label63)
        Me.GroupBox1.Controls.Add(Me.copyLblLocation)
        Me.GroupBox1.Controls.Add(Me.Label65)
        Me.GroupBox1.Controls.Add(Me.Label66)
        Me.GroupBox1.Controls.Add(Me.Label67)
        Me.GroupBox1.Controls.Add(Me.Label68)
        Me.GroupBox1.Controls.Add(Me.Label69)
        Me.GroupBox1.Controls.Add(Me.copylblControlNum)
        Me.GroupBox1.Controls.Add(Me.Label71)
        Me.GroupBox1.Controls.Add(Me.Label72)
        Me.GroupBox1.Controls.Add(Me.Label73)
        Me.GroupBox1.Controls.Add(Me.Label74)
        Me.GroupBox1.Controls.Add(Me.Button4)
        Me.GroupBox1.Controls.Add(Me.Label75)
        Me.GroupBox1.Controls.Add(Me.PictureBox2)
        Me.GroupBox1.Controls.Add(Me.Label76)
        Me.GroupBox1.Controls.Add(Me.Label77)
        Me.GroupBox1.Controls.Add(Me.copylblDateFinished)
        Me.GroupBox1.Controls.Add(Me.copylblDateRequested)
        Me.GroupBox1.Controls.Add(Me.Label80)
        Me.GroupBox1.Controls.Add(Me.Label42)
        Me.GroupBox1.Controls.Add(Me.lblWorkCompletion)
        Me.GroupBox1.Controls.Add(Me.lblRequestedby)
        Me.GroupBox1.Controls.Add(Me.lblWorkmen)
        Me.GroupBox1.Controls.Add(Me.Label38)
        Me.GroupBox1.Controls.Add(Me.Label36)
        Me.GroupBox1.Controls.Add(Me.Label37)
        Me.GroupBox1.Controls.Add(Me.Label34)
        Me.GroupBox1.Controls.Add(Me.Label35)
        Me.GroupBox1.Controls.Add(Me.Label33)
        Me.GroupBox1.Controls.Add(Me.Label32)
        Me.GroupBox1.Controls.Add(Me.txtRemarks)
        Me.GroupBox1.Controls.Add(Me.Label31)
        Me.GroupBox1.Controls.Add(Me.txtRecommendation)
        Me.GroupBox1.Controls.Add(Me.Label30)
        Me.GroupBox1.Controls.Add(Me.txtFindings)
        Me.GroupBox1.Controls.Add(Me.Label29)
        Me.GroupBox1.Controls.Add(Me.lblNature)
        Me.GroupBox1.Controls.Add(Me.lblCategories)
        Me.GroupBox1.Controls.Add(Me.Label26)
        Me.GroupBox1.Controls.Add(Me.Label25)
        Me.GroupBox1.Controls.Add(Me.Label24)
        Me.GroupBox1.Controls.Add(Me.lblDivision)
        Me.GroupBox1.Controls.Add(Me.lblCenter)
        Me.GroupBox1.Controls.Add(Me.Label19)
        Me.GroupBox1.Controls.Add(Me.Label18)
        Me.GroupBox1.Controls.Add(Me.lblLocation)
        Me.GroupBox1.Controls.Add(Me.Label16)
        Me.GroupBox1.Controls.Add(Me.Label15)
        Me.GroupBox1.Controls.Add(Me.Label14)
        Me.GroupBox1.Controls.Add(Me.Label13)
        Me.GroupBox1.Controls.Add(Me.Label12)
        Me.GroupBox1.Controls.Add(Me.lblDateFinished)
        Me.GroupBox1.Controls.Add(Me.lblDateRequested)
        Me.GroupBox1.Controls.Add(Me.lblControlNum)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.PictureBox1)
        Me.GroupBox1.Controls.Add(Me.Label11)
        Me.GroupBox1.Controls.Add(Me.Label10)
        Me.GroupBox1.Controls.Add(Me.Label9)
        Me.GroupBox1.Controls.Add(Me.Label8)
        Me.GroupBox1.Controls.Add(Me.Label7)
        Me.GroupBox1.Controls.Add(Me.ShapeContainer1)
        Me.GroupBox1.Location = New System.Drawing.Point(2, 2)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(1245, 780)
        Me.GroupBox1.TabIndex = 158
        Me.GroupBox1.TabStop = False
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Location = New System.Drawing.Point(607, 537)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(140, 13)
        Me.Label20.TabIndex = 230
        Me.Label20.Text = "Signature over printed name"
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.Font = New System.Drawing.Font("Times New Roman", 11.25!, System.Drawing.FontStyle.Bold)
        Me.Label23.Location = New System.Drawing.Point(550, 563)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(134, 17)
        Me.Label23.TabIndex = 231
        Me.Label23.Text = "REQUESTED BY :"
        '
        'copylblWorkCompletion
        '
        Me.copylblWorkCompletion.AutoSize = True
        Me.copylblWorkCompletion.Location = New System.Drawing.Point(611, 669)
        Me.copylblWorkCompletion.Name = "copylblWorkCompletion"
        Me.copylblWorkCompletion.Size = New System.Drawing.Size(0, 13)
        Me.copylblWorkCompletion.TabIndex = 238
        '
        'copylblRequestedby
        '
        Me.copylblRequestedby.AutoSize = True
        Me.copylblRequestedby.Location = New System.Drawing.Point(611, 587)
        Me.copylblRequestedby.Name = "copylblRequestedby"
        Me.copylblRequestedby.Size = New System.Drawing.Size(0, 13)
        Me.copylblRequestedby.TabIndex = 237
        '
        'copylblWorkmen
        '
        Me.copylblWorkmen.AutoSize = True
        Me.copylblWorkmen.Location = New System.Drawing.Point(609, 515)
        Me.copylblWorkmen.Name = "copylblWorkmen"
        Me.copylblWorkmen.Size = New System.Drawing.Size(0, 13)
        Me.copylblWorkmen.TabIndex = 236
        '
        'Label46
        '
        Me.Label46.AutoSize = True
        Me.Label46.Location = New System.Drawing.Point(950, 679)
        Me.Label46.Name = "Label46"
        Me.Label46.Size = New System.Drawing.Size(73, 13)
        Me.Label46.TabIndex = 235
        Me.Label46.Text = "ICTMD COPY"
        '
        'Label47
        '
        Me.Label47.AutoSize = True
        Me.Label47.Location = New System.Drawing.Point(607, 690)
        Me.Label47.Name = "Label47"
        Me.Label47.Size = New System.Drawing.Size(140, 13)
        Me.Label47.TabIndex = 234
        Me.Label47.Text = "Signature over printed name"
        '
        'Label48
        '
        Me.Label48.AutoSize = True
        Me.Label48.Font = New System.Drawing.Font("Times New Roman", 11.25!, System.Drawing.FontStyle.Bold)
        Me.Label48.Location = New System.Drawing.Point(552, 628)
        Me.Label48.Name = "Label48"
        Me.Label48.Size = New System.Drawing.Size(293, 17)
        Me.Label48.TabIndex = 233
        Me.Label48.Text = "WORK COMPLETION CERTIFICATION"
        '
        'Label49
        '
        Me.Label49.AutoSize = True
        Me.Label49.Font = New System.Drawing.Font("Times New Roman", 11.25!, System.Drawing.FontStyle.Bold)
        Me.Label49.Location = New System.Drawing.Point(550, 493)
        Me.Label49.Name = "Label49"
        Me.Label49.Size = New System.Drawing.Size(269, 17)
        Me.Label49.TabIndex = 229
        Me.Label49.Text = "WORKMEN ASSIGNED TO THE JOB"
        '
        'Button3
        '
        Me.Button3.Location = New System.Drawing.Point(1850, 472)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(78, 41)
        Me.Button3.TabIndex = 205
        Me.Button3.Text = "Print"
        Me.Button3.UseVisualStyleBackColor = True
        '
        'Label50
        '
        Me.Label50.AutoSize = True
        Me.Label50.Location = New System.Drawing.Point(550, 648)
        Me.Label50.Name = "Label50"
        Me.Label50.Size = New System.Drawing.Size(355, 13)
        Me.Label50.TabIndex = 239
        Me.Label50.Text = "This is to certify that the above job order has been completed satisfactorily"
        '
        'Label51
        '
        Me.Label51.AutoSize = True
        Me.Label51.Location = New System.Drawing.Point(607, 607)
        Me.Label51.Name = "Label51"
        Me.Label51.Size = New System.Drawing.Size(140, 13)
        Me.Label51.TabIndex = 232
        Me.Label51.Text = "Signature over printed name"
        '
        'copytxtRemarks
        '
        Me.copytxtRemarks.BackColor = System.Drawing.SystemColors.Window
        Me.copytxtRemarks.Enabled = False
        Me.copytxtRemarks.Location = New System.Drawing.Point(559, 457)
        Me.copytxtRemarks.Multiline = True
        Me.copytxtRemarks.Name = "copytxtRemarks"
        Me.copytxtRemarks.Size = New System.Drawing.Size(464, 35)
        Me.copytxtRemarks.TabIndex = 228
        '
        'Label52
        '
        Me.Label52.AutoSize = True
        Me.Label52.Font = New System.Drawing.Font("Times New Roman", 11.25!, System.Drawing.FontStyle.Bold)
        Me.Label52.Location = New System.Drawing.Point(554, 436)
        Me.Label52.Name = "Label52"
        Me.Label52.Size = New System.Drawing.Size(95, 17)
        Me.Label52.TabIndex = 227
        Me.Label52.Text = "REMARKS :"
        '
        'copytxtRecommendation
        '
        Me.copytxtRecommendation.BackColor = System.Drawing.SystemColors.Window
        Me.copytxtRecommendation.Enabled = False
        Me.copytxtRecommendation.Location = New System.Drawing.Point(558, 398)
        Me.copytxtRecommendation.Multiline = True
        Me.copytxtRecommendation.Name = "copytxtRecommendation"
        Me.copytxtRecommendation.Size = New System.Drawing.Size(464, 35)
        Me.copytxtRecommendation.TabIndex = 226
        '
        'Label53
        '
        Me.Label53.AutoSize = True
        Me.Label53.Font = New System.Drawing.Font("Times New Roman", 11.25!, System.Drawing.FontStyle.Bold)
        Me.Label53.Location = New System.Drawing.Point(553, 377)
        Me.Label53.Name = "Label53"
        Me.Label53.Size = New System.Drawing.Size(165, 17)
        Me.Label53.TabIndex = 225
        Me.Label53.Text = "RECOMENDATIONS :"
        '
        'copytxtFindings
        '
        Me.copytxtFindings.BackColor = System.Drawing.SystemColors.Window
        Me.copytxtFindings.Enabled = False
        Me.copytxtFindings.Location = New System.Drawing.Point(557, 339)
        Me.copytxtFindings.Multiline = True
        Me.copytxtFindings.Name = "copytxtFindings"
        Me.copytxtFindings.Size = New System.Drawing.Size(464, 35)
        Me.copytxtFindings.TabIndex = 224
        '
        'Label54
        '
        Me.Label54.AutoSize = True
        Me.Label54.Font = New System.Drawing.Font("Times New Roman", 11.25!, System.Drawing.FontStyle.Bold)
        Me.Label54.Location = New System.Drawing.Point(552, 319)
        Me.Label54.Name = "Label54"
        Me.Label54.Size = New System.Drawing.Size(209, 17)
        Me.Label54.TabIndex = 223
        Me.Label54.Text = "OBSERVATIONS / FINDINGS"
        '
        'copylblNature
        '
        Me.copylblNature.AutoSize = True
        Me.copylblNature.Location = New System.Drawing.Point(646, 296)
        Me.copylblNature.Name = "copylblNature"
        Me.copylblNature.Size = New System.Drawing.Size(0, 13)
        Me.copylblNature.TabIndex = 222
        '
        'copylblCategories
        '
        Me.copylblCategories.AutoSize = True
        Me.copylblCategories.Location = New System.Drawing.Point(646, 274)
        Me.copylblCategories.Name = "copylblCategories"
        Me.copylblCategories.Size = New System.Drawing.Size(0, 13)
        Me.copylblCategories.TabIndex = 221
        '
        'Label57
        '
        Me.Label57.AutoSize = True
        Me.Label57.Location = New System.Drawing.Point(595, 296)
        Me.Label57.Name = "Label57"
        Me.Label57.Size = New System.Drawing.Size(45, 13)
        Me.Label57.TabIndex = 220
        Me.Label57.Text = "Nature :"
        '
        'Label58
        '
        Me.Label58.AutoSize = True
        Me.Label58.Location = New System.Drawing.Point(577, 274)
        Me.Label58.Name = "Label58"
        Me.Label58.Size = New System.Drawing.Size(63, 13)
        Me.Label58.TabIndex = 219
        Me.Label58.Text = "Categories :"
        '
        'Label59
        '
        Me.Label59.AutoSize = True
        Me.Label59.Font = New System.Drawing.Font("Times New Roman", 11.25!, System.Drawing.FontStyle.Bold)
        Me.Label59.Location = New System.Drawing.Point(553, 252)
        Me.Label59.Name = "Label59"
        Me.Label59.Size = New System.Drawing.Size(130, 17)
        Me.Label59.TabIndex = 218
        Me.Label59.Text = "NATURE OF JOB"
        '
        'copylblDivision
        '
        Me.copylblDivision.AutoSize = True
        Me.copylblDivision.Location = New System.Drawing.Point(646, 231)
        Me.copylblDivision.Name = "copylblDivision"
        Me.copylblDivision.Size = New System.Drawing.Size(0, 13)
        Me.copylblDivision.TabIndex = 217
        '
        'copylblCenter
        '
        Me.copylblCenter.AutoSize = True
        Me.copylblCenter.Location = New System.Drawing.Point(647, 210)
        Me.copylblCenter.Name = "copylblCenter"
        Me.copylblCenter.Size = New System.Drawing.Size(0, 13)
        Me.copylblCenter.TabIndex = 216
        '
        'Label62
        '
        Me.Label62.AutoSize = True
        Me.Label62.Location = New System.Drawing.Point(591, 229)
        Me.Label62.Name = "Label62"
        Me.Label62.Size = New System.Drawing.Size(50, 13)
        Me.Label62.TabIndex = 215
        Me.Label62.Text = "Division :"
        '
        'Label63
        '
        Me.Label63.AutoSize = True
        Me.Label63.Location = New System.Drawing.Point(558, 209)
        Me.Label63.Name = "Label63"
        Me.Label63.Size = New System.Drawing.Size(83, 13)
        Me.Label63.TabIndex = 214
        Me.Label63.Text = "Center / Office :"
        '
        'copyLblLocation
        '
        Me.copyLblLocation.AutoSize = True
        Me.copyLblLocation.Location = New System.Drawing.Point(558, 191)
        Me.copyLblLocation.Name = "copyLblLocation"
        Me.copyLblLocation.Size = New System.Drawing.Size(0, 13)
        Me.copyLblLocation.TabIndex = 213
        '
        'Label65
        '
        Me.Label65.AutoSize = True
        Me.Label65.Font = New System.Drawing.Font("Times New Roman", 11.25!, System.Drawing.FontStyle.Bold)
        Me.Label65.Location = New System.Drawing.Point(550, 171)
        Me.Label65.Name = "Label65"
        Me.Label65.Size = New System.Drawing.Size(149, 17)
        Me.Label65.TabIndex = 212
        Me.Label65.Text = "LOCATION OF JOB"
        '
        'Label66
        '
        Me.Label66.AutoSize = True
        Me.Label66.Font = New System.Drawing.Font("Times New Roman", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label66.Location = New System.Drawing.Point(557, 147)
        Me.Label66.Name = "Label66"
        Me.Label66.Size = New System.Drawing.Size(162, 12)
        Me.Label66.TabIndex = 211
        Me.Label66.Text = "Form No. QWP - ICTMD - 01 - ANNEX1"
        '
        'Label67
        '
        Me.Label67.AutoSize = True
        Me.Label67.Font = New System.Drawing.Font("Times New Roman", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label67.Location = New System.Drawing.Point(559, 126)
        Me.Label67.Name = "Label67"
        Me.Label67.Size = New System.Drawing.Size(207, 17)
        Me.Label67.TabIndex = 210
        Me.Label67.Text = "Technology Management Division"
        '
        'Label68
        '
        Me.Label68.AutoSize = True
        Me.Label68.Font = New System.Drawing.Font("Times New Roman", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label68.Location = New System.Drawing.Point(557, 108)
        Me.Label68.Name = "Label68"
        Me.Label68.Size = New System.Drawing.Size(196, 17)
        Me.Label68.TabIndex = 209
        Me.Label68.Text = "Information Communication and"
        '
        'Label69
        '
        Me.Label69.AutoSize = True
        Me.Label69.Font = New System.Drawing.Font("Times New Roman", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label69.Location = New System.Drawing.Point(550, 88)
        Me.Label69.Name = "Label69"
        Me.Label69.Size = New System.Drawing.Size(282, 17)
        Me.Label69.TabIndex = 207
        Me.Label69.Text = "CORRECTIVE MAINTENANCE FORM"
        '
        'copylblControlNum
        '
        Me.copylblControlNum.AutoSize = True
        Me.copylblControlNum.Location = New System.Drawing.Point(859, 24)
        Me.copylblControlNum.Name = "copylblControlNum"
        Me.copylblControlNum.Size = New System.Drawing.Size(0, 13)
        Me.copylblControlNum.TabIndex = 203
        '
        'Label71
        '
        Me.Label71.AutoSize = True
        Me.Label71.Location = New System.Drawing.Point(768, 46)
        Me.Label71.Name = "Label71"
        Me.Label71.Size = New System.Drawing.Size(85, 13)
        Me.Label71.TabIndex = 201
        Me.Label71.Text = "Date Requested"
        '
        'Label72
        '
        Me.Label72.AutoSize = True
        Me.Label72.Location = New System.Drawing.Point(772, 23)
        Me.Label72.Name = "Label72"
        Me.Label72.Size = New System.Drawing.Size(60, 13)
        Me.Label72.TabIndex = 200
        Me.Label72.Text = "Control No."
        '
        'Label73
        '
        Me.Label73.AutoSize = True
        Me.Label73.Location = New System.Drawing.Point(563, 111)
        Me.Label73.Name = "Label73"
        Me.Label73.Size = New System.Drawing.Size(0, 13)
        Me.Label73.TabIndex = 197
        '
        'Label74
        '
        Me.Label74.AutoSize = True
        Me.Label74.Location = New System.Drawing.Point(583, 72)
        Me.Label74.Name = "Label74"
        Me.Label74.Size = New System.Drawing.Size(0, 13)
        Me.Label74.TabIndex = 198
        '
        'Button4
        '
        Me.Button4.Location = New System.Drawing.Point(1850, 543)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(78, 31)
        Me.Button4.TabIndex = 208
        Me.Button4.Text = "Print Preview"
        Me.Button4.UseVisualStyleBackColor = True
        '
        'Label75
        '
        Me.Label75.AutoSize = True
        Me.Label75.Location = New System.Drawing.Point(772, 67)
        Me.Label75.Name = "Label75"
        Me.Label75.Size = New System.Drawing.Size(72, 13)
        Me.Label75.TabIndex = 202
        Me.Label75.Text = "Date Finished"
        '
        'PictureBox2
        '
        Me.PictureBox2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox2.Image = CType(resources.GetObject("PictureBox2.Image"), System.Drawing.Image)
        Me.PictureBox2.Location = New System.Drawing.Point(553, 16)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(183, 69)
        Me.PictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox2.TabIndex = 199
        Me.PictureBox2.TabStop = False
        '
        'Label76
        '
        Me.Label76.AutoSize = True
        Me.Label76.Location = New System.Drawing.Point(583, 45)
        Me.Label76.Name = "Label76"
        Me.Label76.Size = New System.Drawing.Size(0, 13)
        Me.Label76.TabIndex = 196
        '
        'Label77
        '
        Me.Label77.AutoSize = True
        Me.Label77.Location = New System.Drawing.Point(583, 4)
        Me.Label77.Name = "Label77"
        Me.Label77.Size = New System.Drawing.Size(0, 13)
        Me.Label77.TabIndex = 195
        '
        'copylblDateFinished
        '
        Me.copylblDateFinished.AutoSize = True
        Me.copylblDateFinished.Location = New System.Drawing.Point(858, 66)
        Me.copylblDateFinished.Name = "copylblDateFinished"
        Me.copylblDateFinished.Size = New System.Drawing.Size(0, 13)
        Me.copylblDateFinished.TabIndex = 206
        '
        'copylblDateRequested
        '
        Me.copylblDateRequested.AutoSize = True
        Me.copylblDateRequested.Location = New System.Drawing.Point(859, 45)
        Me.copylblDateRequested.Name = "copylblDateRequested"
        Me.copylblDateRequested.Size = New System.Drawing.Size(0, 13)
        Me.copylblDateRequested.TabIndex = 204
        '
        'Label80
        '
        Me.Label80.AutoSize = True
        Me.Label80.Location = New System.Drawing.Point(583, 20)
        Me.Label80.Name = "Label80"
        Me.Label80.Size = New System.Drawing.Size(0, 13)
        Me.Label80.TabIndex = 194
        '
        'Label42
        '
        Me.Label42.AutoSize = True
        Me.Label42.Location = New System.Drawing.Point(22, 648)
        Me.Label42.Name = "Label42"
        Me.Label42.Size = New System.Drawing.Size(355, 13)
        Me.Label42.TabIndex = 193
        Me.Label42.Text = "This is to certify that the above job order has been completed satisfactorily"
        '
        'lblWorkCompletion
        '
        Me.lblWorkCompletion.AutoSize = True
        Me.lblWorkCompletion.Location = New System.Drawing.Point(78, 672)
        Me.lblWorkCompletion.Name = "lblWorkCompletion"
        Me.lblWorkCompletion.Size = New System.Drawing.Size(0, 13)
        Me.lblWorkCompletion.TabIndex = 192
        '
        'lblRequestedby
        '
        Me.lblRequestedby.AutoSize = True
        Me.lblRequestedby.Location = New System.Drawing.Point(80, 589)
        Me.lblRequestedby.Name = "lblRequestedby"
        Me.lblRequestedby.Size = New System.Drawing.Size(0, 13)
        Me.lblRequestedby.TabIndex = 191
        '
        'lblWorkmen
        '
        Me.lblWorkmen.AutoSize = True
        Me.lblWorkmen.Location = New System.Drawing.Point(82, 517)
        Me.lblWorkmen.Name = "lblWorkmen"
        Me.lblWorkmen.Size = New System.Drawing.Size(0, 13)
        Me.lblWorkmen.TabIndex = 190
        '
        'Label38
        '
        Me.Label38.AutoSize = True
        Me.Label38.Location = New System.Drawing.Point(382, 679)
        Me.Label38.Name = "Label38"
        Me.Label38.Size = New System.Drawing.Size(86, 13)
        Me.Label38.TabIndex = 189
        Me.Label38.Text = "CLIENT'S COPY"
        '
        'Label36
        '
        Me.Label36.AutoSize = True
        Me.Label36.Location = New System.Drawing.Point(80, 692)
        Me.Label36.Name = "Label36"
        Me.Label36.Size = New System.Drawing.Size(140, 13)
        Me.Label36.TabIndex = 188
        Me.Label36.Text = "Signature over printed name"
        '
        'Label37
        '
        Me.Label37.AutoSize = True
        Me.Label37.Font = New System.Drawing.Font("Times New Roman", 11.25!, System.Drawing.FontStyle.Bold)
        Me.Label37.Location = New System.Drawing.Point(23, 630)
        Me.Label37.Name = "Label37"
        Me.Label37.Size = New System.Drawing.Size(293, 17)
        Me.Label37.TabIndex = 187
        Me.Label37.Text = "WORK COMPLETION CERTIFICATION"
        '
        'Label34
        '
        Me.Label34.AutoSize = True
        Me.Label34.Location = New System.Drawing.Point(80, 609)
        Me.Label34.Name = "Label34"
        Me.Label34.Size = New System.Drawing.Size(140, 13)
        Me.Label34.TabIndex = 186
        Me.Label34.Text = "Signature over printed name"
        '
        'Label35
        '
        Me.Label35.AutoSize = True
        Me.Label35.Font = New System.Drawing.Font("Times New Roman", 11.25!, System.Drawing.FontStyle.Bold)
        Me.Label35.Location = New System.Drawing.Point(23, 565)
        Me.Label35.Name = "Label35"
        Me.Label35.Size = New System.Drawing.Size(134, 17)
        Me.Label35.TabIndex = 185
        Me.Label35.Text = "REQUESTED BY :"
        '
        'Label33
        '
        Me.Label33.AutoSize = True
        Me.Label33.Location = New System.Drawing.Point(80, 539)
        Me.Label33.Name = "Label33"
        Me.Label33.Size = New System.Drawing.Size(140, 13)
        Me.Label33.TabIndex = 184
        Me.Label33.Text = "Signature over printed name"
        '
        'Label32
        '
        Me.Label32.AutoSize = True
        Me.Label32.Font = New System.Drawing.Font("Times New Roman", 11.25!, System.Drawing.FontStyle.Bold)
        Me.Label32.Location = New System.Drawing.Point(23, 495)
        Me.Label32.Name = "Label32"
        Me.Label32.Size = New System.Drawing.Size(269, 17)
        Me.Label32.TabIndex = 183
        Me.Label32.Text = "WORKMEN ASSIGNED TO THE JOB"
        '
        'txtRemarks
        '
        Me.txtRemarks.BackColor = System.Drawing.SystemColors.Window
        Me.txtRemarks.Enabled = False
        Me.txtRemarks.Location = New System.Drawing.Point(28, 457)
        Me.txtRemarks.Multiline = True
        Me.txtRemarks.Name = "txtRemarks"
        Me.txtRemarks.Size = New System.Drawing.Size(464, 35)
        Me.txtRemarks.TabIndex = 182
        '
        'Label31
        '
        Me.Label31.AutoSize = True
        Me.Label31.Font = New System.Drawing.Font("Times New Roman", 11.25!, System.Drawing.FontStyle.Bold)
        Me.Label31.Location = New System.Drawing.Point(23, 436)
        Me.Label31.Name = "Label31"
        Me.Label31.Size = New System.Drawing.Size(95, 17)
        Me.Label31.TabIndex = 181
        Me.Label31.Text = "REMARKS :"
        '
        'txtRecommendation
        '
        Me.txtRecommendation.BackColor = System.Drawing.SystemColors.Window
        Me.txtRecommendation.Enabled = False
        Me.txtRecommendation.Location = New System.Drawing.Point(27, 398)
        Me.txtRecommendation.Multiline = True
        Me.txtRecommendation.Name = "txtRecommendation"
        Me.txtRecommendation.Size = New System.Drawing.Size(464, 35)
        Me.txtRecommendation.TabIndex = 180
        '
        'Label30
        '
        Me.Label30.AutoSize = True
        Me.Label30.Font = New System.Drawing.Font("Times New Roman", 11.25!, System.Drawing.FontStyle.Bold)
        Me.Label30.Location = New System.Drawing.Point(22, 377)
        Me.Label30.Name = "Label30"
        Me.Label30.Size = New System.Drawing.Size(165, 17)
        Me.Label30.TabIndex = 179
        Me.Label30.Text = "RECOMENDATIONS :"
        '
        'txtFindings
        '
        Me.txtFindings.BackColor = System.Drawing.SystemColors.Window
        Me.txtFindings.Enabled = False
        Me.txtFindings.Location = New System.Drawing.Point(26, 339)
        Me.txtFindings.Multiline = True
        Me.txtFindings.Name = "txtFindings"
        Me.txtFindings.Size = New System.Drawing.Size(464, 35)
        Me.txtFindings.TabIndex = 178
        '
        'Label29
        '
        Me.Label29.AutoSize = True
        Me.Label29.Font = New System.Drawing.Font("Times New Roman", 11.25!, System.Drawing.FontStyle.Bold)
        Me.Label29.Location = New System.Drawing.Point(21, 319)
        Me.Label29.Name = "Label29"
        Me.Label29.Size = New System.Drawing.Size(209, 17)
        Me.Label29.TabIndex = 177
        Me.Label29.Text = "OBSERVATIONS / FINDINGS"
        '
        'lblNature
        '
        Me.lblNature.AutoSize = True
        Me.lblNature.Location = New System.Drawing.Point(109, 296)
        Me.lblNature.Name = "lblNature"
        Me.lblNature.Size = New System.Drawing.Size(0, 13)
        Me.lblNature.TabIndex = 176
        '
        'lblCategories
        '
        Me.lblCategories.AutoSize = True
        Me.lblCategories.Location = New System.Drawing.Point(109, 274)
        Me.lblCategories.Name = "lblCategories"
        Me.lblCategories.Size = New System.Drawing.Size(0, 13)
        Me.lblCategories.TabIndex = 175
        '
        'Label26
        '
        Me.Label26.AutoSize = True
        Me.Label26.Location = New System.Drawing.Point(58, 296)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(45, 13)
        Me.Label26.TabIndex = 174
        Me.Label26.Text = "Nature :"
        '
        'Label25
        '
        Me.Label25.AutoSize = True
        Me.Label25.Location = New System.Drawing.Point(40, 274)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(63, 13)
        Me.Label25.TabIndex = 173
        Me.Label25.Text = "Categories :"
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.Font = New System.Drawing.Font("Times New Roman", 11.25!, System.Drawing.FontStyle.Bold)
        Me.Label24.Location = New System.Drawing.Point(16, 252)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(130, 17)
        Me.Label24.TabIndex = 172
        Me.Label24.Text = "NATURE OF JOB"
        '
        'lblDivision
        '
        Me.lblDivision.AutoSize = True
        Me.lblDivision.Location = New System.Drawing.Point(109, 231)
        Me.lblDivision.Name = "lblDivision"
        Me.lblDivision.Size = New System.Drawing.Size(0, 13)
        Me.lblDivision.TabIndex = 170
        '
        'lblCenter
        '
        Me.lblCenter.AutoSize = True
        Me.lblCenter.Location = New System.Drawing.Point(110, 210)
        Me.lblCenter.Name = "lblCenter"
        Me.lblCenter.Size = New System.Drawing.Size(0, 13)
        Me.lblCenter.TabIndex = 169
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Location = New System.Drawing.Point(54, 229)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(50, 13)
        Me.Label19.TabIndex = 167
        Me.Label19.Text = "Division :"
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Location = New System.Drawing.Point(21, 209)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(83, 13)
        Me.Label18.TabIndex = 166
        Me.Label18.Text = "Center / Office :"
        '
        'lblLocation
        '
        Me.lblLocation.AutoSize = True
        Me.lblLocation.Location = New System.Drawing.Point(21, 191)
        Me.lblLocation.Name = "lblLocation"
        Me.lblLocation.Size = New System.Drawing.Size(0, 13)
        Me.lblLocation.TabIndex = 165
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Font = New System.Drawing.Font("Times New Roman", 11.25!, System.Drawing.FontStyle.Bold)
        Me.Label16.Location = New System.Drawing.Point(13, 171)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(149, 17)
        Me.Label16.TabIndex = 164
        Me.Label16.Text = "LOCATION OF JOB"
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Font = New System.Drawing.Font("Times New Roman", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.Location = New System.Drawing.Point(16, 150)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(162, 12)
        Me.Label15.TabIndex = 163
        Me.Label15.Text = "Form No. QWP - ICTMD - 01 - ANNEX1"
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Font = New System.Drawing.Font("Times New Roman", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label14.Location = New System.Drawing.Point(18, 129)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(207, 17)
        Me.Label14.TabIndex = 162
        Me.Label14.Text = "Technology Management Division"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Font = New System.Drawing.Font("Times New Roman", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.Location = New System.Drawing.Point(16, 111)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(196, 17)
        Me.Label13.TabIndex = 162
        Me.Label13.Text = "Information Communication and"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Font = New System.Drawing.Font("Times New Roman", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.Location = New System.Drawing.Point(9, 91)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(282, 17)
        Me.Label12.TabIndex = 161
        Me.Label12.Text = "CORRECTIVE MAINTENANCE FORM"
        '
        'lblDateFinished
        '
        Me.lblDateFinished.AutoSize = True
        Me.lblDateFinished.Location = New System.Drawing.Point(317, 66)
        Me.lblDateFinished.Name = "lblDateFinished"
        Me.lblDateFinished.Size = New System.Drawing.Size(0, 13)
        Me.lblDateFinished.TabIndex = 160
        '
        'lblDateRequested
        '
        Me.lblDateRequested.AutoSize = True
        Me.lblDateRequested.Location = New System.Drawing.Point(318, 44)
        Me.lblDateRequested.Name = "lblDateRequested"
        Me.lblDateRequested.Size = New System.Drawing.Size(0, 13)
        Me.lblDateRequested.TabIndex = 7
        '
        'lblControlNum
        '
        Me.lblControlNum.AutoSize = True
        Me.lblControlNum.Location = New System.Drawing.Point(318, 23)
        Me.lblControlNum.Name = "lblControlNum"
        Me.lblControlNum.Size = New System.Drawing.Size(0, 13)
        Me.lblControlNum.TabIndex = 6
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(231, 66)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(72, 13)
        Me.Label3.TabIndex = 5
        Me.Label3.Text = "Date Finished"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(227, 44)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(85, 13)
        Me.Label2.TabIndex = 4
        Me.Label2.Text = "Date Requested"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(231, 22)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(60, 13)
        Me.Label1.TabIndex = 3
        Me.Label1.Text = "Control No."
        '
        'PictureBox1
        '
        Me.PictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox1.Image = CType(resources.GetObject("PictureBox1.Image"), System.Drawing.Image)
        Me.PictureBox1.Location = New System.Drawing.Point(12, 16)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(183, 69)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox1.TabIndex = 1
        Me.PictureBox1.TabStop = False
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(22, 114)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(0, 13)
        Me.Label11.TabIndex = 0
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(22, 75)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(0, 13)
        Me.Label10.TabIndex = 0
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(22, 48)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(0, 13)
        Me.Label9.TabIndex = 0
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(22, 23)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(0, 13)
        Me.Label8.TabIndex = 0
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(22, 7)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(0, 13)
        Me.Label7.TabIndex = 0
        '
        'ShapeContainer1
        '
        Me.ShapeContainer1.Location = New System.Drawing.Point(3, 16)
        Me.ShapeContainer1.Margin = New System.Windows.Forms.Padding(0)
        Me.ShapeContainer1.Name = "ShapeContainer1"
        Me.ShapeContainer1.Shapes.AddRange(New Microsoft.VisualBasic.PowerPacks.Shape() {Me.LineShape12, Me.LineShape11, Me.LineShape10, Me.RectangleShape2, Me.LineShape9, Me.LineShape8, Me.LineShape7, Me.LineShape6, Me.LineShape5, Me.LineShape4, Me.LineShape3, Me.LineShape2, Me.LineShape1, Me.RectangleShape1})
        Me.ShapeContainer1.Size = New System.Drawing.Size(1239, 761)
        Me.ShapeContainer1.TabIndex = 2
        Me.ShapeContainer1.TabStop = False
        '
        'LineShape12
        '
        Me.LineShape12.Name = "LineShape12"
        Me.LineShape12.X1 = 562
        Me.LineShape12.X2 = 822
        Me.LineShape12.Y1 = 671
        Me.LineShape12.Y2 = 671
        '
        'LineShape11
        '
        Me.LineShape11.Name = "LineShape11"
        Me.LineShape11.X1 = 562
        Me.LineShape11.X2 = 822
        Me.LineShape11.Y1 = 516
        Me.LineShape11.Y2 = 516
        '
        'LineShape10
        '
        Me.LineShape10.Name = "LineShape10"
        Me.LineShape10.X1 = 562
        Me.LineShape10.X2 = 822
        Me.LineShape10.Y1 = 588
        Me.LineShape10.Y2 = 588
        '
        'RectangleShape2
        '
        Me.RectangleShape2.Location = New System.Drawing.Point(765, 5)
        Me.RectangleShape2.Name = "RectangleShape2"
        Me.RectangleShape2.Size = New System.Drawing.Size(240, 65)
        '
        'LineShape9
        '
        Me.LineShape9.Name = "LineShape9"
        Me.LineShape9.X1 = 766
        Me.LineShape9.X2 = 1006
        Me.LineShape9.Y1 = 27
        Me.LineShape9.Y2 = 27
        '
        'LineShape8
        '
        Me.LineShape8.Name = "LineShape8"
        Me.LineShape8.X1 = 766
        Me.LineShape8.X2 = 1005
        Me.LineShape8.Y1 = 48
        Me.LineShape8.Y2 = 48
        '
        'LineShape7
        '
        Me.LineShape7.Name = "LineShape7"
        Me.LineShape7.X1 = 851
        Me.LineShape7.X2 = 851
        Me.LineShape7.Y1 = 5
        Me.LineShape7.Y2 = 70
        '
        'LineShape6
        '
        Me.LineShape6.Name = "LineShape6"
        Me.LineShape6.X1 = 29
        Me.LineShape6.X2 = 281
        Me.LineShape6.Y1 = 675
        Me.LineShape6.Y2 = 675
        '
        'LineShape5
        '
        Me.LineShape5.Name = "LineShape5"
        Me.LineShape5.X1 = 29
        Me.LineShape5.X2 = 281
        Me.LineShape5.Y1 = 592
        Me.LineShape5.Y2 = 592
        '
        'LineShape4
        '
        Me.LineShape4.Name = "LineShape4"
        Me.LineShape4.X1 = 29
        Me.LineShape4.X2 = 281
        Me.LineShape4.Y1 = 520
        Me.LineShape4.Y2 = 520
        '
        'LineShape3
        '
        Me.LineShape3.Name = "LineShape3"
        Me.LineShape3.X1 = 309
        Me.LineShape3.X2 = 309
        Me.LineShape3.Y1 = 4
        Me.LineShape3.Y2 = 69
        '
        'LineShape2
        '
        Me.LineShape2.Name = "LineShape2"
        Me.LineShape2.X1 = 224
        Me.LineShape2.X2 = 463
        Me.LineShape2.Y1 = 47
        Me.LineShape2.Y2 = 47
        '
        'LineShape1
        '
        Me.LineShape1.Name = "LineShape1"
        Me.LineShape1.X1 = 224
        Me.LineShape1.X2 = 464
        Me.LineShape1.Y1 = 26
        Me.LineShape1.Y2 = 26
        '
        'RectangleShape1
        '
        Me.RectangleShape1.Location = New System.Drawing.Point(223, 4)
        Me.RectangleShape1.Name = "RectangleShape1"
        Me.RectangleShape1.Size = New System.Drawing.Size(240, 65)
        '
        'PrintDocument1
        '
        '
        'PrintPreviewDialog1
        '
        Me.PrintPreviewDialog1.AutoScrollMargin = New System.Drawing.Size(0, 0)
        Me.PrintPreviewDialog1.AutoScrollMinSize = New System.Drawing.Size(0, 0)
        Me.PrintPreviewDialog1.ClientSize = New System.Drawing.Size(400, 300)
        Me.PrintPreviewDialog1.Enabled = True
        Me.PrintPreviewDialog1.Icon = CType(resources.GetObject("PrintPreviewDialog1.Icon"), System.Drawing.Icon)
        Me.PrintPreviewDialog1.Name = "PrintPreviewDialog1"
        Me.PrintPreviewDialog1.Visible = False
        '
        'PrintDialog1
        '
        Me.PrintDialog1.UseEXDialog = True
        '
        'Timer1
        '
        Me.Timer1.Interval = 10000
        '
        'Timer2
        '
        Me.Timer2.Interval = 10000
        '
        'PrintPreviewDialog2
        '
        Me.PrintPreviewDialog2.AutoScrollMargin = New System.Drawing.Size(0, 0)
        Me.PrintPreviewDialog2.AutoScrollMinSize = New System.Drawing.Size(0, 0)
        Me.PrintPreviewDialog2.ClientSize = New System.Drawing.Size(400, 300)
        Me.PrintPreviewDialog2.Enabled = True
        Me.PrintPreviewDialog2.Icon = CType(resources.GetObject("PrintPreviewDialog2.Icon"), System.Drawing.Icon)
        Me.PrintPreviewDialog2.Name = "PrintPreviewDialog1"
        Me.PrintPreviewDialog2.Visible = False
        '
        'PrintDialog2
        '
        Me.PrintDialog2.UseEXDialog = True
        '
        'btnPrint
        '
        Me.btnPrint.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnPrint.Image = CType(resources.GetObject("btnPrint.Image"), System.Drawing.Image)
        Me.btnPrint.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnPrint.Location = New System.Drawing.Point(1274, 16)
        Me.btnPrint.Name = "btnPrint"
        Me.btnPrint.Size = New System.Drawing.Size(60, 70)
        Me.btnPrint.TabIndex = 159
        Me.btnPrint.Text = "PRINT"
        Me.btnPrint.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnPrint.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.btnPrint.UseVisualStyleBackColor = True
        '
        'frmPrint
        '
        Me.AcceptButton = Me.btnPrint
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1370, 749)
        Me.Controls.Add(Me.btnPrint)
        Me.Controls.Add(Me.GroupBox1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frmPrint"
        Me.ShowIcon = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "PRINT"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents PictureBox1 As PictureBox
    Friend WithEvents Label11 As Label
    Friend WithEvents Label10 As Label
    Friend WithEvents Label9 As Label
    Friend WithEvents Label8 As Label
    Friend WithEvents Label7 As Label
    Friend WithEvents btnPrint As Button
    Friend WithEvents PrintDocument1 As Printing.PrintDocument
    Friend WithEvents PrintPreviewDialog1 As PrintPreviewDialog
    Friend WithEvents PrintDialog1 As PrintDialog
    Friend WithEvents Timer1 As Timer
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents lblLocation As System.Windows.Forms.Label
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents lblDateFinished As System.Windows.Forms.Label
    Friend WithEvents lblDateRequested As System.Windows.Forms.Label
    Friend WithEvents lblControlNum As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents lblWorkCompletion As System.Windows.Forms.Label
    Friend WithEvents lblRequestedby As System.Windows.Forms.Label
    Friend WithEvents lblWorkmen As System.Windows.Forms.Label
    Friend WithEvents Label38 As System.Windows.Forms.Label
    Friend WithEvents Label36 As System.Windows.Forms.Label
    Friend WithEvents Label37 As System.Windows.Forms.Label
    Friend WithEvents Label34 As System.Windows.Forms.Label
    Friend WithEvents Label35 As System.Windows.Forms.Label
    Friend WithEvents Label33 As System.Windows.Forms.Label
    Friend WithEvents Label32 As System.Windows.Forms.Label
    Friend WithEvents txtRemarks As System.Windows.Forms.TextBox
    Friend WithEvents Label31 As System.Windows.Forms.Label
    Friend WithEvents txtRecommendation As System.Windows.Forms.TextBox
    Friend WithEvents Label30 As System.Windows.Forms.Label
    Friend WithEvents txtFindings As System.Windows.Forms.TextBox
    Friend WithEvents Label29 As System.Windows.Forms.Label
    Friend WithEvents lblNature As System.Windows.Forms.Label
    Friend WithEvents lblCategories As System.Windows.Forms.Label
    Friend WithEvents Label26 As System.Windows.Forms.Label
    Friend WithEvents Label25 As System.Windows.Forms.Label
    Friend WithEvents Label24 As System.Windows.Forms.Label
    Friend WithEvents lblDivision As System.Windows.Forms.Label
    Friend WithEvents lblCenter As System.Windows.Forms.Label
    Friend WithEvents Label42 As System.Windows.Forms.Label
    Private WithEvents ShapeContainer1 As Microsoft.VisualBasic.PowerPacks.ShapeContainer
    Private WithEvents LineShape3 As Microsoft.VisualBasic.PowerPacks.LineShape
    Private WithEvents LineShape2 As Microsoft.VisualBasic.PowerPacks.LineShape
    Private WithEvents LineShape1 As Microsoft.VisualBasic.PowerPacks.LineShape
    Private WithEvents RectangleShape1 As Microsoft.VisualBasic.PowerPacks.RectangleShape
    Private WithEvents LineShape6 As Microsoft.VisualBasic.PowerPacks.LineShape
    Private WithEvents LineShape5 As Microsoft.VisualBasic.PowerPacks.LineShape
    Private WithEvents LineShape4 As Microsoft.VisualBasic.PowerPacks.LineShape
    Friend WithEvents Label20 As Label
    Friend WithEvents Label23 As Label
    Friend WithEvents copylblWorkCompletion As Label
    Friend WithEvents copylblRequestedby As Label
    Friend WithEvents copylblWorkmen As Label
    Friend WithEvents Label46 As Label
    Friend WithEvents Label47 As Label
    Friend WithEvents Label48 As Label
    Friend WithEvents Label49 As Label
    Friend WithEvents Button3 As Button
    Friend WithEvents Label50 As Label
    Friend WithEvents Label51 As Label
    Friend WithEvents copytxtRemarks As TextBox
    Friend WithEvents Label52 As Label
    Friend WithEvents copytxtRecommendation As TextBox
    Friend WithEvents Label53 As Label
    Friend WithEvents copytxtFindings As TextBox
    Friend WithEvents Label54 As Label
    Friend WithEvents copylblNature As Label
    Friend WithEvents copylblCategories As Label
    Friend WithEvents Label57 As Label
    Friend WithEvents Label58 As Label
    Friend WithEvents Label59 As Label
    Friend WithEvents copylblDivision As Label
    Friend WithEvents copylblCenter As Label
    Friend WithEvents Label62 As Label
    Friend WithEvents Label63 As Label
    Friend WithEvents copyLblLocation As Label
    Friend WithEvents Label65 As Label
    Friend WithEvents Label66 As Label
    Friend WithEvents Label67 As Label
    Friend WithEvents Label68 As Label
    Friend WithEvents Label69 As Label
    Friend WithEvents copylblControlNum As Label
    Friend WithEvents Label71 As Label
    Friend WithEvents Label72 As Label
    Friend WithEvents Label73 As Label
    Friend WithEvents Label74 As Label
    Friend WithEvents Button4 As Button
    Friend WithEvents Label75 As Label
    Friend WithEvents PictureBox2 As PictureBox
    Friend WithEvents Label76 As Label
    Friend WithEvents Label77 As Label
    Friend WithEvents copylblDateFinished As Label
    Friend WithEvents copylblDateRequested As Label
    Friend WithEvents Label80 As Label
    Friend WithEvents PrintDocument2 As Printing.PrintDocument
    Friend WithEvents Timer2 As Timer
    Friend WithEvents PrintPreviewDialog2 As PrintPreviewDialog
    Friend WithEvents PrintDialog2 As PrintDialog
    Private WithEvents LineShape12 As PowerPacks.LineShape
    Private WithEvents LineShape11 As PowerPacks.LineShape
    Private WithEvents LineShape10 As PowerPacks.LineShape
    Private WithEvents RectangleShape2 As PowerPacks.RectangleShape
    Private WithEvents LineShape9 As PowerPacks.LineShape
    Private WithEvents LineShape8 As PowerPacks.LineShape
    Private WithEvents LineShape7 As PowerPacks.LineShape
End Class
