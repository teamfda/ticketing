﻿Imports System.Drawing.Printing
Imports System.Data.Odbc
Imports System.Globalization

Public Class frmAccompReport1
    Private Sub frmAccompReport1_Load(sender As Object, e As EventArgs) Handles MyBase.Load

    End Sub

    Private Sub btnAccom_Click(sender As Object, e As EventArgs) Handles btnAccom.Click
        Call connect()
        Dim da As OdbcDataAdapter
        Dim dt As New DataTable
        Dim sql As String
        Dim sql1 As String
        Dim FilterDate2 As Date = DateTimePicker2.Value.Date
        Dim FilterDate3 As Date = DateTimePicker3.Value.Date


        If FilterDate2.Date.Day >= DateTimePicker2.Value.Date.Day And FilterDate3.Date.Day <= DateTimePicker3.Value.Date.Day Then
            Dim FilterDate21 = FilterDate2.ToString("yyyy-MM-dd")
            Dim FilterDate31 = FilterDate3.ToString("yyyy-MM-dd")
            sql = "SELECT Division, SUM(Nature = 'Hardware') AS `hardware`, SUM(Nature = 'Software') AS `software`, SUM(Nature = 'Network') AS `network`, SUM(Nature = 'Printer') AS `printer` FROM tblaccomplish WHERE Nature IN ('Hardware', 'Software', 'Network', 'Printer') AND FK = '" & MDIMain.tslnum.Text & "' AND Status = 'Done' AND STR_TO_DATE(Datefinish, '%d-%m-%Y') BETWEEN '" & FilterDate21 & "' AND '" & FilterDate31 & "' GROUP BY Division ORDER BY Division "
            da = New OdbcDataAdapter(sql, modCon.con)
            da.Fill(dt)

            lblAdmin2.Text = MDIMain.tslUser.Text
            lblAdmin.Text = MDIMain.tslUser.Text
            lblDateFrom.Text = FilterDate21
            lblDateTo.Text = FilterDate31
            lblTotalHard.Text = 0
            lblTotalSoft.Text = 0
            lblTotalNet.Text = 0
            lblTotalPrint.Text = 0

            If dt.Rows.Count > 0 Then
                For i As Integer = 0 To dt.Rows.Count - 1
                    lblPrinter.Text = dt.Rows(i)(4) & vbCrLf & vbCrLf & lblPrinter.Text
                    lblNetwork.Text = dt.Rows(i)(3) & vbCrLf & vbCrLf & lblNetwork.Text
                    lblSoftware.Text = dt.Rows(i)(2) & vbCrLf & vbCrLf & lblSoftware.Text
                    lblHardware.Text = dt.Rows(i)(1) & vbCrLf & vbCrLf & lblHardware.Text
                    lblDivision.Text = dt.Rows(i)(0) & vbCrLf & vbCrLf & lblDivision.Text

                    lblTotalHard.Text += dt.Rows(i)(1)
                    lblTotalSoft.Text += dt.Rows(i)(2)
                    lblTotalNet.Text += dt.Rows(i)(3)
                    lblTotalPrint.Text += dt.Rows(i)(4)
                Next

            End If


        End If
    End Sub

    'Private Sub btnPrint_Click(sender As Object, e As EventArgs)
    'PrintDocument1.Print()
    'End Sub

    Private bitmap As Bitmap
    Private Sub PrintDocument1_PrintPage(ByVal sender As System.Object, ByVal e As System.Drawing.Printing.PrintPageEventArgs) Handles PrintDocument1.PrintPage
        PrintDialog1.PrinterSettings = PrintDocument1.PrinterSettings
        If PrintDialog1.ShowDialog() = Windows.Forms.DialogResult.OK Then
            PrintDocument1.PrinterSettings = PrintDialog1.PrinterSettings
            Dim pagesetup As New PageSettings
            With pagesetup
                .Margins.Left = 50
                .Margins.Right = 50
                .Margins.Top = 50
                .Margins.Bottom = 50
                .Landscape = True

            End With
            PrintDocument1.DefaultPageSettings = pagesetup
        End If
        PrintPreviewDialog1.Document = PrintDocument1

        Dim bm As New Bitmap(Me.GroupBox1.Width, Me.GroupBox1.Height)

        GroupBox1.DrawToBitmap(bm, New Rectangle(0, 0, Me.GroupBox1.Width, Me.GroupBox1.Height))

        e.Graphics.DrawImage(bm, 0, 0)
    End Sub

    Private Sub PrintPreviewDialog1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PrintPreviewDialog1.Load
        PrintDialog1.PrinterSettings = PrintDocument1.PrinterSettings
        If PrintDialog1.ShowDialog() = Windows.Forms.DialogResult.OK Then
            PrintDocument1.PrinterSettings = PrintDialog1.PrinterSettings
            Dim pagesetup As New PageSettings
            With pagesetup
                .Margins.Left = 150
                .Margins.Right = 150
                .Margins.Top = 150
                .Margins.Bottom = 150
                .Landscape = False
            End With
            PrintDocument1.DefaultPageSettings = pagesetup
        End If
        PrintDocument1.DefaultPageSettings.Landscape = False
        PrintPreviewDialog1.Document = PrintDocument1
    End Sub

    Private Sub btnPrintPreview_Click(sender As Object, e As EventArgs) Handles btnPrintPreview.Click
        PrintPreviewDialog1.ShowDialog()
    End Sub

    Private Sub btnClose_Click(sender As Object, e As EventArgs) Handles btnClose.Click
        If MsgBox("Are you sure you want to close?", vbQuestion + vbYesNo, "PRINT") = vbYes Then
            Call clean()
            Me.Hide()
        End If
    End Sub

    Private Sub btnRefresh_Click(sender As Object, e As EventArgs) Handles btnRefresh.Click
        Call clean()
    End Sub

    Private Sub clean()
        lblAdmin.Text = ""
        lblDateFrom.Text = ""
        lblDateTo.Text = ""
        lblDivision.Text = ""
        lblHardware.Text = ""
        lblSoftware.Text = ""
        lblNetwork.Text = ""
        lblPrinter.Text = ""
        lblTotalHard.Text = ""
        lblAdmin2.Text = ""
    End Sub

End Class