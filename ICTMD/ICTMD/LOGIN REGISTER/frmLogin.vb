﻿Imports System.Windows.Forms
Imports System.Data.Odbc
Imports Tulpep.NotificationWindow

Public Class frmLogin

    'Button to login 
    Private Sub btnlogin_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnlogin.Click
        Dim da As OdbcDataAdapter
        Dim dt As New DataTable

        Dim sql As String = "SELECT * FROM tbluser WHERE uname = '" & Replace(txtuser.Text, "'", " ") & "' AND BINARY pword = '" & Replace(txtpass.Text, "'", " ") & "' and isactive = 1"
        da = New OdbcDataAdapter(sql, modCon.con)
        da.Fill(dt)


        If dt.Rows.Count = 1 Then
            MDIMain.tslUser.Text = dt.Rows(0)(1) + " " + dt.Rows(0)(2)
            MDIMain.tslPos.Text = dt.Rows(0)(3)
            MDIMain.tslnum.Text = dt.Rows(0)(0)
            Call clean()
            Call filter()
            MDIMain.Show()
            Me.Hide()
        Else
            MsgBox("Username and password are incorrect!", vbExclamation, "Login User")

        End If
        da.Dispose()
    End Sub

    'To exit the login form
    Private Sub btnclose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnclose.Click
        If MsgBox("Are you sure you want to exit?", vbQuestion + vbYesNo, "Exit Program") = vbYes Then
            End
        End If
    End Sub

    'Call database/Connect to database
    Private Sub frmLogin_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Call modCon.connect()

    End Sub

    'To clean the textbox
    Private Sub clean()
        txtuser.Clear()
        txtpass.Clear()
        txtuser.Focus()
    End Sub

    'Filter position
    Public Sub filter()
        With MDIMain
            If .tslPos.Text = "Super Administrator" Then
                .ReportsToolStripMenuItem.Visible = False

            Else
                If .tslPos.Text = "Administrator" Then
                    .UserAccountToolStripMenuItem.Visible = False
                    .ReportsToolStripMenuItem.Visible = False
                    .AdminReport.Visible = False

                Else
                    .tslPos.Text = "User"
                    .ToolStripButton1.Visible = False
                    .tsMaintenance.Visible = False
                    .lblrCount.Visible = False
                    .JobOrderToolStripMenuItem.Visible = False
                    .Reports.Visible = False
                End If
            End If

        End With
    End Sub

    'To show the register form
    Private Sub lblRegister_Click(sender As Object, e As EventArgs) Handles lblRegister.Click
        Me.Hide()
        frmRegister.Show()
    End Sub


End Class
