﻿Imports System.Data.Odbc
Public Class frmRegister

    'Auto pick position
    Private Sub frmGuest_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        cboPos.SelectedIndex = 1
    End Sub

    'To cancel the registration
    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        If MsgBox("Are you sure you want to cancel?", vbQuestion + vbYesNo, "User Account") = vbYes Then
            Call cleaner()
            cboPos.SelectedIndex = 1
        End If
    End Sub

    'To exit the registration form
    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        If MsgBox("Are you sure you want to close?", vbQuestion + vbYesNo, "User Account") = vbYes Then
            Call cleaner()
            Me.Hide()
            frmLogin.Show()
        End If
    End Sub

    'Clean textbox and combobox
    Private Sub cleaner()
        Dim obj As Object
        For Each obj In grpEntry.Controls
            If TypeOf obj Is TextBox Then
                obj.clear()
                cboPos.SelectedIndex = 1
                chkActive.Checked = False
            End If
        Next
    End Sub

    'To register the inputted data
    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim cmd As Odbc.OdbcCommand
        Dim sql As String = ""
        Dim da As OdbcDataAdapter

        Try
            Dim dt As New DataTable
            Dim sql1 As String = "SELECT uname FROM tbluser WHERE uname = '" & Replace(txtuser.Text, "'", " ") & "'"
            da = New OdbcDataAdapter(sql1, modCon.con)
            da.Fill(dt)
            If dt.Rows.Count = 1 Then
                MsgBox("Username Already Used!", vbExclamation, "User Account")

            Else
                If txtfname.Text = "" Or txtlname.Text = "" Or txtpass.Text = "" Or txtuser.Text = "" Or cboPos.Text = "" Then
                    MsgBox("Please Enter All Fieds!", vbExclamation, "User Account")
                    Call cleaner()

                Else
                    If txtpass.Text = txtrepass.Text Then
                        sql = "INSERT INTO tbluser(fname,lname,pos,uname,pword,isactive)VALUES(?,?,?,?,?,?)"
                        MsgBox("Successfully Added!", vbInformation, "User Account")
                        Me.Hide()
                        frmLogin.Show()

                    Else
                        MsgBox("Password did not match!", vbExclamation, "User Account")
                        txtpass.Focus()
                    End If
                End If
            End If

            cmd = New Odbc.OdbcCommand(sql, modCon.con)

            cmd.Parameters.AddWithValue("?", txtfname.Text)
            cmd.Parameters.AddWithValue("?", txtlname.Text)
            cmd.Parameters.AddWithValue("?", cboPos.Text)
            cmd.Parameters.AddWithValue("?", txtuser.Text)
            cmd.Parameters.AddWithValue("?", txtpass.Text)
            cmd.Parameters.AddWithValue("?", IIf(chkActive.CheckState = CheckState.Checked, 1, 0))
            cmd.ExecuteNonQuery()
        Catch ex As Exception

        End Try


    End Sub


End Class