﻿Imports System.Data.Odbc
Imports System.IO
Imports System
Imports System.Threading

Public Class frmAccomplish
    Sub populate()
        Call loadData("SELECT ID,Division,Section,Nature,Categories,Location,Findings,Remarks,Requested,Status,Claimby,Date FROM tblaccomplish WHERE Fk = '" & MDIMain.tslnum.Text & "' AND Status = 'Ongoing' AND Deleted = 'N' ORDER BY Division IN ('ODG', 'CDRR', 'FROO') DESC, Section IN ('ODG-IM') DESC", dvgstudent)
    End Sub

    Private Sub frmAccomplish_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Call populate()
        Call loadComboLoc()
        'Call loadComboNat()
        'Call loadComboRec()
        Call loadComboSec()
        dvgstudent.Tag = ""
        'Timer1.Start()
        Call enabler()
        lblDoneby.Text = MDIMain.tslUser.Text

    End Sub

    'For the color coded data based on their status
    Private Sub dvgstudent_CellFormatting(ByVal sender As Object, ByVal e As DataGridViewCellFormattingEventArgs) Handles dvgstudent.CellFormatting
        For i As Integer = 0 To Me.dvgstudent.Rows.Count - 1
            If Me.dvgstudent.Rows(i).Cells("Column10").Value = "Ongoing" And Me.dvgstudent.Rows(i).Cells("Column2").Value = "CDRR" Then
                Me.dvgstudent.Rows(i).Cells("Column1").Style.ForeColor = Color.Red
                Me.dvgstudent.Rows(i).Cells("Column2").Style.ForeColor = Color.Red
                Me.dvgstudent.Rows(i).Cells("Column3").Style.ForeColor = Color.Red
                Me.dvgstudent.Rows(i).Cells("Column4").Style.ForeColor = Color.Red
                Me.dvgstudent.Rows(i).Cells("Column5").Style.ForeColor = Color.Red
                Me.dvgstudent.Rows(i).Cells("Column6").Style.ForeColor = Color.Red
                Me.dvgstudent.Rows(i).Cells("Column7").Style.ForeColor = Color.Red
                Me.dvgstudent.Rows(i).Cells("Column8").Style.ForeColor = Color.Red
                Me.dvgstudent.Rows(i).Cells("Column9").Style.ForeColor = Color.Red
                Me.dvgstudent.Rows(i).Cells("Column10").Style.ForeColor = Color.Red
                Me.dvgstudent.Rows(i).Cells("Column11").Style.ForeColor = Color.Red
                Me.dvgstudent.Rows(i).Cells("Column12").Style.ForeColor = Color.Red
            Else
                If Me.dvgstudent.Rows(i).Cells("Column10").Value = "Ongoing" And Me.dvgstudent.Rows(i).Cells("Column2").Value = "ODG" Then
                    Me.dvgstudent.Rows(i).Cells("Column1").Style.ForeColor = Color.Red
                    Me.dvgstudent.Rows(i).Cells("Column2").Style.ForeColor = Color.Red
                    Me.dvgstudent.Rows(i).Cells("Column3").Style.ForeColor = Color.Red
                    Me.dvgstudent.Rows(i).Cells("Column4").Style.ForeColor = Color.Red
                    Me.dvgstudent.Rows(i).Cells("Column5").Style.ForeColor = Color.Red
                    Me.dvgstudent.Rows(i).Cells("Column6").Style.ForeColor = Color.Red
                    Me.dvgstudent.Rows(i).Cells("Column7").Style.ForeColor = Color.Red
                    Me.dvgstudent.Rows(i).Cells("Column8").Style.ForeColor = Color.Red
                    Me.dvgstudent.Rows(i).Cells("Column9").Style.ForeColor = Color.Red
                    Me.dvgstudent.Rows(i).Cells("Column10").Style.ForeColor = Color.Red
                    Me.dvgstudent.Rows(i).Cells("Column11").Style.ForeColor = Color.Red
                    Me.dvgstudent.Rows(i).Cells("Column12").Style.ForeColor = Color.Red
                Else
                    If Me.dvgstudent.Rows(i).Cells("Column10").Value = "Ongoing" And Me.dvgstudent.Rows(i).Cells("Column2").Value = "FROO" Then
                        Me.dvgstudent.Rows(i).Cells("Column1").Style.ForeColor = Color.Red
                        Me.dvgstudent.Rows(i).Cells("Column2").Style.ForeColor = Color.Red
                        Me.dvgstudent.Rows(i).Cells("Column3").Style.ForeColor = Color.Red
                        Me.dvgstudent.Rows(i).Cells("Column4").Style.ForeColor = Color.Red
                        Me.dvgstudent.Rows(i).Cells("Column5").Style.ForeColor = Color.Red
                        Me.dvgstudent.Rows(i).Cells("Column6").Style.ForeColor = Color.Red
                        Me.dvgstudent.Rows(i).Cells("Column7").Style.ForeColor = Color.Red
                        Me.dvgstudent.Rows(i).Cells("Column8").Style.ForeColor = Color.Red
                        Me.dvgstudent.Rows(i).Cells("Column9").Style.ForeColor = Color.Red
                        Me.dvgstudent.Rows(i).Cells("Column10").Style.ForeColor = Color.Red
                        Me.dvgstudent.Rows(i).Cells("Column11").Style.ForeColor = Color.Red
                        Me.dvgstudent.Rows(i).Cells("Column12").Style.ForeColor = Color.Red
                    Else
                        If Me.dvgstudent.Rows(i).Cells("Column10").Value = "Ongoing" And Me.dvgstudent.Rows(i).Cells("Column3").Value = "ODG-IM" Then
                            Me.dvgstudent.Rows(i).Cells("Column1").Style.ForeColor = Color.Red
                            Me.dvgstudent.Rows(i).Cells("Column2").Style.ForeColor = Color.Red
                            Me.dvgstudent.Rows(i).Cells("Column3").Style.ForeColor = Color.Red
                            Me.dvgstudent.Rows(i).Cells("Column4").Style.ForeColor = Color.Red
                            Me.dvgstudent.Rows(i).Cells("Column5").Style.ForeColor = Color.Red
                            Me.dvgstudent.Rows(i).Cells("Column6").Style.ForeColor = Color.Red
                            Me.dvgstudent.Rows(i).Cells("Column7").Style.ForeColor = Color.Red
                            Me.dvgstudent.Rows(i).Cells("Column8").Style.ForeColor = Color.Red
                            Me.dvgstudent.Rows(i).Cells("Column9").Style.ForeColor = Color.Red
                            Me.dvgstudent.Rows(i).Cells("Column10").Style.ForeColor = Color.Red
                            Me.dvgstudent.Rows(i).Cells("Column11").Style.ForeColor = Color.Red
                            Me.dvgstudent.Rows(i).Cells("Column12").Style.ForeColor = Color.Red
                        Else
                            If Me.dvgstudent.Rows(i).Cells("Column10").Value = "Ongoing" Then
                                Me.dvgstudent.Rows(i).Cells("Column1").Style.ForeColor = Color.Blue
                                Me.dvgstudent.Rows(i).Cells("Column2").Style.ForeColor = Color.Blue
                                Me.dvgstudent.Rows(i).Cells("Column3").Style.ForeColor = Color.Blue
                                Me.dvgstudent.Rows(i).Cells("Column4").Style.ForeColor = Color.Blue
                                Me.dvgstudent.Rows(i).Cells("Column5").Style.ForeColor = Color.Blue
                                Me.dvgstudent.Rows(i).Cells("Column6").Style.ForeColor = Color.Blue
                                Me.dvgstudent.Rows(i).Cells("Column7").Style.ForeColor = Color.Blue
                                Me.dvgstudent.Rows(i).Cells("Column8").Style.ForeColor = Color.Blue
                                Me.dvgstudent.Rows(i).Cells("Column9").Style.ForeColor = Color.Blue
                                Me.dvgstudent.Rows(i).Cells("Column10").Style.ForeColor = Color.Blue
                                Me.dvgstudent.Rows(i).Cells("Column11").Style.ForeColor = Color.Blue
                                Me.dvgstudent.Rows(i).Cells("Column12").Style.ForeColor = Color.Blue
                            Else
                                If Me.dvgstudent.Rows(i).Cells("Column10").Value = "Done" Then
                                    Me.dvgstudent.Rows(i).Cells("Column1").Style.ForeColor = Color.Green
                                    Me.dvgstudent.Rows(i).Cells("Column2").Style.ForeColor = Color.Green
                                    Me.dvgstudent.Rows(i).Cells("Column3").Style.ForeColor = Color.Green
                                    Me.dvgstudent.Rows(i).Cells("Column4").Style.ForeColor = Color.Green
                                    Me.dvgstudent.Rows(i).Cells("Column5").Style.ForeColor = Color.Green
                                    Me.dvgstudent.Rows(i).Cells("Column6").Style.ForeColor = Color.Green
                                    Me.dvgstudent.Rows(i).Cells("Column7").Style.ForeColor = Color.Green
                                    Me.dvgstudent.Rows(i).Cells("Column8").Style.ForeColor = Color.Green
                                    Me.dvgstudent.Rows(i).Cells("Column9").Style.ForeColor = Color.Green
                                    Me.dvgstudent.Rows(i).Cells("Column10").Style.ForeColor = Color.Green
                                    Me.dvgstudent.Rows(i).Cells("Column11").Style.ForeColor = Color.Green
                                    Me.dvgstudent.Rows(i).Cells("Column12").Style.ForeColor = Color.Green
                                End If
                            End If
                        End If
                    End If
                End If
            End If
        Next
    End Sub

    'To save the all updated data from request
    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click

        If cbDivision.Text = "" Or txtFindings.Text = "" Or cbLocation.Text = "" Or cbNature.Text = "" Or txtRequest.Text = "" Or cbSection.Text = "" Then
            MsgBox("Please fill up all field!", vbExclamation, "Student Information")
        Else
            If Len(dvgstudent.Tag) = 0 Then
                Call modCon.save("INSERT INTO tblaccomplish(Division,Section,Nature,Categories,Location,Findings,Remarks,Requested,Status,Claimby,Doneby,Date,Fk)VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?)")
                MsgBox("ADDED SUCCESSFULLY", vbInformation, "Accomplish Information")
                '" & Replace(tslUserID.Text, "'", " ") & "'
                cbDivision.SelectedIndex = -1
                txtFindings.Text = ""
                txtRemarks.Text = ""
                txtRequest.Text = ""
                cbSection.SelectedIndex = -1
                cbDivision.SelectedIndex = -1
                cmbStatus.SelectedIndex = -1
                cbLocation.SelectedIndex = -1
                cbNature.SelectedIndex = -1
                cmbNature.SelectedIndex = -1
                cbNature.Text = ""
                'Call loadData("SELECT ID,Division,Section,Nature,Categories,Location,Findings,Remarks,Requested,Status,Date FROM tblaccomplish WHERE Fk LIKE '%" & MDIMain.tslUser.Text & "%'", dvgstudent)
                'Call loadData("SELECT ID,Division,Section,Nature,Categories,Location,Findings,Remarks,Requested,Status,Claimby,Date FROM tblaccomplish WHERE Fk LIKE '%" & MDIMain.tslnum.Text & "%' And Deleted = 'No' ORDER BY ID DESC", dvgstudent)
                Call populate()
            Else
                Dim cmd As Odbc.OdbcCommand
                Dim sql As String

                sql = "UPDATE tblaccomplish SET Division = ?, Section = ?, Nature = ?, Categories = ?, Location = ?, Findings = ?, Remarks = ?, Requested = ?, Status = ?, Datefinish = ?, Doneby = ? WHERE ID = " & Val(dvgstudent.Tag) & ""
                '"UPDATE tblnature Set Nature = ?, Categories = ? WHERE ID = " & Val(dvgstudent.Tag) & ""
                MsgBox("SAVED SUCCESSFULLY", vbInformation, "Accomplish Information")
                cmd = New Odbc.OdbcCommand(sql, modCon.con)
                cmd.Parameters.AddWithValue("?", cbDivision.SelectedItem)
                cmd.Parameters.AddWithValue("?", cbSection.SelectedItem)
                cmd.Parameters.AddWithValue("?", cbNature.SelectedItem)
                cmd.Parameters.AddWithValue("?", cmbNature.SelectedItem)
                cmd.Parameters.AddWithValue("?", cbLocation.SelectedItem)
                cmd.Parameters.AddWithValue("?", txtFindings.Text)
                cmd.Parameters.AddWithValue("?", txtRemarks.Text)
                cmd.Parameters.AddWithValue("?", txtRequest.Text)
                cmd.Parameters.AddWithValue("?", cmbStatus.SelectedItem)
                cmd.Parameters.AddWithValue("?", Date.Now.ToString("dd-MM-yyyy") + "  |  " + Date.Now.ToLongTimeString())
                cmd.Parameters.AddWithValue("?", lblDoneby.Text)
                cmd.ExecuteNonQuery()


                '                Call modCon.save("UPDATE tblaccomplish Set Division = ?, Section = ?, Nature = ?, Categories = ?, Location = ?, Findings = ?, Remarks = ?, Requested = ?, Date = ?, Fk = ? WHERE ID = " & Val(dvgstudent.Tag) & "VALUES(?,?,?,?,?,?,?,?,?,?)")
                '               MsgBox("UPDATED SUCCESSFULLY", vbInformation, "Accomplish Information")
                dvgstudent.Tag = ""
                cbDivision.SelectedIndex = -1
                txtFindings.Text = ""
                txtRemarks.Text = ""
                txtRequest.Text = ""
                cbSection.SelectedIndex = -1
                cbLocation.SelectedIndex = -1
                cbNature.SelectedIndex = -1
                cmbNature.SelectedIndex = -1
                cmbStatus.SelectedIndex = -1
                cbNature.Text = ""
                'Call loadData("Select ID,Division,Section,Nature,Categories,Location,Findings,Remarks,Requested,Status,Date FROM tblaccomplish WHERE Fk2 Like '%" & MDIMain.tslUser.Text & "%'", dvgstudent)
                Call loadData("SELECT ID,Division,Section,Nature,Categories,Location,Findings,Remarks,Requested,Status,Claimby,Date FROM tblaccomplish WHERE Fk LIKE '%" & MDIMain.tslnum.Text & "%' And Deleted = 'No' ORDER BY ID DESC", dvgstudent)
                Call populate()
            End If
        End If
    End Sub

    'To select data from datagrid going to their fields
    Private Sub dvgstudent_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dvgstudent.CellClick

        If e.RowIndex >= 0 Then
            dvgstudent.Tag = dvgstudent.Item(0, e.RowIndex).Value
            cbDivision.Text = dvgstudent.Item(1, e.RowIndex).Value
            cbSection.Text = dvgstudent.Item(2, e.RowIndex).Value
            cbNature.Text = dvgstudent.Item(3, e.RowIndex).Value
            cmbNature.Text = dvgstudent.Item(4, e.RowIndex).Value
            cbLocation.Text = dvgstudent.Item(5, e.RowIndex).Value
            txtFindings.Text = dvgstudent.Item(6, e.RowIndex).Value
            txtRemarks.Text = dvgstudent.Item(7, e.RowIndex).Value
            txtRequest.Text = dvgstudent.Item(8, e.RowIndex).Value
            cmbStatus.Text = dvgstudent.Item(9, e.RowIndex).Value

        End If

        If cmbStatus.Text = "Done" Then
            Call enabler()
        Else
            If cmbStatus.Text = "Ongoing" Then
                cbDivision.Enabled = False
                cbSection.Enabled = False
                cbNature.Enabled = False
                cmbNature.Enabled = False
                cbLocation.Enabled = False
                txtFindings.Enabled = True
                txtRemarks.Enabled = True
                txtRequest.Enabled = False
                btnSave.Enabled = True
                cmbStatus.Enabled = True
            End If

            'Call enabler1()
        End If

    End Sub

    Private Sub enabler1()
        cbDivision.Enabled = True
        cbSection.Enabled = True
        cbNature.Enabled = True
        cmbNature.Enabled = True
        cbLocation.Enabled = True
        txtFindings.Enabled = True
        txtRemarks.Enabled = True
        txtRequest.Enabled = True
        btnSave.Enabled = True
        cmbStatus.Enabled = True
    End Sub

    Private Sub enabler()
        cbDivision.Enabled = False
        cbSection.Enabled = False
        cbNature.Enabled = False
        cmbNature.Enabled = False
        cbLocation.Enabled = False
        txtFindings.Enabled = False
        txtRemarks.Enabled = False
        txtRequest.Enabled = False
        cmbStatus.Enabled = False
        btnSave.Enabled = False
        btnDelete.Enabled = False
    End Sub

    'To clear the selected data from their fields
    Private Sub btnClear_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClear.Click
        cbDivision.SelectedIndex = -1
        cmbStatus.SelectedIndex = -1
        txtFindings.Text = ""
        txtRemarks.Text = ""
        dvgstudent.Text = ""
        cbSection.SelectedIndex = -1
        cbLocation.Text = ""
        cbNature.Text = ""
        cbLocation.SelectedIndex = -1
        cbNature.SelectedIndex = -1
        cmbNature.SelectedIndex = -1
        txtRequest.Text = ""
        Call populate()
        Call enabler()
    End Sub

    'Load the data from tbllocation
    Private Sub loadComboLoc()
        Dim da As OdbcDataAdapter
        Dim dt As New DataTable
        Dim sql As String = "SELECT Location FROM tbllocation"
        da = New OdbcDataAdapter(sql, modCon.con)
        da.Fill(dt)
        cbLocation.Items.Clear()
        cbLocation.Items.Add("")
        For x = 0 To dt.Rows.Count - 1
            cbLocation.Items.Add(dt.Rows(x)(0))
        Next

    End Sub

    'Load the data from tblnature
    Private Sub loadComboNat()
        Dim da As OdbcDataAdapter
        Dim dt As New DataTable
        Dim sql As String = "SELECT Nature FROM tblnature"
        da = New OdbcDataAdapter(sql, modCon.con)
        da.Fill(dt)
        cbNature.Items.Clear()
        cbNature.Items.Add("")
        For x = 0 To dt.Rows.Count - 1
            cbNature.Items.Add(dt.Rows(x)(0))
        Next

    End Sub

    'Load the data in categories from tblnature where there is hardware 
    Private Sub loadComboCat1()

        Dim da As OdbcDataAdapter
        Dim dt As New DataTable
        Dim sql As String = "SELECT Categories FROM tblnature WHERE Nature LIKE 'Hardware'"
        da = New OdbcDataAdapter(sql, modCon.con)
        da.Fill(dt)
        cmbNature.Items.Clear()
        cmbNature.Items.Add("")
        For x = 0 To dt.Rows.Count - 1
            cmbNature.Items.Add(dt.Rows(x)(0))
        Next

    End Sub

    'Load the data in categories from tblnature where there is software
    Private Sub loadComboCat2()

        Dim da As OdbcDataAdapter
        Dim dt As New DataTable
        Dim sql As String = "SELECT Categories FROM tblnature WHERE Nature LIKE 'Software'"
        da = New OdbcDataAdapter(sql, modCon.con)
        da.Fill(dt)
        cmbNature.Items.Clear()
        cmbNature.Items.Add("")
        For x = 0 To dt.Rows.Count - 1
            cmbNature.Items.Add(dt.Rows(x)(0))
        Next

    End Sub

    'Load the data in categories from tblnature where there is network
    Private Sub loadComboCat3()

        Dim da As OdbcDataAdapter
        Dim dt As New DataTable
        Dim sql As String = "SELECT Categories FROM tblnature WHERE Nature LIKE 'Network'"
        da = New OdbcDataAdapter(sql, modCon.con)
        da.Fill(dt)
        cmbNature.Items.Clear()
        cmbNature.Items.Add("")
        For x = 0 To dt.Rows.Count - 1
            cmbNature.Items.Add(dt.Rows(x)(0))
        Next

    End Sub

    'Load the data in categories from tblnature where there is printer
    Private Sub loadComboCat4()

        Dim da As OdbcDataAdapter
        Dim dt As New DataTable
        Dim sql As String = "SELECT Categories FROM tblnature WHERE Nature LIKE 'Printer'"
        da = New OdbcDataAdapter(sql, modCon.con)
        da.Fill(dt)
        cmbNature.Items.Clear()
        cmbNature.Items.Add("")
        For x = 0 To dt.Rows.Count - 1
            cmbNature.Items.Add(dt.Rows(x)(0))
        Next

    End Sub

    'Load the data categories from tbldivision
    Private Sub loadComboSec()
        Dim da As OdbcDataAdapter
        Dim dt As New DataTable
        Dim sql As String = "SELECT Categories FROM tbldivision"
        da = New OdbcDataAdapter(sql, modCon.con)
        da.Fill(dt)
        cbSection.Items.Clear()
        cbSection.Items.Add("")
        For x = 0 To dt.Rows.Count - 1
            cbSection.Items.Add(dt.Rows(x)(0))
        Next

    End Sub

    'For the exit of form
    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        If MsgBox("Are you sure you want to close?", vbQuestion + vbYesNo, "PRINT") = vbYes Then

            Me.Hide()
        End If
    End Sub

    'To select data from tblnature
    Private Sub cbNature_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbNature.SelectedIndexChanged
        cmbNature.SelectedIndex = -1
        If cbNature.Text = "Hardware" Then
            Call loadComboCat1()

        Else
            If cbNature.Text = "Software" Then
                Call loadComboCat2()

            Else
                If cbNature.Text = "Network" Then
                    Call loadComboCat3()

                Else
                    If cbNature.Text = "Printer" Then
                        Call loadComboCat4()

                    End If
                End If
            End If
        End If
    End Sub

    'To select data from tbldivision
    Private Sub cbDivision_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbDivision.SelectedIndexChanged
        cbSection.SelectedIndex = -1
        If cbDivision.Text = "AFS" Then
            Call loadComboCata()
        Else
            If cbDivision.Text = "CDRR" Then
                Call loadComboCatb()
            Else
                If cbDivision.Text = "CFRR" Then
                    Call loadComboCatc()
                Else
                    If cbDivision.Text = "CCRR" Then
                        Call loadComboCatk()
                    Else
                        If cbDivision.Text = "CDRRHR" Then
                            Call loadComboCatd()

                        Else
                            If cbDivision.Text = "CSL" Then
                                Call loadComboCatf()
                            Else
                                If cbDivision.Text = "ODG" Then
                                    Call loadComboCatg()
                                Else
                                    If cbDivision.Text = "REU" Then
                                        Call loadComboCath()
                                    Else
                                        If cbDivision.Text = "FROO" Then
                                            Call loadComboCati()
                                        Else
                                            If cbDivision.Text = "PPS" Then
                                                Call loadComboCatj()
                                            End If
                                        End If
                                    End If
                                End If
                            End If
                        End If
                    End If
                End If
            End If
        End If

    End Sub

    'Load the data in categories from tbldivision where the division is like to afs
    Private Sub loadComboCata()

        Dim da As OdbcDataAdapter
        Dim dt As New DataTable
        Dim sql As String = "SELECT Categories FROM tbldivision WHERE Division LIKE 'AFS'"
        da = New OdbcDataAdapter(sql, modCon.con)
        da.Fill(dt)
        cbSection.Items.Clear()
        cbSection.Items.Add("")
        For x = 0 To dt.Rows.Count - 1
            cbSection.Items.Add(dt.Rows(x)(0))
        Next

    End Sub

    'Load the data in categories from tbldivision where the division is like to cdrr
    Private Sub loadComboCatb()

        Dim da As OdbcDataAdapter
        Dim dt As New DataTable
        Dim sql As String = "SELECT Categories FROM tbldivision WHERE Division LIKE 'CDRR'"
        da = New OdbcDataAdapter(sql, modCon.con)
        da.Fill(dt)
        cbSection.Items.Clear()
        cbSection.Items.Add("")
        For x = 0 To dt.Rows.Count - 1
            cbSection.Items.Add(dt.Rows(x)(0))
        Next

    End Sub

    'Load the data in categories from tbldivision where the division is like to cfrr
    Private Sub loadComboCatc()

        Dim da As OdbcDataAdapter
        Dim dt As New DataTable
        Dim sql As String = "SELECT Categories FROM tbldivision WHERE Division LIKE 'CFRR'"
        da = New OdbcDataAdapter(sql, modCon.con)
        da.Fill(dt)
        cbSection.Items.Clear()
        cbSection.Items.Add("")
        For x = 0 To dt.Rows.Count - 1
            cbSection.Items.Add(dt.Rows(x)(0))
        Next

    End Sub

    'Load the data in categories from tbldivision where the division is like to cdrrhr
    Private Sub loadComboCatd()

        Dim da As OdbcDataAdapter
        Dim dt As New DataTable
        Dim sql As String = "SELECT Categories FROM tbldivision WHERE Division LIKE 'CDRRHR'"
        da = New OdbcDataAdapter(sql, modCon.con)
        da.Fill(dt)
        cbSection.Items.Clear()
        cbSection.Items.Add("")
        For x = 0 To dt.Rows.Count - 1
            cbSection.Items.Add(dt.Rows(x)(0))
        Next

    End Sub

    'Load the data in categories from tbldivision where the division is like to csl
    Private Sub loadComboCatf()

        Dim da As OdbcDataAdapter
        Dim dt As New DataTable
        Dim sql As String = "SELECT Categories FROM tbldivision WHERE Division LIKE 'CSL'"
        da = New OdbcDataAdapter(sql, modCon.con)
        da.Fill(dt)
        cbSection.Items.Clear()
        cbSection.Items.Add("")
        For x = 0 To dt.Rows.Count - 1
            cbSection.Items.Add(dt.Rows(x)(0))
        Next

    End Sub

    'Load the data in categories from tbldivision where the division is like to odg
    Private Sub loadComboCatg()

        Dim da As OdbcDataAdapter
        Dim dt As New DataTable
        Dim sql As String = "SELECT Categories FROM tbldivision WHERE Division LIKE 'ODG'"
        da = New OdbcDataAdapter(sql, modCon.con)
        da.Fill(dt)
        cbSection.Items.Clear()
        cbSection.Items.Add("")
        For x = 0 To dt.Rows.Count - 1
            cbSection.Items.Add(dt.Rows(x)(0))
        Next

    End Sub

    'Load the data in categories from tbldivision where the division is like to reu
    Private Sub loadComboCath()

        Dim da As OdbcDataAdapter
        Dim dt As New DataTable
        Dim sql As String = "SELECT Categories FROM tbldivision WHERE Division LIKE 'REU'"
        da = New OdbcDataAdapter(sql, modCon.con)
        da.Fill(dt)
        cbSection.Items.Clear()
        cbSection.Items.Add("")
        For x = 0 To dt.Rows.Count - 1
            cbSection.Items.Add(dt.Rows(x)(0))
        Next

    End Sub

    'Load the data in categories from tbldivision where the division is like to froo
    Private Sub loadComboCati()

        Dim da As OdbcDataAdapter
        Dim dt As New DataTable
        Dim sql As String = "SELECT Categories FROM tbldivision WHERE Division LIKE 'FROO'"
        da = New OdbcDataAdapter(sql, modCon.con)
        da.Fill(dt)
        cbSection.Items.Clear()
        cbSection.Items.Add("")
        For x = 0 To dt.Rows.Count - 1
            cbSection.Items.Add(dt.Rows(x)(0))
        Next

    End Sub

    'Load the data in categories from tbldivision where the division is like to pps
    Private Sub loadComboCatj()

        Dim da As OdbcDataAdapter
        Dim dt As New DataTable
        Dim sql As String = "SELECT Categories FROM tbldivision WHERE Division LIKE 'PPS'"
        da = New OdbcDataAdapter(sql, modCon.con)
        da.Fill(dt)
        cbSection.Items.Clear()
        cbSection.Items.Add("")
        For x = 0 To dt.Rows.Count - 1
            cbSection.Items.Add(dt.Rows(x)(0))
        Next

    End Sub

    'Load the data in categories from tbldivision where the division is like to pps
    Private Sub loadComboCatk()

        Dim da As OdbcDataAdapter
        Dim dt As New DataTable
        Dim sql As String = "SELECT Categories FROM tbldivision WHERE Division LIKE 'CCRR'"
        da = New OdbcDataAdapter(sql, modCon.con)
        da.Fill(dt)
        cbSection.Items.Clear()
        cbSection.Items.Add("")
        For x = 0 To dt.Rows.Count - 1
            cbSection.Items.Add(dt.Rows(x)(0))
        Next

    End Sub

    'To clean all the entries
    Private Sub cleaner()
        Dim obj As Object
        For Each obj In grpEntry.Controls
            If TypeOf obj Is TextBox Then
                obj.clear()
            End If
        Next
    End Sub



    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        'Call loadData("SELECT ID,Division,Section,Nature,Categories,Location,Findings,Remarks,Requested,Status,Date FROM tblaccomplish WHERE Fk2 LIKE '%" & MDIMain.tslUser.Text & "%'", dvgstudent)
        Call loadData("SELECT ID,Division,Section,Nature,Categories,Location,Findings,Remarks,Requested,Status,Claimby,Date FROM tblaccomplish WHERE Fk LIKE '%" & MDIMain.tslnum.Text & "%' And Deleted = 'No' ORDER BY ID DESC", dvgstudent)
    End Sub
    'DELETE
    Private Sub btnDelete_Click(sender As Object, e As EventArgs) Handles btnDelete.Click
        If Len(dvgstudent.Tag) = 0 Then
            MsgBox("Please select record to delete!", vbExclamation, "User Account")
        Else
            Dim cmd As Odbc.OdbcCommand
            Dim sql As String = "UPDATE tblaccomplish SET Deleted = 'Y' WHERE ID = " & Val(dvgstudent.Tag) & " "
            cmd = New Odbc.OdbcCommand(sql, modCon.con)
            If MsgBox("Are you sure you want to delete?", vbQuestion + vbYesNo, "User Account") = vbYes Then
                cmd.ExecuteNonQuery()
                MsgBox("Successfully Deleted!", vbInformation, "User Account")
                Call cleaner()
                dvgstudent.Tag = ""
                Call populate()
                'Call loadData("SELECT ID,Division,Section,Nature,Categories,Location,Findings,Remarks,Requested,Status,Claimby,Date FROM tblaccomplish WHERE Fk LIKE '%" & MDIMain.tslnum.Text & "%' AND Deleted = 'No' ORDER BY ID DESC", dvgstudent)

            Else
                Call cleaner()
                dvgstudent.Tag = ""
            End If
        End If

    End Sub

    Private Sub Panel1_Paint(sender As Object, e As PaintEventArgs) Handles Panel1.Paint

    End Sub

    Private Sub Label1_Click(sender As Object, e As EventArgs) Handles Label1.Click

    End Sub
End Class