﻿Imports System.Data.Odbc
Imports System.IO
Imports System
Imports System.Threading
Imports System.Drawing.Printing
Public Class frmSearch

    Private Sub frmSearch_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Call loadData("SELECT * FROM tblaccomplish", dgvStudent)
        Call FromDatabase()
        Timer1.Start()
    End Sub
    Private bitmap As Bitmap

    'For printing
    'Ito po yung isa sa mga tatanggalin ko para di po redundant yung printing hehehe
    Private Sub PrintDocument1_PrintPage(ByVal sender As System.Object, ByVal e As System.Drawing.Printing.PrintPageEventArgs) Handles PrintDocument1.PrintPage
        PrintDialog1.PrinterSettings = PrintDocument1.PrinterSettings
        If PrintDialog1.ShowDialog() = Windows.Forms.DialogResult.OK Then
            PrintDocument1.PrinterSettings = PrintDialog1.PrinterSettings
            Dim pagesetup As New PageSettings
            With pagesetup
                .Margins.Left = 50
                .Margins.Right = 50
                .Margins.Top = 50
                .Margins.Bottom = 50
                .Landscape = False
            End With
            PrintDocument1.DefaultPageSettings = pagesetup
        End If
        PrintPreviewDialog1.Document = PrintDocument1

        Dim bm As New Bitmap(Me.dgvStudent.Width, Me.dgvStudent.Height)

        dgvStudent.DrawToBitmap(bm, New Rectangle(0, 0, Me.dgvStudent.Width, Me.dgvStudent.Height))

        e.Graphics.DrawImage(bm, 0, 0)
    End Sub

    'To show all the data from tblaccomplish in datagridview
    Private Sub FromDatabase()
        Dim OleDBC As New OdbcCommand
        Dim OleDBDR As OdbcDataReader
        Dim c As Integer
        c = 0
        With OleDBC
            .Connection = modCon.con
            .CommandText = "SELECT * FROM tblaccomplish "
        End With
        OleDBDR = OleDBC.ExecuteReader
        dgvStudent.Rows.Clear()
        If OleDBDR.HasRows Then
            While OleDBDR.Read
                dgvStudent.Rows.Add()
                dgvStudent.Item(0, c).Value = OleDBDR.Item(0)
                dgvStudent.Item(1, c).Value = OleDBDR.Item(1)
                dgvStudent.Item(2, c).Value = OleDBDR.Item(2)
                dgvStudent.Item(3, c).Value = OleDBDR.Item(3)
                dgvStudent.Item(4, c).Value = OleDBDR.Item(4)
                dgvStudent.Item(5, c).Value = OleDBDR.Item(5)
                dgvStudent.Item(6, c).Value = OleDBDR.Item(6)
                dgvStudent.Item(7, c).Value = OleDBDR.Item(7)
                dgvStudent.Item(8, c).Value = OleDBDR.Item(8)
                dgvStudent.Item(9, c).Value = OleDBDR.Item(9)
                dgvStudent.Item(10, c).Value = OleDBDR.Item(10)
                dgvStudent.Item(11, c).Value = OleDBDR.Item(11)
                dgvStudent.Item(12, c).Value = OleDBDR.Item(12)
                dgvStudent.Item(13, c).Value = OleDBDR.Item(13)
                dgvStudent.Item(14, c).Value = OleDBDR.Item(14)
                dgvStudent.Item(15, c).Value = OleDBDR.Item(15)
                dgvStudent.Item(16, c).Value = OleDBDR.Item(16)
                dgvStudent.Item(17, c).Value = OleDBDR.Item(17)


                c = c + 1
            End While
        End If
        If OleDBDR.HasRows Then
            lblSearchResult.Text = "Search Results :"
            lblSearchCount.Text = dgvStudent.RowCount

        Else
            lblSearchResult.Text = "Search Results :"
            lblSearchCount.Text = "0"
            dgvStudent.Rows.Clear()
        End If
    End Sub


    Private Sub btnPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        PrintDocument1.Print()
    End Sub

    'Print preview
    Private Sub btnPrintPreview_Click(sender As Object, e As EventArgs)
        PrintPreviewDialog1.ShowDialog()
    End Sub
    Private Sub PrintPreviewDialog1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PrintPreviewDialog1.Load
        PrintDialog1.PrinterSettings = PrintDocument1.PrinterSettings
        If PrintDialog1.ShowDialog() = Windows.Forms.DialogResult.OK Then
            PrintDocument1.PrinterSettings = PrintDialog1.PrinterSettings
            Dim pagesetup As New PageSettings
            With pagesetup
                .Margins.Left = 150
                .Margins.Right = 150
                .Margins.Top = 150
                .Margins.Bottom = 150
                .Landscape = False
            End With
            PrintDocument1.DefaultPageSettings = pagesetup
        End If
        PrintDocument1.DefaultPageSettings.Landscape = True
        PrintPreviewDialog1.Document = PrintDocument1
    End Sub

    'Button to load the data when there are selected dates
    Private Sub btnload_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnload.Click
        Call SearchResult()
    End Sub

    'Select dates from datepicker
    Private Sub SearchResult()
        Dim OleDBC As New System.Data.Odbc.OdbcCommand
        Dim OleDBDR As System.Data.Odbc.OdbcDataReader
        Dim c As Integer
        Dim FilterDate As Date
        c = 0


        Dim FilterDate2 As Date = DateTimePicker2.Value.Date
        Dim FilterDate1 As Date = DateTimePicker1.Value.Date

        If FilterDate2.Date.Day >= DateTimePicker2.Value.Date.Day And FilterDate1.Date.Day <= DateTimePicker1.Value.Date.Day Then
            Dim FilterDate21 = FilterDate2.ToString("yyyy-MM-dd")
            Dim FilterDate31 = FilterDate1.ToString("yyyy-MM-dd")
            With OleDBC
                .Connection = modCon.con
                .CommandText = "SELECT * FROM tblaccomplish WHERE STR_TO_DATE(DateRequested, '%d-%m-%Y') BETWEEN '" & FilterDate21 & "' AND '" & FilterDate31 & "'"
            End With
            OleDBDR = OleDBC.ExecuteReader
            dgvStudent.Rows.Clear()
            If OleDBDR.HasRows Then

                While OleDBDR.Read
                        dgvStudent.Rows.Add()
                        dgvStudent.Item(0, c).Value = OleDBDR.Item(0)
                        dgvStudent.Item(1, c).Value = OleDBDR.Item(1)
                        dgvStudent.Item(2, c).Value = OleDBDR.Item(2)
                        dgvStudent.Item(3, c).Value = OleDBDR.Item(3)
                        dgvStudent.Item(4, c).Value = OleDBDR.Item(4)
                        dgvStudent.Item(5, c).Value = OleDBDR.Item(5)
                        dgvStudent.Item(6, c).Value = OleDBDR.Item(6)
                        dgvStudent.Item(7, c).Value = OleDBDR.Item(7)
                        dgvStudent.Item(8, c).Value = OleDBDR.Item(8)
                        dgvStudent.Item(9, c).Value = OleDBDR.Item(9)
                        dgvStudent.Item(10, c).Value = OleDBDR.Item(10)
                        dgvStudent.Item(11, c).Value = OleDBDR.Item(11)
                        dgvStudent.Item(12, c).Value = OleDBDR.Item(12)
                        dgvStudent.Item(13, c).Value = OleDBDR.Item(13)
                        dgvStudent.Item(14, c).Value = OleDBDR.Item(14)
                        dgvStudent.Item(15, c).Value = OleDBDR.Item(15)
                        dgvStudent.Item(16, c).Value = OleDBDR.Item(16)
                        dgvStudent.Item(17, c).Value = OleDBDR.Item(17)

                        c = c + 1
                End While
            End If

        End If
        If OleDBDR.HasRows Then
            lblSearchResult.Text = "Search Results :"
            lblSearchCount.Text = dgvStudent.RowCount

        Else
            lblSearchResult.Text = "Search Results :"
                    lblSearchCount.Text = "0"
                    dgvStudent.Rows.Clear()
                End If

    End Sub

    'To load the data from tblaccomplish when there are selected and inputed data
    Private Sub SearchResult2()
        Dim OleDBC As New System.Data.Odbc.OdbcCommand
        Dim OleDBDR As System.Data.Odbc.OdbcDataReader
        Dim c As Integer
        Dim search As String = ""
        c = 0
        With OleDBC
            .Connection = modCon.con
            .CommandText = "SELECT * FROM tblaccomplish "
        End With
        OleDBDR = OleDBC.ExecuteReader
        dgvStudent.Rows.Clear()
        If OleDBDR.HasRows Then
            If cmbSearch.Text = "" Then
                MsgBox("Please enter your Query", vbExclamation, "SEARCH")
                Call FromDatabase()
            Else
                While OleDBDR.Read

                    If cmbSearch.Text = "Claim by" Then
                        search = OleDBDR.Item(11).ToString()
                    ElseIf cmbSearch.Text = "Status" Then
                        search = OleDBDR.Item(10).ToString()
                    ElseIf cmbSearch.Text = "Requested by" Then
                        search = OleDBDR.Item(8).ToString()
                    End If

                    If search.ToLower() = txtSearch.Text.ToLower() Then
                        dgvStudent.Rows.Add()
                        dgvStudent.Item(0, c).Value = OleDBDR.Item(0).ToString()
                        dgvStudent.Item(1, c).Value = OleDBDR.Item(1).ToString()
                        dgvStudent.Item(2, c).Value = OleDBDR.Item(2).ToString()
                        dgvStudent.Item(3, c).Value = OleDBDR.Item(3).ToString()
                        dgvStudent.Item(4, c).Value = OleDBDR.Item(4).ToString()
                        dgvStudent.Item(5, c).Value = OleDBDR.Item(5).ToString()
                        dgvStudent.Item(6, c).Value = OleDBDR.Item(6).ToString()
                        dgvStudent.Item(7, c).Value = OleDBDR.Item(7).ToString()
                        dgvStudent.Item(8, c).Value = OleDBDR.Item(8).ToString()
                        dgvStudent.Item(9, c).Value = OleDBDR.Item(9).ToString()
                        dgvStudent.Item(10, c).Value = OleDBDR.Item(10).ToString()
                        dgvStudent.Item(11, c).Value = OleDBDR.Item(11).ToString()
                        dgvStudent.Item(12, c).Value = OleDBDR.Item(12).ToString()
                        dgvStudent.Item(13, c).Value = OleDBDR.Item(13).ToString()
                        dgvStudent.Item(14, c).Value = OleDBDR.Item(14).ToString()
                        dgvStudent.Item(15, c).Value = OleDBDR.Item(15).ToString()
                        dgvStudent.Item(16, c).Value = OleDBDR.Item(16).ToString()
                        dgvStudent.Item(17, c).Value = OleDBDR.Item(17).ToString()

                        c = c + 1
                    End If

                End While
            End If

        End If
        If OleDBDR.HasRows Then
            lblSearchResult.Text = "Search Results :"
            lblSearchCount.Text = dgvStudent.RowCount

        Else
            lblSearchResult.Text = "Search Results :"
            lblSearchCount.Text = "0"
            dgvStudent.Rows.Clear()
        End If
    End Sub

    'Button search to load the data when there are selected and inputted data
    Private Sub btnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Call SearchResult2()
        Call clean()
    End Sub

    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick
        'Call FromDatabase()
    End Sub

    'To exit the form
    Private Sub btnclose_Click(sender As Object, e As EventArgs) Handles btnclose.Click
        If MsgBox("Are you sure you want to close?", vbQuestion + vbYesNo, "SEARCH") = vbYes Then
            Call clean()
            Me.Hide()

        End If
    End Sub

    'To clean all the entries
    Private Sub clean()
        txtSearch.Clear()
        cmbSearch.SelectedIndex = -1
        lblSearchCount.Text = ""
    End Sub

    Private Sub btnRefresh_Click(sender As Object, e As EventArgs) Handles btnRefresh.Click
        Call FromDatabase()
    End Sub
End Class