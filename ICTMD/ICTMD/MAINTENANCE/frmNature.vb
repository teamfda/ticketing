﻿Imports System.Data.Odbc
Public Class frmNature
    Sub populate()
        Call loadData("SELECT ID,Nature,Categories FROM tblnature WHERE Deleted = 'N'", dgvUser)
    End Sub

    'Load the data in datagridview
    Private Sub frmNature_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        dgvUser.Tag = ""
        cmbCategories.SelectedIndex = -1
        txtdef.Text = ""
        Call populate()
        'Timer1.Start()
        Call enabler(True)

    End Sub

    'Coondition for buttons
    Private Sub enabler(ByVal bool As Boolean)
        btnAdd.Enabled = bool
        btnEdit.Enabled = bool
        btnDel.Enabled = bool
        btnSave.Enabled = Not bool
        btnCancel.Enabled = Not bool
        btnClose.Enabled = bool
        grpEntry.Enabled = Not bool
        dgvUser.Enabled = bool
    End Sub

    'To enable the data for editing
    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        If Len(dgvUser.Tag) = 0 Then
            MsgBox("Please select record to update!", vbExclamation, "NATURE")
        Else
            Call enabler(False)

        End If
    End Sub

    'To cancel the current performing task
    Private Sub btnCancel_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        If MsgBox("Are you sure you want to cancel?", vbQuestion + vbYesNo, "NATURE") = vbYes Then
            Call enabler(True)
            dgvUser.Tag = ""
            Call cleaner()
            cmbCategories.SelectedIndex = -1
        End If
    End Sub

    'To delete the selected data
    Private Sub btnDel_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDel.Click
        If Len(dgvUser.Tag) = 0 Then
            MsgBox("Please select record to delete!", vbExclamation, "NATURE")
        Else
            Dim cmd As Odbc.OdbcCommand
            Dim sql As String = "UPDATE tblnature SET Deleted = 'Y' WHERE ID = " & Val(dgvUser.Tag) & ""
            cmd = New Odbc.OdbcCommand(sql, modCon.con)
            If MsgBox("Are you sure you want to delete?", vbQuestion + vbYesNo, "NATURE") = vbYes Then
                cmd.ExecuteNonQuery()
                MsgBox("Successfully Deleted!", vbInformation, "NATURE")
                Call cleaner()
                dgvUser.Tag = ""
                'Call loadData("SELECT ID,Nature,Categories,Priority FROM tblnature", dgvUser)
                Call populate()
                cmbCategories.SelectedIndex = -1
            Else
                Call cleaner()
                dgvUser.Tag = ""
            End If
        End If
    End Sub

    'To exit the division form
    Private Sub btnClose_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        If MsgBox("Are you sure you want to close?", vbQuestion + vbYesNo, "NATURE") = vbYes Then
            Me.Hide()
        End If
    End Sub

    'For selection of data from datagridview
    Private Sub dgvUser_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvUser.CellClick
        If e.RowIndex >= 0 Then
            dgvUser.Tag = dgvUser.Item(0, e.RowIndex).Value
            cmbCategories.Text = dgvUser.Item(1, e.RowIndex).Value
            txtdef.Text = dgvUser.Item(2, e.RowIndex).Value

        End If
    End Sub

    'Clean textbox
    Private Sub cleaner()
        Dim obj As Object
        For Each obj In grpEntry.Controls
            If TypeOf obj Is TextBox Then
                obj.clear()
            End If
        Next
    End Sub

    'To save the new added data and updated data
    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim cmd As Odbc.OdbcCommand
        Dim sql As String = ""
        Dim da As OdbcDataAdapter
        Try
            If txtdef.Text = "" Or cmbCategories.Text = "" Then
                MsgBox("Please enter your Query", vbExclamation, "NATURE")
                Call enabler(True)
            Else
                If Len(dgvUser.Tag) = 0 Then
                    Dim dt As New DataTable
                    Dim sql1 As String = "SELECT * FROM tblnature WHERE Nature = '" & cmbCategories.Text & "' AND Categories = '" & txtdef.Text & "' "
                    da = New OdbcDataAdapter(sql1, modCon.con)
                    da.Fill(dt)
                    If dt Is Nothing Then
                        sql = "INSERT INTO tblnature(Nature,Categories)VALUES(?,?)"
                        MsgBox("Successfully Added!", vbInformation, "NATURE")
                        Call enabler(True)
                    Else
                        MsgBox("Record Already Exist!", vbExclamation, "User Account")
                    End If

                Else
                    Dim dt As New DataTable
                    Dim sql1 As String = "SELECT * FROM tblnature WHERE Nature = '" & cmbCategories.Text & "' AND Categories = '" & txtdef.Text & "' "
                    da = New OdbcDataAdapter(sql1, modCon.con)
                    da.Fill(dt)

                    If dt.Rows.Count <> 1 Then
                        sql = "UPDATE tblnature SET Nature = ?, Categories = ? WHERE ID = " & Val(dgvUser.Tag) & ""
                        MsgBox("Successfully Updated!", vbInformation, "NATURE")
                        Call enabler(True)
                    Else
                        MsgBox("Record Already Exist!", vbExclamation, "User Account")
                    End If

                End If
            End If

            cmd = New Odbc.OdbcCommand(sql, modCon.con)
            cmd.Parameters.AddWithValue("?", cmbCategories.Text)
            cmd.Parameters.AddWithValue("?", txtdef.Text)
            cmd.ExecuteNonQuery()
        Catch ex As Exception
        End Try
        Call populate()
        'Call loadData("SELECT ID,Nature,Categories,Priority FROM tblnature", dgvUser)
        cmbCategories.SelectedIndex = -1
        Call cleaner()
    End Sub

    'To add new record in division form
    Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        If MsgBox("Do you want to add a new record?", vbQuestion + vbYesNo, "NATURE") = vbYes Then
            dgvUser.Tag = ""
            cmbCategories.Text = ""
            txtdef.Text = ""
            Call cleaner()
            Call enabler(False)
        End If
    End Sub

    'Refresh the datagridview
    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick
        Call loadData("SELECT ID,Nature,Categories,Priority FROM tblnature WHERE Deleted = 'N'", dgvUser)
    End Sub

End Class