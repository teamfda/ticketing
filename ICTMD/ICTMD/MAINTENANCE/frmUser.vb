﻿Imports System.Data.Odbc
Public Class frmUser
    Sub populate()
        Call loadData("SELECT user_id,fname,lname,pos,uname,pword,isactive FROM tbluser WHERE Deleted = 'N'", dgvUser)
    End Sub

    'Load the data in datagridview
    Private Sub frmUser_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Call populate()
        'Timer1.Start()
        Call enabler(True)
    End Sub

    'Condition of buttons
    Private Sub enabler(ByVal bool As Boolean)
        btnAdd.Enabled = bool
        btnEdit.Enabled = bool
        btnDel.Enabled = bool
        btnSave.Enabled = Not bool
        btnCancel.Enabled = Not bool
        btnClose.Enabled = bool
        grpEntry.Enabled = Not bool
        dgvUser.Enabled = bool
    End Sub

    'To add new record in division form
    Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        If MsgBox("Do you want to add a new record?", vbQuestion + vbYesNo, "User Account") = vbYes Then
            dgvUser.Tag = ""
            Call cleaner()
            Call enabler(False)
            txtfname.Enabled = True
            txtlname.Enabled = True
        End If
    End Sub

    'To enable the data for editing
    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        If Len(dgvUser.Tag) = 0 Then
            MsgBox("Please select record to update!", vbExclamation, "User Account")
        Else
            Call enabler(False)
            txtfname.Enabled = False
            txtlname.Enabled = False

        End If
    End Sub

    'To cancel the current performing task
    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        If MsgBox("Are you sure you want to cancel?", vbQuestion + vbYesNo, "User Account") = vbYes Then
            Call enabler(True)
            dgvUser.Tag = ""
            Call cleaner()
        End If
    End Sub

    'To delete the selected data
    Private Sub btnDel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDel.Click
        If Len(dgvUser.Tag) = 0 Then
            MsgBox("Please select record to delete!", vbExclamation, "User Account")
        Else
            Dim cmd As Odbc.OdbcCommand
            Dim sql As String = "UPDATE tbluser SET Deleted = 'Y' WHERE user_id = " & Val(dgvUser.Tag) & ""
            cmd = New Odbc.OdbcCommand(sql, modCon.con)
            If MsgBox("Are you sure you want to delete?", vbQuestion + vbYesNo, "User Account") = vbYes Then
                cmd.ExecuteNonQuery()
                MsgBox("Successfully Deleted!", vbInformation, "User Account")
                Call cleaner()
                dgvUser.Tag = ""
                'Call loadData("SELECT user_id,fname,lname,pos,uname,pword,isactive FROM tbluser WHERE Deleted = 'N'", dgvUser)
                Call populate()
            Else
                Call cleaner()
                dgvUser.Tag = ""
            End If
        End If
    End Sub

    'To exit the division form
    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        If MsgBox("Are you sure you want to close?", vbQuestion + vbYesNo, "User Account") = vbYes Then
            cmbSearch.SelectedIndex = -1
            txtSearch.Clear()
            Call cleaner()
            Me.Hide()
        End If
    End Sub

    'For selection of data from datagridview
    Private Sub dgvUser_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvUser.CellClick

        If e.RowIndex >= 0 Then

            dgvUser.Tag = dgvUser.Item(0, e.RowIndex).Value
            txtfname.Text = dgvUser.Item(1, e.RowIndex).Value
            txtlname.Text = dgvUser.Item(2, e.RowIndex).Value
            cboPos.Text = dgvUser.Item(3, e.RowIndex).Value
            txtuser.Text = dgvUser.Item(4, e.RowIndex).Value
            txtpass.Text = dgvUser.Item(5, e.RowIndex).Value
            txtrepass.Text = dgvUser.Item(5, e.RowIndex).Value
            chkActive.CheckState = IIf(dgvUser.Item(6, e.RowIndex).Value = 1, 1, 0)
        End If
    End Sub

    'Clean textbox
    Private Sub cleaner()
        Dim obj As Object
        For Each obj In grpEntry.Controls
            If TypeOf obj Is TextBox Then
                obj.clear()
                cboPos.Text = " "
                chkActive.Checked = False
            End If
        Next
    End Sub

    'To save the new added data and updated data
    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim cmd As Odbc.OdbcCommand
        Dim sql As String = ""
        Dim da As OdbcDataAdapter
        Dim txtuser1 = txtuser.Text

        Try
            If txtfname.Text = "" Or txtlname.Text = "" Or txtpass.Text = "" Or txtuser.Text = "" Or cboPos.Text = "" Then
                MsgBox("Please Enter All Fieds!", vbExclamation, "User Account")
                Call cleaner()

            Else
                If txtpass.Text = txtrepass.Text Then
                    If Len(dgvUser.Tag) = 0 Then
                        Dim dt As New DataTable
                        Dim sql1 As String = "SELECT * FROM tbluser WHERE uname = '" & txtuser.Text & "'"
                        da = New OdbcDataAdapter(sql1, modCon.con)
                        da.Fill(dt)
                        If dt.Rows.Count = 0 Then
                            sql = "INSERT INTO tbluser(fname,lname,pos,uname,pword,isactive)VALUES(?,?,?,?,?,?)"
                            MsgBox("Successfully Added!", vbInformation, "User Account")
                        Else
                            MsgBox("Username Already Used!", vbExclamation, "User Account")
                        End If

                    Else
                        Dim dt As New DataTable
                        Dim sql1 As String = "SELECT * FROM tbluser WHERE uname = '" & txtuser.Text & "'"
                        da = New OdbcDataAdapter(sql1, modCon.con)
                        da.Fill(dt)

                        If dt.Rows.Count <> 1 Then
                            sql = "UPDATE tbluser SET fname = ?, lname = ?, pos = ?, uname = ?, pword = ?, isactive = ? WHERE user_id = " & Val(dgvUser.Tag) & ""
                            MsgBox("Successfully Updated!", vbInformation, "User Account")
                        Else
                            MsgBox("Username Already Used!", vbExclamation, "User Account")
                        End If

                    End If
                Else
                    MsgBox("Password did not match!", vbExclamation, "User Account")
                    txtpass.Focus()

                End If
            End If

            cmd = New Odbc.OdbcCommand(sql, modCon.con)
                cmd.Parameters.AddWithValue("?", txtfname.Text)
                cmd.Parameters.AddWithValue("?", txtlname.Text)
                cmd.Parameters.AddWithValue("?", cboPos.Text)
                cmd.Parameters.AddWithValue("?", txtuser.Text)
                cmd.Parameters.AddWithValue("?", txtpass.Text)
                cmd.Parameters.AddWithValue("?", IIf(chkActive.CheckState = CheckState.Checked, 1, 0))
                cmd.ExecuteNonQuery()

                'Call loadData("SELECT user_id,fname,lname,pos,uname,pword,isactive FROM tbluser WHERE Deleted = 'N'", dgvUser)
                Call populate()
                Call cleaner()
                Call enabler(True)


        Catch ex As Exception

        End Try
    End Sub

    'SEARCH BUTTON
    Private Sub btnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Try
            If txtSearch.Text = "" Then
                MsgBox("Please Select Dropdown Menu And Enter your Query", vbInformation, "Information")
            End If
            If cmbSearch.Text = "First Name" Then
                Call loadData("SELECT user_id,fname,lname,pos,uname,pword,isactive FROM tbluser WHERE fname LIKE '%" & txtSearch.Text & "%'", dgvUser)
            Else
                If cmbSearch.Text = "Last Name" Then
                    Call loadData("SELECT user_id,fname,lname,pos,uname,pword,isactive FROM tbluser WHERE lname LIKE '%" & txtSearch.Text & "%'", dgvUser)
                Else
                    If cmbSearch.Text = "Position" Then
                        Call loadData("SELECT user_id,fname,lname,pos,uname,pword,isactive FROM tbluser WHERE pos LIKE '%" & txtSearch.Text & "%'", dgvUser)
                    Else
                        If cmbSearch.Text = "Username" Then
                            Call loadData("SELECT user_id,fname,lname,pos,uname,pword,isactive FROM tbluser WHERE uname LIKE '%" & txtSearch.Text & "%'", dgvUser)
                        End If
                    End If
                End If
            End If

        Catch ex As Exception
        End Try

    End Sub

    'Refresh the datagridview
    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick
        Call loadData("SELECT user_id,fname,lname,pos,uname,pword,isactive FROM tbluser WHERE Deleted = 'N'", dgvUser)
    End Sub

End Class
